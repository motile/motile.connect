﻿using Xunit;

namespace motile.Connect.Tests
{
    public class AtstringTests
    {
        public class SimpleClass
        {
            public string P1 { get; set; }

            public SimpleClass Sub { get; set; }
        }

        [Fact]
        public void Of_SomeProperty_ReturnsPropertyName()
        {
            var c = new SimpleClass();

            Assert.Equal("P1", atstring.of(c, (x) => x.P1));
        }

        [Fact]
        public void Of_SomeNestedProperty_ReturnsPropertyName()
        {
            var c = new SimpleClass();

            Assert.Equal("Sub.P1", atstring.of(c, (x) => x.Sub.P1));
        }
    }
}
