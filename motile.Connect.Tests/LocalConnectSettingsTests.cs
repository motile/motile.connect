﻿using motile.Connect.Validation;
using Xunit;

namespace motile.Connect.Tests
{
    public class LocalConnectSettingsTests
    {
        [Fact]
        public void AddValidationInfoToIgnore_Check_RemoveValidationInfoToIgnore_CheckAgain()
        {
            var s = new LocalConnectSettings();

            var valInfo = new ValidationInfo() { DTOID = "1", PropertyName = "name", Message = "message", Severity = ValidationInfo.Warning, ValidatorID = "1234" };

            s.AddValidationInfoToIgnore(valInfo);

            Assert.Single(s.IgnoreValidationInfos);

            s.RemoveValidationInfoToIgnore(valInfo);

            Assert.Empty(s.IgnoreValidationInfos);
        }
    }
}
