﻿using System.Collections.Generic;
using Xunit;

namespace motile.Connect.Tests
{
    public class DiffernceTests
    {
        public class SimpleClass
        {
            public SimpleClass()
            {
                List1 = new List<string>();
                List2 = new List<SimpleClass>();
            }
            public string P1 { get; set; }

            public SimpleClass Sub { get; set; }

            public List<string> List1 { get; set; }

            public List<SimpleClass> List2 { get; set; }
        }

        [Fact]
        public void IsPath_SimpleProperty_IdentifiesProperty()
        {
            var c1 = new SimpleClass() { P1 = "a" };
            var c2 = new SimpleClass() { P1 = "b" };
            var d = ChangeProperty.Create(c1, c2, x => x.P1);

            Assert.True(d.IsPath(c2, (x) => x.P1));
            Assert.False(d.IsPath(c2, (x) => x.Sub));
            Assert.False(d.IsPath(c2, (x) => x.Sub.P1));
        }

        [Fact]
        public void IsPath_ListProperty_IdentifiesProperty()
        {
            var c = new SimpleClass();
            var d = AddEntryDifference.Create(c, x => x.List1, "test");

            Assert.True(d.IsPath(c, (x) => x.List1));
        }


        [Fact]
        public void IsPath_NestedProperty_IdentifiesProperty()
        {
            var c1 = new SimpleClass();
            var c2 = new SimpleClass();
            c1.Sub = new SimpleClass(){P1 = "a"};
            c2.Sub = new SimpleClass(){P1 = "b"};

            var d = ChangeProperty.Create(c1, c2, x => x.Sub.P1);

            Assert.True(d.IsPath(c2, (x) => x.Sub.P1));
            Assert.False(d.IsPath(c2, (x) => x.P1));
        }
    }
}

