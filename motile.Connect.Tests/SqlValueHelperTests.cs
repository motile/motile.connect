﻿using motile.Connect.CQ.OleDb;
using System;
using System.Globalization;
using Xunit;

namespace motile.Connect.Tests
{
    public class SqlValueHelperTests
    {
        [Theory]
        [InlineData(null, "null")]
        [InlineData("", "''")]
        [InlineData("test", "'test'")]
        public void EscapeValue_SomeString_ReturnsExpectedValue(string value, string expected)
        {
            Assert.Equal(SqlValueHelper.EscapeValue<string>(value), expected);
        }

        [Theory]
        [InlineData(1, "1")]
        [InlineData(null, "")]
        public void EscapeValue_SomeInt_ReturnsExpectedValue(int? value, string expected)
        {
            Assert.Equal(SqlValueHelper.EscapeValue<int?>(value), expected);
        }


        [Theory]
        [InlineData(true, "-1")]
        [InlineData(false, "0")]
        public void EscapeValue_Somebool_ReturnsExpectedValue(bool value, string expected)
        {
            Assert.Equal(SqlValueHelper.EscapeValue<bool>(value), expected);
        }


        [Theory]
        [InlineData("2014.02.01", "CDate('2014.02.01 00:00:00')")]
        public void EscapeValue_SomeDateTime_ReturnsExpectedValue(string value, string expected)
        {
            var date = DateTime.ParseExact(value, "yyyy.MM.dd", CultureInfo.InvariantCulture);

            Assert.Equal(SqlValueHelper.EscapeValue<DateTime>(date), expected);
        }

        [Theory]
        [InlineData("2014.02.01", "CDate('2014.02.01 00:00:00')")]
        [InlineData("", "null")]
        public void EscapeValue_SomeNullableDateTime_ReturnsExpectedValue(string value, string expected)
        {
            DateTime? date = null;

            if (!string.IsNullOrEmpty(value))
            {
                date = DateTime.ParseExact(value, "yyyy.MM.dd", CultureInfo.InvariantCulture);
            }

            Assert.Equal(SqlValueHelper.EscapeValue<DateTime?>(date), expected);
        }
    }
}
