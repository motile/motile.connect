﻿using FluentValidation;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Validation.Tests
{
    public class ValidationAggregatorTests
    {
        public class DTO : IDTO
        {
            public string ID { get; set; }

            public DtoKey DtoKey
            {
                get
                {
                    return new DtoKey("DTO", ID);
                }
            }
        }

        public class DTOValidator : AbstractValidator<DTO>, IValidatorInfo
        {
            public DTOValidator()
            {
                RuleFor(x => x.ID).NotEqual("invalid").AsWarningMessage();
            }

            public string GetValidatorID()
            {
                return "test";
            }
        }

        [Fact]
        public async Task ValidateAsync_DTOContainsAWarning_ReturnsTheWarning()
        {
            var settings = new ValidationSettings();
            var agg = new ValidatorAggregator<DTO>(new[] { new DTOValidator() }, settings);

            var dto = new DTO { ID = "invalid" };

            var v = await agg.ValidateAsync(dto);

            Assert.Single(v);
        }

        [Fact]
        public async Task ValidateAsync_DTOContainsAWarning_SettingIsSetToIgnorIt_ReturnsEmtpyList()
        {
            var settings = new ValidationSettings();

            var agg = new ValidatorAggregator<DTO>(new[] { new DTOValidator() }, settings);

            var dto = new DTO { ID = "invalid" };

            var v = await agg.ValidateAsync(dto);

            var warning = v.Single();
            settings.AddValidationInfoToIgnore(warning);

            agg = new ValidatorAggregator<DTO>(new[] { new DTOValidator() }, settings);

            var v2 = await agg.ValidateAsync(dto);

            Assert.Empty(v2);
        }
    }
}
