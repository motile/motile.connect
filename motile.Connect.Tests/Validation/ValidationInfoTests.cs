﻿using Xunit;

namespace motile.Connect.Validation.Tests
{    
    public class ValidationInfoTests
    {
        [Fact]
        public void GetSeverity_AnErrorMessage_ReturnsError()
        {
            var m = ValidationInfo.FormatAsErrorMessage("test");

            Assert.Equal(ValidationInfo.GetSeverity(m), ValidationInfo.Error);
        }

        [Fact]
        public void GetSeverity_AnWarningMessage_ReturnsWarning()
        {
            var m = ValidationInfo.FormatAsWarningMessage("test");

            Assert.Equal(ValidationInfo.GetSeverity(m), ValidationInfo.Warning);
        }


        [Fact]
        public void GetSeverity_AnMultilineErrorMessage_ReturnsWarning2()
        {
            var m = @"Error: Die Einträge sind nicht kumuliert: 2: 05.01.2015
7: 05.01.2015
15: 05.01.2015";

            Assert.Equal(ValidationInfo.GetSeverity(m), ValidationInfo.Error);
        }


        [Fact]
        public void GetSeverity_AnExcetion_ReturnsError()
        {
            var m = ValidationInfo.FormatAsWarningMessage("Exception: test");

            Assert.Equal(ValidationInfo.GetSeverity(m), ValidationInfo.Error);
        }
    }
}
