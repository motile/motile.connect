﻿using System;
using Xunit;

namespace motile.Connect.Validation.Tests
{
    public class ValidationSettingsTests
    {
        [Fact]
        public void AddValidationInfoToIgnore_AddAWarning_Check_Remove_CheckAgain()
        {
            var s = new ValidationSettings();

            var valInfo = new ValidationInfo() { DTOID = "1", PropertyName = "name", Message = "message", Severity = ValidationInfo.Warning, ValidatorID = "1234" };

            s.AddValidationInfoToIgnore(valInfo);

            Assert.True(s.IgnoreValidationInfo(valInfo));

            s.RemoveValidationInfoToIgnore(valInfo);

            Assert.False(s.IgnoreValidationInfo(valInfo));

        }

        [Fact]
        public void AddValidationInfoToIgnore_TryToAddAnError_ThrowsAnException()
        {
            var s = new ValidationSettings();

            var valInfo = new ValidationInfo() { DTOID = "1", PropertyName = "name", Message = "message", Severity = ValidationInfo.Error, ValidatorID = "1234" };

            Assert.Throws<InvalidOperationException>(() => s.AddValidationInfoToIgnore(valInfo));            
        }
    }
}
