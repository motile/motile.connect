﻿using Autofac;
using Moq;
using motile.Connect.CQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Fake
{
    public class FakeConstants
    {
        public const int AuszubildenenPflegerNr = 5647;
    }
    public class FakeDataOptions
    {
        public IEnumerable<ConnexiaFall> Fälle { get; set; }
        public IEnumerable<ConnexiaLeistung> Leistungen { get; set; }
    }

    public class ConnexiaFakeContainerProvider : ConnexiaContainerProviderDiff
    {
        private FakeDataOptions _options;

        public ConnexiaFakeContainerProvider(FakeDataOptions options = null)
            : base(new DummyDialogManger())
        {
            _options = options ?? new FakeDataOptions();
        }
        protected override void ConfigBuilder(ContainerBuilder builder)
        {
            var mockRepositoryConnexiaFall = new Mock<IQueryEnginePart<ConnexiaFall, DtoQuery<ConnexiaFall>>>();
            mockRepositoryConnexiaFall.Setup(x => x.QueryAsync(It.IsAny<DtoQuery<ConnexiaFall>>()))
                .Returns<DtoQuery<ConnexiaFall>>(q => Task.FromResult(new QueryResult<ConnexiaFall>(_options.Fälle?.Where(cf => cf.DtoKey == q.Key).FirstOrDefault())));

            builder.RegisterInstance(mockRepositoryConnexiaFall.Object).As<IQueryEnginePart<ConnexiaFall, DtoQuery<ConnexiaFall>>>();

            var mockRepositoryConnexiaLeistung = new Mock<IQueryEnginePart<ConnexiaLeistung, DtoQuery<ConnexiaLeistung>>>();
            mockRepositoryConnexiaLeistung.Setup(x => x.QueryAsync(It.IsAny<DtoQuery<ConnexiaLeistung>>()))
                .Returns<DtoQuery<ConnexiaLeistung>>(q => Task.FromResult(new QueryResult<ConnexiaLeistung>(_options.Leistungen?.Where(cf => cf.DtoKey == q.Key).FirstOrDefault())));

            builder.RegisterInstance(mockRepositoryConnexiaLeistung.Object).As<IQueryEnginePart<ConnexiaLeistung, DtoQuery<ConnexiaLeistung>>>();

            var mockRepositoryFallListe = new Mock<IQueryEnginePart<ConnexiaFallListe, DtoQuery<ConnexiaFallListe>>>();
            mockRepositoryFallListe.Setup(x => x.QueryAsync(It.IsAny<DtoQuery<ConnexiaFallListe>>()))
                .Returns<DtoQuery<ConnexiaFallListe>>(q => Task.FromResult(new QueryResult<ConnexiaFallListe>(GetFallListe(q.Key.Id))));

            builder.RegisterInstance(mockRepositoryFallListe.Object).As<IQueryEnginePart<ConnexiaFallListe, DtoQuery<ConnexiaFallListe>>>();

            var mockAus = new Mock<IPflegerIsinRoleProvider>();

            mockAus.Setup(x => x.IsInRole(It.Is<int>(i => i == FakeConstants.AuszubildenenPflegerNr), It.Is<string>(s => s == "Auszubildend"))).Returns(Task.FromResult(true));
            mockAus.Setup(x => x.IsInRole(It.Is<int>(i => i != FakeConstants.AuszubildenenPflegerNr), It.IsAny<string>())).Returns(Task.FromResult(false));

            builder.RegisterInstance(mockAus.Object).As<IPflegerIsinRoleProvider>();

            base.ConfigBuilder(builder);
        }

        private ConnexiaFallListe GetFallListe(string id)
        {

            ConnexiaFallListe.GetIDFaktoren(id, out DateTime from, out DateTime to, out bool nurAktive);

            return new ConnexiaFallListe()
            {
                Von = from,
                Bis = to,
                NurAktive = nurAktive,
                Adressnummern = _options.Fälle.ToDictionary(x => x.Adressnummer, x => $"{x.Nachname} {x.Vorname}")
            };
        }
    }
}
