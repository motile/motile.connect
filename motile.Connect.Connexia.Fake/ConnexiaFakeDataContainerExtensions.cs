﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace motile.Connect.Connexia.Fake
{
    public static class ConnexiaFakeDataContainerExtensions
    {
        public static ConnexiaFakeDataContainer Mit<T>(this ConnexiaFakeDataContainer container, Expression<Func<ConnexiaFall, T>> expression, T value)
        {
            var memberSelectorExpression = expression.Body as MemberExpression;
            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(container.Fall, value, null);
                }
            }

            return container;
        }

        public static ConnexiaFakeDataContainer MitDoku(this ConnexiaFakeDataContainer container, string gruppe, string detail, DateTime? datum = null, string wert = null)
        {
            var list = container.Fall.Dokus;

            if (!datum.HasValue)
            {
                datum = list.Select(x => x.Datum).Max();
            }

            foreach (var item in list.Where(x => x.Datum == datum.Value && x.Gruppe == gruppe).ToList())
            {
                list.Remove(item);
            }

            if (!string.IsNullOrEmpty(detail))
            {
                foreach (var d in detail.Split(','))
                {
                    list.Add(new ConnexiaFall.Doku() { Datum = datum.Value, Detail = d, Gruppe = gruppe, Wert = wert });
                }
            }

            return container;
        }

        public static ConnexiaFakeDataContainer OhneDokDaten(this ConnexiaFakeDataContainer container)
        {
            container.Fall.Dokus.Clear();

            return container;
        }

        public static ConnexiaFakeDataContainer MitLeistungen(this ConnexiaFakeDataContainer container, int[] leistungen, int? pflegernummer = 1, DateTime? datum = null)
        {
            var date = datum ?? DateTime.Today;
            var from = new DateTime(date.Year, date.Month, 1);                              //ersterTagDesMonats
            var to = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddMinutes(-1);    //letzerTagDesMonats

            ConnexiaLeistung cl = container.Leistungen.SingleOrDefault(); // todo: unterschiedliche Pfleger + Zeiträume

            if (cl == null)
            {
                cl = new ConnexiaLeistung()
                {
                    Pflegernummer = pflegernummer ?? ConnexiaFall.EmptyID,
                    Von = from,
                    Bis = to,
                    Liste = new List<ConnexiaLeistung.Eintrag>()
                };

                container.Leistungen.Add(cl);
            }

            cl.Liste.AddRange(leistungen
                .GroupBy(x => x)
                .Select(x => new ConnexiaLeistung.Eintrag()
                {
                    Adressnummer = container.Fall.Adressnummer,
                    Datum = date,
                    Leistung = x.Key,
                    Anzahl = x.Count()
                }).ToList());


            return container;
        }
    }
}
