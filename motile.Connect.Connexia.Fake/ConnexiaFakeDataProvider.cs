﻿using motile.Connect.Connexia.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Fake
{
    public class ConnexiaFakeDataProvider
    {
        public ConnexiaFall GetStandardFall(int adressnummer = -1000)
        {
            var dokDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);   //ersterTagDesMonats;

            return new ConnexiaFall()
            {
                Adressnummer = adressnummer,
                Nachname = "Testklient",
                Vorname = "Test",
                Land = "Österreich",
                Adresse = "Teststraße 1",
                PLZ = "6900",
                Ort = "Bregenz",
                Geschlecht = "w",
                Staatsbuergerschaft = "AT",
                Geburtsdatum = new DateTime(year: 2001, month: 1, day: 1),
                Versicherung = "19",
                Versicherungsnummer = "1235-01.01.01",
                Dokus = (new[]
                    {
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "00", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "02", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "03", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "03", Detail = "02" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "04", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "05", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "07", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "08", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "09", Detail = "01" },
                        new ConnexiaFall.Doku() { Datum = dokDate, Gruppe = "21", Detail = "01" }
                    }).ToList()
            };
        }

        public ConnexiaFall GetStandardFallNichtKlient(int adressnummer = -1900)
        {
            return new ConnexiaFall()
            {
                Adressnummer = adressnummer,
                Nachname = "Testklient",
                Vorname = "Beratung",
                Land = null,
                PLZ = null,
                Geschlecht = null,
                Geburtsdatum = null,
                Versicherung = null,
                Versicherungsnummer = null
            };
        }
    }
}
