﻿using System.Threading.Tasks;

namespace motile.Connect.Connexia.Fake
{
    public class DummyDialogManger : IDialogManager
    {
        public Task<string> AskAsync(string message, string[] options, string defaultAnswer = null)
        {
            return Task.FromResult<string>(defaultAnswer);
        }
    }    
}