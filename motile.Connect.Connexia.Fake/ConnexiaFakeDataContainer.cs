﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Fake
{
    public class ConnexiaFakeDataContainer
    {
        public ConnexiaFall Fall { get; set; }
        public List<ConnexiaLeistung> Leistungen { get; set; } = new List<ConnexiaLeistung>();
    }
}
