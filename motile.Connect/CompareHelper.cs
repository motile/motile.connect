﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace motile.Connect
{
    public static class CompareHelper
    {
        public static IEnumerable<ChangeProperty> CompareString<T>(T master, T changeThis, params Expression<Func<T, string>>[] expression)
        {
            foreach (var e in expression)
            {
                var cc = e.Compile();

                var val1 = GetStringLengthValue(changeThis, e, cc(changeThis)) ?? string.Empty;
                var val2 = GetStringLengthValue(master, e, cc(master)) ?? string.Empty;

                if (!string.Equals(val2, val1, StringComparison.Ordinal))
                    yield return ChangeProperty.Create(master, changeThis, e, val1, val2);
            }
        }

        public static string GetStringLengthValue<T>(T instance, Expression<Func<T, string>> expression)
        {
            if (instance == null)
                return null;

            var memberExpression = expression.Body as MemberExpression;

            var pInfo = memberExpression.Member as PropertyInfo;
            var value = (string)pInfo.GetValue(instance);

            return GetStringLengthValue<T>(instance, expression, value);
        }

        public static string GetStringLengthValue<T>(Expression<Func<T, string>> expression, string value)
        {
            return GetStringLengthValue<T>(default(T), expression, value);
        }

        public static string GetStringLengthValue<T>(T instance, Expression<Func<T, string>> expression, string value)
        {
            var memberExpression = expression.Body as MemberExpression;

            var result = value ?? string.Empty;

            var stringLength = memberExpression.Member.GetCustomAttributes(typeof(StringLengthAttribute), true).Cast<StringLengthAttribute>().FirstOrDefault();
            if (stringLength != null)
            {
                if (!string.IsNullOrEmpty(result) && result.Length > stringLength.MaximumLength)
                    result = result.Substring(0, stringLength.MaximumLength).TrimEnd();

                if (result == string.Empty && stringLength.MinimumLength > 0)
                    result = null;
            }

            return result;
        }

        public static IEnumerable<ChangeProperty> CompareInt<T>(T master, T changeThis, params Expression<Func<T, int>>[] expression)
        {
            foreach (var e in expression)
            {
                var cc = e.Compile();

                if (cc(master) != cc(changeThis))
                    yield return ChangeProperty.Create(master, changeThis, e);
            }
        }

        public static IEnumerable<ChangeProperty> CompareNInt<T>(T master, T changeThis, params Expression<Func<T, int?>>[] expression)
        {
            foreach (var e in expression)
            {
                var cc = e.Compile();

                var d1 = cc(master);
                var d2 = cc(changeThis);

                if (d1 == null && d2 == null)
                {

                }
                else if (d1 == null || d2 == null || d1 != d2)
                {
                    yield return ChangeProperty.Create(master, changeThis, e);
                }
            }
        }

        public static IEnumerable<ChangeProperty> CompareNDateTime<T>(T master, T changeThis, params Expression<Func<T, DateTime?>>[] expression)
        {
            foreach (var e in expression)
            {
                var cc = e.Compile();

                var d1 = cc(master);
                var d2 = cc(changeThis);

                if (d1 == null && d2 == null)
                {

                }
                else if (d1 == null || d2 == null || d1 != d2)
                {
                    yield return ChangeProperty.Create(master, changeThis, e);
                }
            }
        }
    }
}
