﻿using System;
using System.Linq;
using motile.Connect.Validation;
using System.Collections.Generic;

namespace motile.Connect
{
    public class LocalConnectSettings
    {
        public const string ID = "ConnectSettings";

        public LocalConnectSettings()
        {
            this.IgnoreValidationInfos = new Dictionary<string, DateTime>();
        }

        public Dictionary<string, DateTime> IgnoreValidationInfos { get; set; }

        public void AddValidationInfoToIgnore(ValidationInfo info)
        {
            var valSetting = new ValidationSettings(IgnoreValidationInfos);
            valSetting.AddValidationInfoToIgnore(info);
            this.IgnoreValidationInfos = new Dictionary<string, DateTime>(valSetting.IgnoreValidationInfos);
        }

        public void RemoveValidationInfoToIgnore(ValidationInfo info)
        {
            var valSetting = new ValidationSettings(IgnoreValidationInfos);
            valSetting.RemoveValidationInfoToIgnore(info);
            this.IgnoreValidationInfos = new Dictionary<string, DateTime>(valSetting.IgnoreValidationInfos);
        }
    }
}
