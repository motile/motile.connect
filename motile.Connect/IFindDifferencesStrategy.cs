﻿using System.Collections.Generic;

namespace motile.Connect
{       
    public interface IFindDifferencesStrategy<T>
        where T: IDTO
    {
        IEnumerable<DiffsPack> GetDifferences(T master, T changeThis);
    }

}
