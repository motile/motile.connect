﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace motile.Connect
{
    public class LocalSettingsProvider<T>
        where T : class, new()
    {
        private readonly string _settingsID;
        private string _path;

        public LocalSettingsProvider(string settingsID)
        {
            _settingsID = settingsID;
        }

        public T GetSettings()
        {
            T result = null;
            var path = GetPath();

            if (File.Exists(path))
            {
                var s = File.ReadAllText(path);
                if (!string.IsNullOrEmpty(s))
                {
                    try
                    {
                        result = JsonConvert.DeserializeObject<T>(s);
                    }
                    catch { }
                }
            }

            if (result == null)
            {
                result = new T();
            }

            return result;
        }

        public void SetSettings(T settings)
        {
            SetSettings(settings, GetPath());
        }

        private void SetSettings(T settings, string path)
        {
            var s = JsonConvert.SerializeObject(settings);

            (new FileInfo(path)).Directory.Create();

            File.WriteAllText(path, s);
        }


        private string GetPath()
        {
            if (string.IsNullOrEmpty(_path))
            {
                _path = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "motile.Connect", $"{_settingsID}.json");

                if (!File.Exists(_path))
                {
                    if (!Directory.Exists(Path.GetDirectoryName(_path)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(_path));
                    }

                    var obsoleteLocation = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "motile.Connect", $"{_settingsID}.json");

                    if (!File.Exists(obsoleteLocation))
                    {
                        obsoleteLocation = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "motile.Connect", $"{_settingsID}.json");
                    }

                    if (File.Exists(obsoleteLocation))
                    {
                        try
                        {
                            File.Copy(obsoleteLocation, _path);
                        }
                        catch
                        {
                         
                        }
                        try
                        {
                            File.Delete(obsoleteLocation);
                        }
                        catch
                        {
                         
                        }
                    }
                }
            }

            return _path;
        }
    }
}
