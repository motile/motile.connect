﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace motile.Connect
{
    public class AddEntryDifference : Difference
    {
        public static AddEntryDifference Create<T, V>(T dto, Expression<Func<T, IEnumerable<V>>> path, V newEntry)
        {
            return new AddEntryDifference() { Path = atstring.of(dto, path), NewEntry = newEntry };
        }

        public object NewEntry { get; set; }
    }

    public class RemoveEntryDifference : Difference
    {
        public static RemoveEntryDifference Create<T, V>(T dto, Expression<Func<T, IEnumerable<V>>> path, V remove)
        {
            return new RemoveEntryDifference() { Path = atstring.of(dto, path), RemoveEntry = remove };
        }

        public object RemoveEntry { get; set; }
    }

    public class ChangeProperty : Difference
    {
        public static ChangeProperty Create<T, U>(T master, T changeThis, Expression<Func<T, U>> expression)
        {
            var f = expression.Compile();

            return new ChangeProperty() { Path = atstring.of(master, expression), OldValue = f(changeThis), NewValue = f(master) };
        }

        public static ChangeProperty Create<T, U>(T master, T changeThis, Expression<Func<T, U>> expression, U oldValue, U newvalue)
        {
            return new ChangeProperty() { Path = atstring.of(master, expression), OldValue = oldValue, NewValue = newvalue };
        }

        public object OldValue { get; set; }
        public object NewValue { get; set; }
    }

    public abstract class Difference
    {
        public string Path { get; set; }

        public bool IsPath<T, U>(Expression<Func<T, U>> expression)
        {
            return IsPath(default(T), expression);
        }

        public bool IsPath<T, U>(params Expression<Func<T, U>>[] expressions)
        {
            return IsPath(default(T), expressions);
        }


        public bool IsPath<T, U>(T dto, Expression<Func<T, U>> expression)
        {
            var membername = atstring.of(dto, expression);

            return membername == Path;
        }

        public bool IsPath<T, U>(T dto, params Expression<Func<T, U>>[] expressions)
        {
            bool result = false;
            foreach (var expression in expressions)
            {
                var membername = atstring.of<T, U>(dto, expression);

                result = membername == Path;

                if (result)
                    break;
            }

            return result;
        }
    }

    public class DiffsPack
    {
        public DiffsPack(DtoKey dtoKey, params Difference[] diffs)
        {
            this.DtoKey = dtoKey;
            this.Diffs = diffs ?? new Difference[] { };
        }
        public Difference[] Diffs { get; private set; }

        public DtoKey DtoKey { get; private set; }
    }
}
