﻿using motile.Connect.CQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect
{    
    public class Connector<T>
        where T : IDTO
    {
        #region - private fields
        private IQueryEngine _queryEngine;
        private IFindDifferencesStrategy<T> _findDifferencesStrategy;
        private IChangeInfoGenerator<T> _ciGen;
        #endregion

        #region - ctr
        public Connector(IQueryEngine queryEngine, IFindDifferencesStrategy<T> findDifferencesStrategy, IChangeInfoGenerator<T> changeInfoGenerator, CommandExecutor commandExecutor)
        {
            this._queryEngine = queryEngine;
            this._findDifferencesStrategy = findDifferencesStrategy;
            this._ciGen = changeInfoGenerator;
        }
        #endregion

        #region - methods
        public async Task<IEnumerable<ChangeInfo>> ResolveChangeInfosAsync(T master)
        {
            IEnumerable<ChangeInfo> result = null;

            var changeThis = await _queryEngine.QueryAsync(new DtoQuery<T>(master.DtoKey));

            if (!changeThis.IsEmpty())
            {
                result = await this.ResolveChangeInfosAsync(master, changeThis.Results.First());
            }

            return result ?? new ChangeInfo[] { };
        }

        public async Task<IEnumerable<ChangeInfo>> ResolveChangeInfosAsync(T master, T changeThis)
        {
            var diffsPacks = _findDifferencesStrategy.GetDifferences(master, changeThis);

            var cis = await _ciGen.Process(diffsPacks);

            return cis;
        }
        #endregion
    }
}