﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace motile.Connect
{

    public interface ICachingProvider
    {
        void AddItem(string key, object value, int exp);
        bool GetItem<T>(string key, out T item);
    }

    public class MemoryCacheProvider : ICachingProvider
    {

        private MemoryCache cache = new MemoryCache("MemoryCacheProvider");

        static readonly object padlock = new object();
        private readonly string empty = "empty";

        public void AddItem(string key, object value, int expires)
        {
            lock (padlock)
            {
                DateTimeOffset d;
                if (expires > 0)
                    d = DateTimeOffset.Now.Add(TimeSpan.FromMilliseconds(expires));
                else
                    d = DateTimeOffset.MaxValue;

                cache.Add(key, value ?? empty, d);
            }
        }

        public void RemoveItem(string key)
        {
            lock (padlock)
            {
                cache.Remove(key);
            }
        }

        public bool GetItem<T>(string key, out T item)
        {
            lock (padlock)
            {
                var c = cache[key];

                var result = c != null;

                if (c is string && ((string)c) == empty)
                {
                    c = null;
                }

                if (c != null)
                {
                    item = (T)c;
                }
                else
                {
                    item = default(T);
                }

                return result;
            }
        }
    }
}
