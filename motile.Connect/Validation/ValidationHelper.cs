﻿using FluentValidation;
using FluentValidation.Internal;
using System;

namespace motile.Connect.Validation
{
    public static class ValidationHelper
    {
        public static IRuleBuilderOptions<T, TProperty> WithErrorMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, string errorMessage)
        {
            return rule.WithMessage(ValidationInfo.FormatAsErrorMessage(errorMessage));
        }

        public static IRuleBuilderOptions<T, TProperty> WithErrorMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, Func<T, string> messageProvider)
        {

            return rule.WithMessage((x) => ValidationInfo.FormatAsErrorMessage(messageProvider(x)));
        }

        public static IRuleBuilderOptions<T, TProperty> WithWarningMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, string errorMessage)
        {
            return rule.WithMessage(ValidationInfo.FormatAsWarningMessage(errorMessage));
        }

        public static IRuleBuilderOptions<T, TProperty> WithWarningMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, Func<T, string> messageProvider)
        {
            return rule.WithMessage((x) => ValidationInfo.FormatAsWarningMessage(messageProvider(x)));
        }

        public static IRuleBuilderOptions<T, TProperty> AsErrorMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule)
        {
            return AsFormatedMessage(rule, ValidationInfo.FormatAsErrorMessage);
        }

        public static IRuleBuilderOptions<T, TProperty> AsWarningMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule)
        {
            return AsFormatedMessage(rule, ValidationInfo.FormatAsWarningMessage);
        }

        private static IRuleBuilderOptions<T, TProperty> AsFormatedMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, Func<string, string> getmessage)
        {
            if (rule is RuleBuilder<T, TProperty> rb)
            {
                return rule.WithMessage(getmessage(rb.Rule.CurrentValidator.Options.ErrorMessageSource.GetString(null)));
            }

            throw new NotImplementedException();
        }
    }
}
