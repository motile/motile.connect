﻿using FluentValidation.Results;
using System;
using System.Text.RegularExpressions;

namespace motile.Connect.Validation
{
    public class ValidationInfo
    {
        public static readonly string Error = "Error";
        public static readonly string Warning = "Warning";

        #region - properties
        public string DTOID { get; set; }
        public string Severity { get; set; }
        public string Message { get; set; }
        public string PropertyName { get; set; }
        public object AttemptedValue { get; set; }
        public string ValidatorID { get; set; }
        #endregion

        #region - helpers
        public string GetKey()
        {
            return GetKey(this);
        }
        public static string GetKey(ValidationInfo info)
        {
            return string.Format("{0};{1};{2};{3}", info.ValidatorID, info.DTOID, info.PropertyName, GetAttemptedValueString(info.AttemptedValue));
        }

        public static string GetValidatorIdFromKey(string key)
        {
            return key.Split(';')[0];
        }

        public static string GetDTOIdFromKey(string key)
        {
            return key.Split(';')[1];
        }

        private static string GetAttemptedValueString(object attemptedValue)
        {
            if (attemptedValue == null)
                return string.Empty;

            if (attemptedValue is string)
                return (string)attemptedValue;

            if (attemptedValue is ValueType)
                return attemptedValue.ToString();

            return string.Empty;

        }

        public static string FormatAsErrorMessage(string message) => $"{Error}: {message}";

        public static string FormatAsWarningMessage(string message) => $"{Warning}: {message}";
        

        private static readonly Regex SeverityPattern = new Regex("^(?<severity>.+?): (?<message>(.|\n)+)$");

        public static string GetSeverity(ValidationFailure error)
        {
            return GetSeverity(error.ErrorMessage);
        }

        public static string GetSeverity(string messageWithSeverity)
        {
            string result = null;

            if (!string.IsNullOrEmpty(messageWithSeverity))
            {
                if (messageWithSeverity.Contains("Exception:"))  //Wenn eine Exception gemeldet wird, dann soll das immer als Error gelten
                    return ValidationInfo.Error;

                var m = SeverityPattern.Match(messageWithSeverity);
                if (m.Success)
                {
                    result = m.Groups["severity"].Value;
                }
            }

            return result;
        }

        public static string GetMessage(string messageWithSeverity)
        {
            string result = null;

            if (!string.IsNullOrEmpty(messageWithSeverity))
            {
                var m = SeverityPattern.Match(messageWithSeverity);
                if (m.Success)
                {
                    result = m.Groups["message"].Value;
                }
            }

            return result;
        }
        #endregion
    }
}
