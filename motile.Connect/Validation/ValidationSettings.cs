﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace motile.Connect.Validation
{
    public class ValidationSettings
    {
        public ValidationSettings(IDictionary<string, DateTime> ignoreInfos = null)
        {
            IgnoreValidationInfos = ignoreInfos != null ? new SortedDictionary<string, DateTime>(ignoreInfos) : new SortedDictionary<string, DateTime>();
        }

        public IDictionary<string, DateTime> IgnoreValidationInfos { get; private set; }

        #region - methods
        public void AddValidationInfoToIgnore(ValidationInfo info)
        {

            if (info.Severity != ValidationInfo.Warning)
                throw new InvalidOperationException("Es können nur Warnungen ignoriert werden!");

            var s = info.GetKey();
            if (!IgnoreValidationInfos.ContainsKey(s))
            {
                IgnoreValidationInfos.Add(s, DateTime.Now);
            }
        }

        public void RemoveValidationInfoToIgnore(ValidationInfo info)
        {
            var s = info.GetKey();
            if (IgnoreValidationInfos.ContainsKey(s))
            {
                IgnoreValidationInfos.Remove(s);
            }
        }

        public bool IgnoreValidationInfo(ValidationInfo info)
        {
            return IgnoreValidationInfos.ContainsKey(info.GetKey());
        }
        #endregion
    }
}
