﻿using FluentValidation;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace motile.Connect.Validation
{
    public class ValidatorAggregator<T>
        where T : IDTO
    {

        private readonly List<IValidator<T>> _validators;
        private readonly ValidationSettings _settings;

        public ValidatorAggregator(IEnumerable<IValidator<T>> validators, ValidationSettings settings)
        {
            _validators = validators.ToList();
            _settings = settings;
        }

        public async Task<IEnumerable<ValidationInfo>> ValidateAsync(T dto)
        {
            var tasks = _validators.Select(x => new { ID = GetValidatorID(x), ValidationTask = x.ValidateAsync(dto) }).ToList();

            await Task.WhenAll(tasks.Select(x => x.ValidationTask));

            var result = tasks
                .Where(x => !x.ValidationTask.Result.IsValid)
                .SelectMany(x => x.ValidationTask.Result.Errors.Select(y => new { x.ID, Error = y }))
                .Select(x => new ValidationInfo()
                {
                    DTOID = dto.DtoKey.Id,
                    ValidatorID = x.ID,
                    Severity = ValidationInfo.GetSeverity(x.Error),
                    Message = ValidationInfo.GetMessage(x.Error.ErrorMessage),
                    PropertyName = x.Error.PropertyName,
                    AttemptedValue = x.Error.AttemptedValue
                })
                .Where(x => !IgnoreThis(x))
                .ToList();

            return result;
        }

        private bool IgnoreThis(ValidationInfo info)
        {
            var result = false;

            if (_settings != null && info.Severity == ValidationInfo.Warning)
            {
                result = _settings.IgnoreValidationInfo(info);
            }
            return result;
        }

        private string GetValidatorID(object validator)
        {
            IValidatorInfo info;
            if ((info = validator as IValidatorInfo) != null)
            {
                return info.GetValidatorID();
            }
            else if (info != null)
            {
                return validator.GetType().Name;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
