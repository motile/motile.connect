﻿
namespace motile.Connect.Validation
{
    public interface IValidatorInfo
    {
        string GetValidatorID();
    }
}
