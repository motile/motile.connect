﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace motile.Connect.Validation
{
    public class LoadInfoPropertyValidator<T, U> : PropertyValidator
    {
        
        private readonly Func<T, Task<U>> _resolveAsnc;
        private readonly Func<PropertyValidatorContext, T, U, bool> _isValid;        

        public override bool ShouldValidateAsync(ValidationContext context) => true;        

        public LoadInfoPropertyValidator(Func<T, Task<U>> resolveAsnc, Func<PropertyValidatorContext, T, U, bool> isValid)
            : base("{PropertyName}, {Detail}")
        {
            _resolveAsnc = resolveAsnc;
            _isValid = isValid;

        }

        protected override ValidationFailure CreateValidationError(PropertyValidatorContext context)
        {
            return base.CreateValidationError(context);
        }
        
        protected override async Task<bool> IsValidAsync(PropertyValidatorContext context, CancellationToken cancellation)
        {
            bool result = false;

            var t = (T)context.Instance;

            if (t != null)
            {
                var u = await _resolveAsnc(t);
                if (u != null)
                {
                    try
                    {
                        result = _isValid(context, t, u);
                    }
                    catch (Exception e)
                    {
                        context.MessageFormatter.AppendArgument("Detail", $"Exception: {e.Message}");
                        result = false;
                    }
                    
                }
            }

            return result;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            throw new InvalidOperationException();
        }
    }
}
