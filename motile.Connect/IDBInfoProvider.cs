﻿
namespace motile.Connect
{
    public interface IDBInfoProvider
    {
        string Name { get; }
    }
}
