﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace motile.Connect
{
    public class ConnectEntry
    {
        public ConnectEntry(DtoKey dtoKey)
        {
            this.DtoKey = dtoKey;
            this.ChangeInfos = new List<ChangeInfo>();

            this.Entity = dtoKey.Id;
            this.Domain = dtoKey.TypeKey;
        }

        public DtoKey DtoKey { get; private set; }
        public string Entity { get; set; }
        public string Domain { get; set; }

        public List<ChangeInfo> ChangeInfos { get; private set; }
    }
}
