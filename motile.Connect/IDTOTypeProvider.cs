﻿using motile.Connect.CQ;
using System;

namespace motile.Connect
{
    public interface IDTOTypeProvider
    {
        Type GetTypeFromKey(string typeKey);

        DtoQuery<T> CreateQuery<T>(string id)
            where T : IDTO;
    }
}
