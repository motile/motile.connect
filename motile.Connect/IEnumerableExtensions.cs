﻿using System.Collections;
using System.Text;
using System.Linq;

namespace motile.Connect
{
    public static class IEnumerableExtensions
    {
        
        public static string AggregateToString(this IEnumerable items, string delimiter)
        {
            StringBuilder stringBuilder = null;
            if (items != null)
            {
                foreach (object current in items)
                {
                    if (current != null)
                    {
                        if (stringBuilder == null)
                        {
                            stringBuilder = new StringBuilder();
                        }
                        else
                        {
                            stringBuilder.Append(delimiter);
                        }
                        stringBuilder.Append(current.ToString());
                    }
                }
            }

            if (stringBuilder == null)
            {
                return string.Empty;
            }

            return stringBuilder.ToString();
        }
    }
}
