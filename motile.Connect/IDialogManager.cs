﻿using System.Threading.Tasks;

namespace motile.Connect
{
    public interface IDialogManager
    {
        Task<string> AskAsync(string message, string[] options, string defaultAnswer = null);
    }
}
