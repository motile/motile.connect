﻿using motile.Connect.CQ;

namespace motile.Connect
{
    public class ChangeInfo
    {
        public ChangeInfo(DiffsPack diffsPack, params Command[] commands)
        {
            this.DiffsPack = diffsPack;
            this.Commands = commands ?? new Command[] { };
        }

        public DiffsPack DiffsPack { get; private set; }
        public Command[] Commands { get; private set; }
    }
}
