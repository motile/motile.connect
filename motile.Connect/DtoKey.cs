﻿using System;
using System.Diagnostics.Contracts;

namespace motile.Connect
{
    public class DtoKey : IEquatable<DtoKey>
    {
        public DtoKey(string typeKey, string id)
        {
            Contract.Assert(!string.IsNullOrEmpty(typeKey));
            Contract.Assert(!string.IsNullOrEmpty(id));

            this.TypeKey = typeKey;
            this.Id = id;
        }

        public string TypeKey { get; private set; }
        public string Id { get; private set; }

        #region - IEquatable<DtoKey> Members
        public bool Equals(DtoKey other)
        {
            var result = !object.ReferenceEquals(other, null)
                && this.TypeKey == other.TypeKey
                && this.Id == other.Id;

            return result;
        }

        public override int GetHashCode()
        {
            var result = 0;
            result = (result * 397) ^ this.TypeKey.GetHashCode();
            result = (result * 397) ^ this.Id.GetHashCode();
            
            return result;
        }

        public override bool Equals(object obj)
        {
            var selEnt = obj as DtoKey;
            if (!object.ReferenceEquals(selEnt, null))
            {
                return Equals(selEnt);
            }
            else
            {
                return false;
            }
        }

        public static bool operator ==(DtoKey i1, DtoKey i2)
        {
            bool equals = (object.ReferenceEquals(i1, null)) && (object.ReferenceEquals(i2, null));

            if (!object.ReferenceEquals(i1, null))
            {
                equals = i1.Equals(i2);
            }

            return equals;
        }

        public static bool operator !=(DtoKey i1, DtoKey i2)
        {
            return !(i1 == i2);
        }
        #endregion
    }
}
