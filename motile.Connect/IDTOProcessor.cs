﻿using System.Threading.Tasks;

namespace motile.Connect
{
    public interface IDTOProcessor
    {
        Task<bool> ProcessAsync(IDTO dto);
    }

    public interface IDTOProcessor<in T> : IDTOProcessor
        where T : IDTO
    {
        Task<bool> ProcessAsync(T dto);
    }  
}