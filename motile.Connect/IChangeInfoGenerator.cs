﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace motile.Connect
{
    public interface IChangeInfoGenerator<T>
        where T: IDTO
    {
        Task<IEnumerable<ChangeInfo>> Process(IEnumerable<DiffsPack> differences);
    }
}
