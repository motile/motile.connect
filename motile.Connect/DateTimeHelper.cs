﻿using System;

namespace motile.Connect
{
    public static class DateTimeHelper
    {        
        public static DateTime Clip(this DateTime date, long roundTicks)
        {        
            return new DateTime(date.Ticks - date.Ticks % roundTicks);
        }

        public static DateTime Clip(this DateTime date)
        {
            return Clip(date, TimeSpan.TicksPerSecond);
        }

    }
}
