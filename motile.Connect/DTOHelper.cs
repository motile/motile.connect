﻿
using System.Text.RegularExpressions;
namespace motile.Connect
{
    public static class DTOHelper
    {
        public static string GetFileName(this IDTO dto)
        {
            return string.Format("[{0}]_{1}.txt", dto.GetType().Name, dto.DtoKey.Id);
        }

        private static Regex _fileNamePattern = new Regex(@"\[(?<type>(\w|\d)+?)]_.*?.txt$");
        public static string GetTypeNameFromFileName(string fileName)
        {
            string result = string.Empty;

            var m = _fileNamePattern.Match(fileName);
            if (m.Success)
            {
                result = m.Groups["type"].Value;
                }

            return result;
        }
    }
}
