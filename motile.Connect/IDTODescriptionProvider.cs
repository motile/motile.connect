﻿using System.Threading.Tasks;

namespace motile.Connect
{
    public interface IDTODescriptionProvider
    {
        Task<string> WithIDAsync(DtoKey key);
    }


    public interface IDTODescriptionProvider<T>
        where T:IDTO
    {
        Task<string> WithIDAsync(string id);
    }
}
