﻿namespace motile.Connect.CQ
{  
    public abstract class Command
    {
        public Command(string info)
        {            
            this.Info = info;
        }
        
        public string Info { get; private set; }
    }
}
