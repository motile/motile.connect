﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace motile.Connect.CQ
{
    public abstract class QueryResult
    {
        public abstract IEnumerable GetResults();
    }

    public class QueryResult<T> : QueryResult
    {
        public QueryResult(T result)
        {
            if (result != null)
                this.Results = new[] { result };
            else
                this.Results = new T[] { };
        }

        public QueryResult(T[] results)
        {
            this.Results = results != null ? results.ToArray() : new T[] { };
        }

        public T[] Results { get; private set; }

        public override IEnumerable GetResults()
        {
            return this.Results;
        }

        public bool IsEmpty() { return this.Results == null || this.Results.Length == 0; }
    }
}
