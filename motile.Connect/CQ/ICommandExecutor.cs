﻿using System.Threading.Tasks;

namespace motile.Connect.CQ
{
    public interface ICommandExecutor
    {
        Task<bool> Process(DtoKey key, Command command);

        bool CanProcess(DtoKey key, Command command);
    }
}
