﻿using System;
using System.Threading.Tasks;

namespace motile.Connect.CQ.Fnc
{
    public class FncCommand : Command
    {
        private readonly Func<Task<bool>> _f;

        public FncCommand(Func<Task<bool>> f, string info)
            :base(info)
        {
            _f = f;
        }

        public Task<bool> Execute()
        {
            return _f();
        }
    }
}
