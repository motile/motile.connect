﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using motile.Connect.CQ.Fnc;

namespace motile.Connect.CQ
{
    public class CommandExecutor
    {
        #region - private fields
        private IEnumerable<ICommandExecutor> _processors;
        #endregion

        #region - ctr
        public CommandExecutor(IEnumerable<ICommandExecutor> processors)
        {
            _processors = processors;
        }
        #endregion

        #region - public methods
        public async Task<bool> Execute(IEnumerable<ChangeInfo> changeInfos)
        {
            bool result = true;

            foreach (var ci in changeInfos)
            {
                var dtoKey = ci.DiffsPack.DtoKey;

                foreach (var cmd in ci.Commands)
                {
                    if (cmd is FncCommand)
                    {
                        try
                        {
                            var task = ((FncCommand)cmd).Execute();

                            await task;
                        }
                        catch
                        {
                            result = false;                            
                        }
                    }
                    else
                    {
                        var p = _processors.Where(x => x.CanProcess(dtoKey, cmd)).FirstOrDefault();
                        if (p != null)
                        {
                            result = await p.Process(dtoKey, cmd);
                        }
                    }

                    if (!result)
                    {
                        break;
                    }
                }
            }

            return result;
        }
        #endregion
    }
}
