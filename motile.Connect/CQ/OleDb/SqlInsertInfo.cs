﻿using System.Linq;

namespace motile.Connect.CQ.OleDb
{
    public class SqlInsertInfo
    {
        public SqlInsertInfo(string table, string columnID, string id, string columnNames, string columnValues)
        {
            this.Table = table;
            this.ColumnId = columnID;
            this.Id = id;
            this.ColumnNames = columnNames;
            this.ColumnValues = columnValues;

        }


        public static SqlInsertInfo Create<U>(string table, string columnID, U id, string[] columnNames = null, string[] values = null)
        {
            string colNames = string.Empty;
            string colValues = string.Empty;

            if (columnNames != null && columnNames.Any())
            {
                for (var i = 0; i < columnNames.Length; i++)
                {
                    colNames = string.Format("{0},[{1}]", colNames, columnNames[i]);
                    colValues = string.Format("{0},{1}", colValues, values[i]);
                }
            }

            return new SqlInsertInfo(table, columnID, SqlValueHelper.EscapeValue(id), colNames, colValues);
        }

        public string Id { get; private set; }
        public string Table { get; private set; }
        public string ColumnId { get; private set; }
        public string ColumnNames { get; private set; }
        public string ColumnValues { get; private set; }
    }  
}
