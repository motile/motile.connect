﻿using System;
using System.Data.OleDb;

namespace motile.Connect.CQ.OleDb
{
    public class OleDbBaseCommand : Command
    {
        public OleDbBaseCommand(string cmdText, string info)
            : base(info)
        {
            this.CmdText = cmdText;
        }

        public string CmdText { get; private set; }

        public OleDbCommand GetCommand()
        {
            return new OleDbCommand(this.CmdText);
        }
    }
}
