﻿namespace motile.Connect.CQ.OleDb
{
    public class OleDbSQLInsertCommand : OleDbBaseCommand
    {
        #region - ctr
        public OleDbSQLInsertCommand(SqlInsertInfo insertInfo, string info)
            : base(GetSQL(insertInfo), info)
        {
            
        }
        #endregion

        #region - methods
        public static string GetSQL(SqlInsertInfo insertInfo)
        {
            return string.Format("INSERT INTO [{0}] ([{1}]{2}) VALUES ({3}{4})", insertInfo.Table, insertInfo.ColumnId, insertInfo.ColumnNames, insertInfo.Id, insertInfo.ColumnValues);
        }
        #endregion
    }
}
