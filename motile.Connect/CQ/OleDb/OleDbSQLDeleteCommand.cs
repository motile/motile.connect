﻿namespace motile.Connect.CQ.OleDb
{
    public class OleDbSQLDeleteCommand : OleDbBaseCommand
    {
        #region - ctr
        public OleDbSQLDeleteCommand(SqlDeleteInfo deleteInfo, string info)
            : base(GetSQL(deleteInfo), info)
        {
        
        }
        #endregion

        #region - methods
        public static string GetSQL(SqlDeleteInfo deleteInfo)
        {
            return string.Format("DELETE [{0}].* FROM [{0}] WHERE [{0}].[{1}]={2}", deleteInfo.Table, deleteInfo.ColumnId, deleteInfo.Id);
        }
        #endregion
    }
}
