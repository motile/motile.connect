﻿namespace motile.Connect.CQ.OleDb
{
    public class SqlUpdateInfo
    {
        public SqlUpdateInfo(string table, string columnID, string columnValue, string id, string newValue)
        {
            this.Table = table;
            this.ColumnId = columnID;
            this.ColumnValue = columnValue;
            this.Id = id;
            this.NewValue = newValue;
        }


        public static SqlUpdateInfo Create<U, V>(string table, string columnID, string columnValue, U id, V newValue)
        {
            return new SqlUpdateInfo(table, columnID, columnValue, SqlValueHelper.EscapeValue(id), SqlValueHelper.EscapeValue(newValue));
        }

        public string Id { get; private set; }
        public string Table { get; private set; }
        public string ColumnValue { get; private set; }
        public string ColumnId { get; private set; }
        public string NewValue { get; private set; }
    }
}
