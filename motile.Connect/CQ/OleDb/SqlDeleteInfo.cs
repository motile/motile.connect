﻿
namespace motile.Connect.CQ.OleDb
{
    public class SqlDeleteInfo
    {
        public SqlDeleteInfo(string table, string columnId, string id)
        {
            this.Id = id;
            this.Table = table;
            this.ColumnId = columnId;
        }

        public string Id { get; private set; }
        public string Table { get; private set; }
        public string ColumnId { get; private set; }

        public static SqlDeleteInfo Create<U>(string table, string columnID, U id)
        {
            return new SqlDeleteInfo(table, columnID, SqlValueHelper.EscapeValue(id));
        }
    }
}
