﻿using System;

namespace motile.Connect.CQ.OleDb
{
    public static class SqlValueHelper
    {
        public static string EscapeValue<T>(T value)
        {
            string result = string.Empty;

            if (typeof(T) == typeof(string))
            {
                if (value == null)
                {
                    result = "null";
                }
                else
                {
                    result = string.Format("'{0}'", value);
                }
            }
            else if (typeof(T) == typeof(bool))
            {
                result = ((bool)(object)value) ? "-1" : "0";
            }
            else if (typeof(T) == typeof(DateTime))
            {
                result = string.Format("CDate('{0:yyyy.MM.dd HH:mm:ss}')", value);
            }
            else if (typeof(T) == typeof(DateTime?))
            {
                var d = (DateTime?)(object)value;
                if (d.HasValue)
                    result = EscapeValue(d.Value);
                else
                    result = "null";
            }
            else if (typeof(T) == typeof(double) || typeof(T) == typeof(float))
            {
                result = value.ToString();
            }
            else if (value != null)
            {
                result = value.ToString();
            }
            return result;
        }
    }
}
