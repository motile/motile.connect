﻿namespace motile.Connect.CQ.OleDb
{
    public class OleDbSQLUpdateCommand : OleDbBaseCommand
    {
        #region - ctr
        public OleDbSQLUpdateCommand(SqlUpdateInfo updateInfo, string info)
            : base(GetSQL(updateInfo), info)
        {
            
        }
        #endregion

        #region - methods
        public static string GetSQL(SqlUpdateInfo updateInfo)
        {
            return string.Format("UPDATE [{0}] SET [{0}].[{1}] = {2} WHERE [{0}].[{3}] = {4}", updateInfo.Table, updateInfo.ColumnValue, updateInfo.NewValue, updateInfo.ColumnId, updateInfo.Id);
        }
        #endregion
    }
}
