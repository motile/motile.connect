﻿using System.Threading.Tasks;

namespace motile.Connect.CQ
{
    public interface IQueryEnginePart<T, Q>
        where Q : Query<T>
    {
        Task<QueryResult<T>> QueryAsync(Q query);
    }


    public interface IQueryEngine
    {
        Task<QueryResult<T>> QueryAsync<T>(Query<T> query);
    }
}
