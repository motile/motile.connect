﻿using System;

namespace motile.Connect.CQ
{
    public abstract class Query : IEquatable<Query>
    {
        #region - IEquatable<Entry> Members
        public bool Equals(Query other)
        {
            var result = !object.ReferenceEquals(other, null)
                && this.ToString() == other.ToString();

            return result;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var selEnt = obj as Query;
            if (!object.ReferenceEquals(selEnt, null))
            {
                return Equals(selEnt);
            }
            else
            {
                return false;
            }
        }

        public static bool operator ==(Query i1, Query i2)
        {
            bool equals = (object.ReferenceEquals(i1, null)) && (object.ReferenceEquals(i2, null));

            if (!object.ReferenceEquals(i1, null))
            {
                equals = i1.Equals(i2);
            }

            return equals;
        }

        public static bool operator !=(Query i1, Query i2)
        {
            return !(i1 == i2);
        }


        public abstract string AllPropsToUniqueString();


        public override string ToString()
        {
            return AllPropsToUniqueString();
        }
        #endregion
    }

    public abstract class Query<T> : Query
    {

    }

    public class DtoQuery<T> : Query<T>
        where T : IDTO
    {
        public DtoQuery(DtoKey key)
        {
            this.Key = key;
        }
        
        public DtoKey Key { get; private set; }

        public override string AllPropsToUniqueString()
        {
            return $"dto{Key.TypeKey}:{Key.Id}";
        }
    }
}
