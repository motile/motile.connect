# required parameters :
#       $buildNumber
Framework "4.5.1"

properties {
	# build properties - change as needed
	$baseDir = resolve-path .\
	$sourceDir = "$baseDir"
	$buildDir = "$baseDir\build"
	# change as needed, TestResults is used by TeamCity
	$testDir = "$buildDir\TestResults"
	$packageDir = "$buildDir\package"
	
	$companyName = "motile users Software GmbH"
	$projectName = "motile.Connect"
	$projectConfig = "Release"	
	
	$projectAssemblies = @(
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.dll",
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.pdb",		
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.Autofac.dll",
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.Autofac.pdb",		
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.Connexia.dll",
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.Connexia.pdb",		
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.Connexia.Wpf.dll",
		"$sourceDir\motile.Connect.Connexia.Wpf\bin\$projectConfig\motile.Connect.Connexia.Wpf.pdb"
	)	

	# if not provided, default to 1.0.0.0
	if(!$version)
	{
		$version = "1.0.0.0"
	}

	# tools
	# change testExecutable as needed, defaults to mstest    
	$testExecutable = "$baseDir\packages\xunit.runner.console.2.2.0\tools\xunit.console.exe"
	
	$nuget = "$sourceDir\packages\NuGet.CommandLine.2.8.5\tools\NuGet.exe"

	# if not provided, default to Dev
	if (!$nuGetSuffix)
	{
		$nuGetSuffix = "Dev"
	}

	# nuspec files    
	$nugetName = $projectName + ".Wpf"
	$projectSymbolsNuspec = "$packageDir\$projectName.symbols.nuspec"
	$projectNuspecTitle = $nugetName
	$projectNuspecDescription = $nugetName

	# deploy scripts
	$projectDeployFile = "$buildDir\Deploy-$projectName.ps1"
}

task default -depends Test, PackageNuGet

# Initialize the build, delete any existing package or test folders
task Init {
	Write-Host "Deleting the package directory"
	DeleteFile $packageDir
	Write-Host "Deleting the test directory"
	DeleteFile $testDir
}

# Builds the Project solution and any other solutions necessary
task Build -depends Init {
	Write-Host "Cleaning the solution"
	exec { msbuild /t:clean /v:q /nologo /p:Configuration=$projectConfig $sourceDir\$projectName.sln }
	DeleteFile $error_dir
	Write-Host "Building the solution"
	exec { msbuild /t:build /v:q /nologo /p:Configuration=$projectConfig $sourceDir\$projectName.sln }
}

# Execute unit tests
# Change as necessary if using a different test tool
task Test -depends Build  {
	exec { & $testExecutable  "$sourceDir\motile.Connect.Tests\bin\$projectConfig\motile.Connect.Tests.dll" } 
	exec { & $testExecutable  "$sourceDir\motile.Connect.Connexia.Tests\bin\$projectConfig\motile.Connect.Connexia.Tests.dll" } 
}
	
# Create a common assembly info file to be shared by all projects with the provided version number
task CommonAssemblyInfo {
	$version = "1.0.0.0" 	
	CreateCommonAssemblyInfo "$version" $nugetName "$sourceDir\CommonAssemblyInfo.cs"
}

# Package the project web code
# Copy only the necessary files, exclude .cs files
 #
task PackageProject -depends Build {
   Write-Host "Packaging $projectName"
   
   CreateDirectory $packageDir

   $projectAssemblies | Copy-Item -Destination $packageDir

   CreateDirectory ($packageDir + "\src")

   Copy-Item -Path ($sourceDir + "\motile.connect") -Filter "*.cs"  -Destination ($packageDir + "\src") -Recurse -Force
   DeleteDirectory ($packageDir + "\src\motile.connect\obj")
   DeleteDirectory ($packageDir + "\src\motile.connect\bin")
   
   Copy-Item -Path ($sourceDir + "\motile.connect.autofac") -Filter "*.cs"  -Destination ($packageDir + "\src") -Recurse -Force
   DeleteDirectory ($packageDir + "\src\motile.connect.autofac\obj")
   DeleteDirectory ($packageDir + "\src\motile.connect.autofac\bin")   
   
   Copy-Item -Path ($sourceDir + "\motile.connect.connexia") -Filter "*.cs"  -Destination ($packageDir + "\src") -Recurse -Force
   DeleteDirectory ($packageDir + "\src\motile.connect.connexia\obj")
   DeleteDirectory ($packageDir + "\src\motile.connect.connexia\bin")   

   Copy-Item -Path ($sourceDir + "\motile.connect.connexia.wpf") -Filter "*.cs"  -Destination ($packageDir + "\src") -Recurse -Force
   DeleteDirectory ($packageDir + "\src\motile.connect.connexia.wpf\obj")
   DeleteDirectory ($packageDir + "\src\motile.connect.connexia.wpf\bin")
}

# The Package task depends on all other package tasks
# Template only includes one package task (PackageProject).
# If you need to package multiple solutions, add them as dependencies for Package
task Package -depends PackageProject { #, PackageApiProject, PackageDatabase {
}

# PackageNuGet creates the NuGet packages for each package needed to deploy the solution
task PackageNuGet -depends Package  {    
	Write-Host "Create $nugetName nuget manifest"
	
	CreateNuGetSymnbolsManifest $version $nugetName $projectSymbolsNuspec $projectNuspecTitle $projectNuspecDescription	
	exec { & $nuget pack $projectSymbolsNuspec -OutputDirectory $packageDir -Verbosity quiet -Symbols}
}

task DeployNuGet -depends Test, PackageNuGet  {
	$packageFileName = $packageDir + "\" + $nugetName + "." + $version + ".nupkg"
	
	.nuget\nuget push $packageFileName -Source https://www.myget.org/F/motile/api/v2/package

	$packageSymbolFileName = $packageDir + "\" + $nugetName + "." + $version + ".symbols.nupkg"
	.nuget\nuget push $packageSymbolFileName -Source https://www.myget.org/F/motile/api/v2/package
}

#

# ------------------------------------------------------------------------------------#
# Utility methods
# ------------------------------------------------------------------------------------#

# Copy files needed for a website, ignore source files and other unneeded files
function global:CopyWebSiteFiles($source, $destination){
	$exclude = @('*.user', '*.dtd', '*.tt', '*.cs', '*.csproj', '*.orig', '*.log')
	CopyFiles $source $destination $exclude
	DeleteDirectory "$destination\obj"
}

# copy files to a destination
# create the directory if it does not exist
function global:CopyFiles($source, $destination, $exclude = @()){    
	CreateDirectory $destination
	Get-ChildItem $source -Recurse -Exclude $exclude | Copy-Item -Destination { Join-Path $destination $_.FullName.Substring($source.length); }
}

# Create a directory
function global:CreateDirectory($directoryName)
{
	mkdir $directoryName -ErrorAction SilentlyContinue | Out-Null
}

# Delete a directory
function global:DeleteDirectory($directory_name)
{
	rd $directory_name -recurse -force -ErrorAction SilentlyContinue | Out-Null
}

# Delete a file if it exists
function global:DeleteFile($file) {
	if ($file)
	{
		Remove-Item $file -force -recurse -ErrorAction SilentlyContinue | Out-Null
	}
}


function global:CreateNuGetSymnbolsManifest($version, $applicationName, $filename, $title, $description)
{
"<?xml version=""1.0""?>
<package xmlns=""http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd"">
  <metadata>
	<id>$applicationName</id>
	<title>$title</title>
	<version>$version</version>
	<authors>$companyName</authors>
	<owners>$companyName</owners>    
	<requireLicenseAcceptance>false</requireLicenseAcceptance>
	<description>$description</description>     
	 <dependencies>
	  <group targetFramework=""net45"">
		<dependency id=""Autofac"" version=""4.6"" />
		<dependency id=""Caliburn.Micro"" version=""3.1"" />		
		<dependency id=""Dapper"" version=""1.50"" />
		<dependency id=""FluentValidation"" version=""7.1"" />
		<dependency id=""Newtonsoft.Json"" version=""10.0"" />		
	  </group>
	</dependencies>   
  </metadata>
   <files>
	<file src=""motile.Connect.dll"" target=""lib\net45"" />
	<file src=""motile.Connect.pdb"" target=""lib\net45"" />	
	<file src=""motile.Connect.Autofac.dll"" target=""lib\net45"" />
	<file src=""motile.Connect.Autofac.pdb"" target=""lib\net45"" />
	<file src=""motile.Connect.Connexia.dll"" target=""lib\net45"" />
	<file src=""motile.Connect.Connexia.pdb"" target=""lib\net45"" />
	<file src=""motile.Connect.Connexia.Wpf.dll"" target=""lib\net45"" />
	<file src=""motile.Connect.Connexia.Wpf.pdb"" target=""lib\net45"" />
	<file src=""**\*.cs"" target=""src\"" />		
  </files>
</package>" | Out-File $filename -encoding "ASCII"
}

# Create a CommonAssemblyInfo file
function global:CreateCommonAssemblyInfo($version, $applicationName, $filename)
{
"using System;
using System.Reflection;
using System.Runtime.InteropServices;

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4927
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: ComVisibleAttribute(false)]
[assembly: AssemblyVersionAttribute(""$version"")]
[assembly: AssemblyFileVersionAttribute(""$version"")]
[assembly: AssemblyCopyrightAttribute(""Copyright 2015"")]
[assembly: AssemblyProductAttribute(""$applicationName"")]
[assembly: AssemblyCompanyAttribute(""motile users Software GmbH"")]
[assembly: AssemblyConfigurationAttribute(""release"")]
[assembly: AssemblyInformationalVersionAttribute(""$version"")]"  | Out-File $filename -encoding "ASCII"    
}



