﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect
{    
    public class ConsoleDialog : IDialogManager
    {
        public Task<string> AskAsync(string message, string[] options, string defaultAnswer = null)
        {
            string result = defaultAnswer;

            if (options != null && options.Length > 0)
            {
                Console.WriteLine(message);
                for (var i = 1; i <= options.Length; i++)
                {
                    Console.WriteLine(string.Format("{0}: {1}", i, options[i - 1]));
                }

                if (!string.IsNullOrEmpty(defaultAnswer))
                {
                    Console.WriteLine(string.Format("default: {0}", defaultAnswer));
                }

                Console.Write(string.Format("Wählen Sie eine Option (1-{0}):", options.Length));
                
                var answer = Console.ReadLine();

                if (!string.IsNullOrEmpty(answer))
                {
                    int option;

                    if (int.TryParse(answer, out option))
                    {
                        if (option > 0 && option <= options.Length)
                        {
                            result = options[option - 1];
                        }
                    }
                }
            }

            return Task.FromResult<string>(result);

        }
    }
}
