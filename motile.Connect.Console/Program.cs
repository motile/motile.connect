﻿using motile.Connect.Demo;
using System;
using System.IO;
using System.Threading.Tasks;

namespace motile.Connect
{
    class Program
    {
        private static string cPathInbox = "inbox";


        static void Main(string[] args)
        {
            var path = @"c:\schnittstellendemo\";
            var pathInbox = Path.Combine(path, cPathInbox);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var connectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}vgkvdat.mdb", path);

            using (var demonstrator = ResolveDemonstrator(path, connectionString))
            {
                var task = demonstrator.Init();

                task.Wait();
            }

            for (;;)
            {
                using (var demonstrator = ResolveDemonstrator(path, connectionString))
                {
                    var t = demonstrator.Check(pathInbox);
                    t.Wait();
                }

                var input = Console.ReadKey();

                if (input.Key == ConsoleKey.Escape)
                {
                    break;
                }

            }
        }


        private static Demonstrator ResolveDemonstrator(string path, string connectionString)
        {
            var demonstrator = new Demonstrator(path, connectionString, new ConsoleDialog(), new Action<string>(x => Console.WriteLine(x)));

            return demonstrator;
        }
    }
}

