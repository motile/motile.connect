﻿using System.Collections.Generic;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace motile.Connect.Connexia
{
    public class CityProvider
    {
        public class City
        {
            public string Code { get; set; }
            public string ConnexiaCode { get; set; }
            public string Name { get; set; }
        }

        #region - fields
        private static List<City> _cities;  //einfach gestricktes singleton
        #endregion

        #region - ctr
        public CityProvider(ConnexiaSettings settings)
        {
            LoadCitiesFromConnexia(settings.ConnectionString);
        }
        #endregion

        public IEnumerable<City> Cities { get { return _cities; } }

        #region - methods

        private void LoadCitiesFromConnexia(string connectionString)
        {
            if (_cities == null)
            {
                _cities = new List<City>();
                using (var connection = new OleDbConnection(connectionString))
                {
                    // teilweise ist der Ort mit Zusatz versehen (z.b. Hard, Vorarlberg). Der Zusatz soll vernachlässigt werden.
                    var ortRegex = new Regex("^(?<ort>[^,]+)(?<zusatz>.*?)$");

                    // teilweise sind Postleitzahlen mit "/" enthalten. Nur 4- oder 6-stellige Zahlen erlauben
                    var plzRegex = new Regex(@"^\d{4}(\d{2})?$");

                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT Postleitzahl, Ort FROM tb_Orte";

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var plz = reader.GetFieldValue<string>(0);
                            var ort = reader.GetFieldValue<string>(1);

                            if (plzRegex.IsMatch(plz))
                            {
                                if (!string.IsNullOrEmpty(ort))
                                {
                                    var m = ortRegex.Match(ort);
                                    if (m.Success)
                                    {
                                        ort = m.Groups["ort"].Value;
                                    }

                                    ort = ort.Trim();
                                }

                                _cities.Add(new City()
                                {
                                    Code = plz.Length > 4 ? plz.Substring(0, 4) : plz,
                                    ConnexiaCode = plz,
                                    Name = ort
                                });
                            }
                        }

                        reader.Close();
                    }
                    connection.Close();
                }
            }
        }
        #endregion
    }
}
