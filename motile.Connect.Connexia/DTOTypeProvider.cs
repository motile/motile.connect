﻿using motile.Connect.CQ;
using System;

namespace motile.Connect.Connexia
{
    public class DTOTypeProvider : IDTOTypeProvider
    {
        public const string ConnexiaFall = "ConnexiaFall";
        public const string ConnexiaFallListe = "ConnexiaFallListe";
        public const string ConnexiaLeistung = "ConnexiaLeistung";

        public const string ConnexiaPfleger = "ConnexiaPfleger";        
        public const string ConnexiaDBState = "ConnexiaDBState";

        public static DtoKey CreateKey<T>(string id)
        {
            if (typeof(T) == typeof(ConnexiaFall))
                return new DtoKey(ConnexiaFall, id);

            if (typeof(T) == typeof(ConnexiaFallListe))
                return new DtoKey(ConnexiaFallListe, id);

            if (typeof(T) == typeof(ConnexiaLeistung))
                return new DtoKey(ConnexiaLeistung, id);

            if (typeof(T) == typeof(ConnexiaPfleger))
                return new DtoKey(ConnexiaPfleger, id);
            
            if (typeof(T) == typeof(ConnexiaDBState))
                return new DtoKey(ConnexiaDBState, id);
            
            throw new NotImplementedException();

        }

        public static DtoQuery<T> CreateQuery<T>(string id)
            where T : IDTO
        {
            return new DtoQuery<T>(CreateKey<T>(id));
        }

        DtoQuery<T> IDTOTypeProvider.CreateQuery<T>(string id)            
        {
            return CreateQuery<T>(id);
        }
        

        public Type GetTypeFromKey(string typeKey)
        {
            Type result = null;
            switch (typeKey)
            {
                case ConnexiaFall:
                    result = typeof(ConnexiaFall);
                    break;
                case ConnexiaFallListe:
                    result = typeof(ConnexiaFallListe);
                    break;
                case ConnexiaLeistung:
                    result = typeof(ConnexiaLeistung);
                    break;
                case ConnexiaPfleger:
                    result = typeof(ConnexiaPfleger);
                    break;                
                case ConnexiaDBState:
                    result = typeof(ConnexiaDBState);
                    break;
            }

            return result;
        }
    }
}
