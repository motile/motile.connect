﻿using Autofac;
using FluentValidation;
using motile.Connect.Autofac;
using motile.Connect.Validation;
using System.Linq;
using System.Reflection;

namespace motile.Connect.Connexia
{
    public class ConnexiaContainerProviderDiff : ConnectContainerProviderBase
    {

        #region - ctr
        public ConnexiaContainerProviderDiff(IDialogManager dlgMgr)
            : base(dlgMgr)
        {

        }
        #endregion

        protected override void ConfigBuilder(ContainerBuilder builder)
        {
            //FindDiffs
            builder.RegisterType<ConnexiaDBStateFindDifferencesStrategy>().As<IFindDifferencesStrategy<ConnexiaDBState>>();

            builder.RegisterType<ConnexiaFallListeFindDifferencesStrategy>().As<IFindDifferencesStrategy<ConnexiaFallListe>>();

            builder.RegisterType<ConnexiaFallFindDifferencesStrategy>().As<IFindDifferencesStrategy<ConnexiaFall>>();

            builder.RegisterType<ConnexiaLeistungFindDifferencesStrategy>().As<IFindDifferencesStrategy<ConnexiaLeistung>>();

            builder.RegisterType<ConnexiaFindDifferencesStrategy>().As<IFindDifferencesStrategy<ConnexiaPfleger>>();


            builder.RegisterType<ConnexiaFallCategorizer>().AsSelf();

            builder.Register(c => new ConnexiaFallCachedCategorizer(c.Resolve<ConnexiaFallCategorizer>()))
                .AsSelf()
                .As<IConnexiaFallCategorizer>();

            builder.RegisterType<ConnexiaFallAdressnummerCategorizer>().As<IConnexiaFallAdressnummerCategorizer>();


            var asm = Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(asm)
                   .Where(t => t.GetInterfaces().Any() && t.GetInterfaces()[0] == typeof(IValidator<ConnexiaFall>))
                   .As<IValidator<ConnexiaFall>>();

            builder.RegisterAssemblyTypes(asm)
                   .Where(t => t.GetInterfaces().Any() && t.GetInterfaces()[0] == typeof(IValidator<ConnexiaLeistung>))
                   .As<IValidator<ConnexiaLeistung>>();

            builder.RegisterAssemblyTypes(asm)
                  .Where(t => t.GetInterfaces().Any() && t.GetInterfaces()[0] == typeof(IValidator<ConnexiaFallListe>))
                  .As<IValidator<ConnexiaFallListe>>();

            builder.RegisterType<ValidatorAggregator<ConnexiaFallListe>>().AsSelf();
            builder.RegisterType<ValidatorAggregator<ConnexiaFall>>().AsSelf();
            builder.RegisterType<ValidatorAggregator<ConnexiaLeistung>>().AsSelf();
            builder.RegisterType<ValidatorAggregator<ConnexiaPfleger>>().AsSelf();

            builder.RegisterType<DTOTypeProvider>().As<IDTOTypeProvider>();

            //allgemeine DescriptionProvider
            builder.RegisterType<DTODescriptionProvider>().As<IDTODescriptionProvider>();
            builder.RegisterGeneric(typeof(DTODefaultDescriptionProvider<>)).As(typeof(IDTODescriptionProvider<>));
            //spezielle DescriptionProvider
            builder.RegisterType<ConnexiaFallListeDescriptionProvider>().As<IDTODescriptionProvider<ConnexiaFallListe>>();
            builder.RegisterType<ConnexiaLeistungDescriptionProvider>().As<IDTODescriptionProvider<ConnexiaLeistung>>();

            builder.RegisterType<NameOfAddressProvider>().InstancePerMatchingLifetimeScope(CompositionHelper.LifetimeTag).AsSelf();
            builder.RegisterType<NameOfPflegerProvider>().InstancePerMatchingLifetimeScope(CompositionHelper.LifetimeTag).AsSelf();

            builder.RegisterType<PflegerIsinRoleProvider>()
                .As<IPflegerIsinRoleProvider>()
                .SingleInstance()
                .IfNotRegistered(typeof(IPflegerIsinRoleProvider));
            
            base.ConfigBuilder(builder);
        }
    }
}
