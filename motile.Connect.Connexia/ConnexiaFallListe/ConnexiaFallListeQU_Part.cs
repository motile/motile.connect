﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallListeQU_Part : ConnexiaBaseQE_Part<ConnexiaFallListe>
    {
        #region - ctr
        public ConnexiaFallListeQU_Part(OleDbConnection connection)
            : base(connection)
        {

        }
        #endregion

        #region - public methods
        public async Task<bool> IsAddressInArchive(int id)
        {
            bool result = false;

            var row = (await _connection.QueryAsync("SELECT Archiv FROM Adressen WHERE Adressnummer = @adressnummer", new { adressnummer = id })).FirstOrDefault();

            result = row != null && !string.IsNullOrEmpty(row.Archiv);

            return result;
        }
        #endregion

        #region - private methods
        protected override async Task<ConnexiaFallListe> GetDTOAsync(string id, OleDbConnection connection)
        {
            DateTime von;
            DateTime bis;
            bool nurAktive;

            ConnexiaFallListe.GetIDFaktoren(id, out von, out bis, out nurAktive);

            var result = new ConnexiaFallListe() { Von = von, Bis = bis, NurAktive = nurAktive };

            var sqlWhereOr = new List<string>();

            if (nurAktive)
            {

                //Leistungsdaten oder Doku im entsprechenden Zeitraum
                sqlWhereOr.Add("EXISTS(SELECT * FROM Leistungen AS l " +
                    "WHERE a.Adressnummer = l.Adressnummer and l.Datum is not null " +  //falsche Datensätze mit l.Datum null ausschließen
                    " and ((l.übertragen_am is null and l.Datum BETWEEN #2018/01/01# AND @to) or l.Datum BETWEEN @from AND @to))");

                sqlWhereOr.Add("EXISTS(SELECT * FROM Doku AS d WHERE a.Adressnummer = d.Adressnummer and ((d.übertragen_am is null and d.Datum BETWEEN #2018/01/01# AND @to) or d.Datum BETWEEN @from AND @to))");
            }
            else
            {
                if (bis < DateTime.Today || von < DateTime.Today.AddMonths(-2)) // betrifft die Vergangenheit
                {
                    sqlWhereOr.Add("EXISTS(SELECT * FROM Leistungen AS l WHERE a.Adressnummer = l.Adressnummer and l.Datum BETWEEN @from AND @to)");
                }

                if (bis.AddMonths(1) > DateTime.Today) // betrifft (zumindes beinahe) aktuelle daten
                {
                    sqlWhereOr.Add("(((IIf(isnull([archiv]),'',[archiv]))=''))");
                }
            }

            var sql = string.Format(@"SELECT Adressnummer, Name_1 FROM Adressen AS a WHERE NOT (((IIf(isnull([Name_1]),'',[Name_1]))='')) AND ({0})", sqlWhereOr.AggregateToString(" OR "));

            IEnumerable<dynamic> rows = await connection.QueryAsync(sql.ToString(), new { from = von, to = bis });

            foreach (var row in rows)
            {
                result.Adressnummern.Add(row.Adressnummer, row.Name_1);
            }

            return result;
        }
        #endregion
    }
}
