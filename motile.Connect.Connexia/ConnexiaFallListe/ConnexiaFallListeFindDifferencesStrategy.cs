﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallListeFindDifferencesStrategy : IFindDifferencesStrategy<ConnexiaFallListe>
    {
        public IEnumerable<DiffsPack> GetDifferences(ConnexiaFallListe master, ConnexiaFallListe changeThis)
        {
            Contract.Equals(master.Von, changeThis.Von);
            Contract.Equals(master.Bis, changeThis.Bis);

            var entriesToAdd = master.Adressnummern.Select(x => x.Key).Except(changeThis.Adressnummern.Select(x => x.Key)).ToList();
            var entriesToRemove = changeThis.Adressnummern.Select(x => x.Key).Except(master.Adressnummern.Select(x => x.Key)).ToList();

            foreach (var remove in entriesToRemove)
            {
                yield return new DiffsPack(master.DtoKey, RemoveEntryDifference.Create(changeThis, x => x.Adressnummern, changeThis.Adressnummern.Where(x => x.Key == remove).Single()));
            }

            foreach (var add in entriesToAdd)
            {
                yield return new DiffsPack(master.DtoKey, AddEntryDifference.Create(changeThis, x => x.Adressnummern, master.Adressnummern.Where(x => x.Key == add).Single()));
            }
        }
    }
}
