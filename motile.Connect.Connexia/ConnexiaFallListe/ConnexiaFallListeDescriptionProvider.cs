﻿using System;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallListeDescriptionProvider : IDTODescriptionProvider<ConnexiaFallListe>
    {
        public Task<string> WithIDAsync(string id)
        {
            DateTime von;
            DateTime bis;
            bool nurAktive;

            ConnexiaFallListe.GetIDFaktoren(id, out von, out bis, out nurAktive);

            return Task.FromResult("Adressliste");
        }
    }
}
