﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace motile.Connect.Connexia
{
    [DataContract]
    public class ConnexiaFallListe : IDTO
    {

        public ConnexiaFallListe()
        {
            Adressnummern = new Dictionary<int, string>();
        }

        [DataMember]
        public DateTime Von { get; set; }

        [DataMember]
        public DateTime Bis { get; set; }

        [DataMember]
        public Dictionary<int, string> Adressnummern { get; set; }

        public bool NurAktive { get; set; }

        public DtoKey DtoKey
        {
            get { return new DtoKey(DTOTypeProvider.ConnexiaFallListe, GetIDAsString(this.Von, this.Bis, this.NurAktive)); }
        }

        private static Regex _idPattern = new Regex(@"^(?<von>\d+)_(?<bis>\d+)(?<aktive>\+nurAktive)?$");
        public static void GetIDFaktoren(string id, out DateTime von, out DateTime bis, out bool nurAktive)
        {
            var m = _idPattern.Match(id);

            von = DateTime.ParseExact(m.Groups["von"].Value, "yyyyMMdd", CultureInfo.InvariantCulture);
            bis = DateTime.ParseExact(m.Groups["bis"].Value, "yyyyMMdd", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59);
            nurAktive = !(string.IsNullOrEmpty(m.Groups["aktive"].Value));
        }

        public static string GetIDAsString(DateTime von, DateTime bis, bool nurAktive = false)
        {
            return nurAktive ? string.Format("{0:yyyyMMdd}_{1:yyyyMMdd}+nurAktive", von, bis) : string.Format("{0:yyyyMMdd}_{1:yyyyMMdd}", von, bis);
        }
    }
}
