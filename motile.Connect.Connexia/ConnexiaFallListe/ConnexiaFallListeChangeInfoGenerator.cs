﻿using motile.Connect.CQ;
using motile.Connect.CQ.OleDb;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallListeChangeInfoGenerator : IChangeInfoGenerator<ConnexiaFallListe>
    {
        #region - fields
        private string cachedAnswer = string.Empty;
        private ConnexiaFallListeQU_Part _connexiaFallListeRepository { get; set; }

        private IDialogManager _dialogManager { get; set; }
        #endregion

        #region - ctr
        public ConnexiaFallListeChangeInfoGenerator(ConnexiaFallListeQU_Part connexiaFallListeRepository, IDialogManager dialogManager)
        {
            this._connexiaFallListeRepository = connexiaFallListeRepository;
            this._dialogManager = dialogManager;
        }
        #endregion

        #region - methods
        public async Task<IEnumerable<ChangeInfo>> Process(IEnumerable<DiffsPack> diffsPacks)
        {
            var result = new List<ChangeInfo>();

            foreach (var diffsPack in diffsPacks)
            {
                var diffs = diffsPack.Diffs.ToList();


                var cmds = new List<Command>();

                while (diffs.Any())
                {
                    var diff = diffs[0];
                    diffs.RemoveAt(0);

                    var path = diff.Path;

                    if (diff.IsPath<ConnexiaFallListe, Dictionary<int, string>>((x) => x.Adressnummern))
                    {
                        if (diff is AddEntryDifference)
                        {
                            var newentry = (KeyValuePair<int, string>)((AddEntryDifference)diff).NewEntry;

                            var isInArchive = await _connexiaFallListeRepository.IsAddressInArchive(newentry.Key);
                            if (isInArchive)
                            {
                                var updateInfo = SqlUpdateInfo.Create<int, string>("Adressen", "Adressnummer", "Archiv", newentry.Key, null);

                                cmds.Add(new OleDbSQLUpdateCommand(updateInfo, $"Adresse aus dem Archiv ({newentry.Key}, {newentry.Value})"));
                            }
                            else
                            {
                                var insertInfo = SqlInsertInfo.Create("Adressen", "Adressnummer", newentry.Key, new[] { "Name_1" }, new[] { SqlValueHelper.EscapeValue(newentry.Value) });

                                cmds.Add(new OleDbSQLInsertCommand(insertInfo, $"neue Adresse ({newentry.Value}, {newentry.Key})"));

                                cmds.Add(new OleDbBaseCommand(CheckLaufnummer(newentry.Key), $"Überprüfe Adressnummer ({newentry.Key}, {newentry.Value})"));
                            }
                        }
                        else if (diff is RemoveEntryDifference)
                        {
                            var remove = (KeyValuePair<int, string>)((RemoveEntryDifference)diff).RemoveEntry;

                            var message = $"Eine Adresse ({remove.Key}, {remove.Value}) ist nicht in der aktuellen Aufstellung. Wie möchten Sie diese Adresse behandeln?";

                            var answer = string.Empty;

                            if (string.IsNullOrEmpty(cachedAnswer))
                            {
                                answer = await _dialogManager.AskAsync(message, new[] { "archivieren", "alle archivieren" }, "archivieren");
                            }
                            else
                            {
                                answer = cachedAnswer;
                            }

                            OleDbBaseCommand cmd = null;

                            if (string.Equals(answer, "archivieren", StringComparison.CurrentCultureIgnoreCase) || string.Equals(answer, "alle archivieren", StringComparison.CurrentCultureIgnoreCase))
                            {
                                var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Archiv", remove.Key, "P");
                                cmd = new OleDbSQLUpdateCommand(updateInfo, $"Adresse ({remove.Key}, {remove.Value}) archivieren");

                                if (string.Equals(answer, "alle archivieren", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    cachedAnswer = "archivieren";
                                }
                            }
                            else
                            {
                                break;
                            }

                            if (cmd != null)
                            {
                                cmds.Add(cmd);
                            }
                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }

                result.Add(new ChangeInfo(diffsPack, cmds.ToArray()));
            }

            return result;
        }

        private string CheckLaufnummer(int adressnummer)
        {
            /*
            siehe igk_2002_gnv.mdb
            X = get_laufnummer("adrnr")
            ...
            SQL_Command = SQL_Command & gl_Laufnummer * -1 & "','"
            ...
            */
            var laufnummer = adressnummer * -1;
            Contract.Assert(laufnummer >= 0);

            var cmdText = $"Update Laufnummern set Wert = {laufnummer} where Art = 'adrnr' and Wert < {laufnummer}";

            return cmdText;
        }
        #endregion
    }
}
