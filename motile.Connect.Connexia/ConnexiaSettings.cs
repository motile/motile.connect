﻿using System;
using System.IO;

namespace motile.Connect.Connexia
{

    public class ConnexiaSettings
    {
        public static ConnexiaSettings GetDefault()
        {
            var result = new ConnexiaSettings();

            var sp = new LocalSettingsProvider<LocalConnexiaSettings>(LocalConnexiaSettings.ID);
            var localSettings = sp.GetSettings();
            var db = localSettings.Database;

            if (!string.IsNullOrEmpty(db))
            {
                try
                {
                    NetDriveHelper.EnsureNetworkDriveIsConnected(new FileInfo(db).Directory.FullName);
                }
                catch
                {

                }
                result.ConnectionString = String.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", db);
            }

            return result;
        }

        public ConnexiaSettings()
        {
            OnlyTransdok = true;
        }

        public bool OnlyTransdok { get; set; }

        public string ConnectionString { get; set; }
    }
}
