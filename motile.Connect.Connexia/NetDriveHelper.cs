﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace motile.Connect.Connexia
{
    public static class NetDriveHelper
    {
        [DllImport("mpr.dll")]
        static extern uint WNetGetConnection(string lpLocalName, StringBuilder lpRemoteName, ref int lpnLength);

        public static void EnsureNetworkDriveIsConnected(string path)
        {
            if (!Directory.Exists(path)) // nur notwendig, wenn noch nicht verbunden
            {
                var (isLocal, uncPath, drive) = IsLocalDrive(path);

                if (!isLocal)
                {
                    NetUse(drive, uncPath);
                }
            }
        }

        internal static (bool IsLocal, string UncPath, string Drive) IsLocalDrive(string path)
        {
            bool isLocal = true;  // assume local until disproved

            // strip trailing backslashes from driveName
            var drive = path.Substring(0, 2);

            int length = 256; // to be on safe side 
            StringBuilder networkShare = new StringBuilder(length);
            uint status = WNetGetConnection(drive, networkShare, ref length);

            // does a network share exist for this drive?
            if (networkShare.Length != 0)
            {
                // now networkShare contains a UNC path in format \\MachineName\ShareName
                // retrieve the MachineName portion
                String shareName = networkShare.ToString();
                string[] splitShares = shareName.Split('\\');
                // the 3rd array element now contains the machine name
                if (Environment.MachineName == splitShares[2])
                    isLocal = true;
                else
                    isLocal = false;
            }

            return (isLocal, networkShare.ToString(), drive);
        }

        internal static void NetUse(string drive, string path)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = "/C" + $"net use {drive} {path}"
            };

            var p = Process.Start(startInfo);

            p.WaitForExit();
        }
    }
}
