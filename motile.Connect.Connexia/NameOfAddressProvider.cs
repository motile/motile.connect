﻿using motile.Connect.CQ;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class NameOfAddressProvider
    {
        private readonly IDictionary<int, string> _names = new SortedDictionary<int, string>();
        private readonly AsyncLock _lock = new AsyncLock();
        private readonly IQueryEngine _queryEngine;

        public NameOfAddressProvider(IQueryEngine queryEngine)
        {
            _queryEngine = queryEngine;
        }

        public async Task<string> GetName(int adressnummer)
        {
            string result;
            if (!_names.ContainsKey(adressnummer))
            {
                using (await _lock.LockAsync())
                {
                    var cf = (await _queryEngine.QueryAsync(DTOTypeProvider.CreateQuery<ConnexiaFall>(ConnexiaFall.GetID(adressnummer)))).Results.FirstOrDefault();

                    if (cf != null)
                    {
                        result = $"{cf.Nachname} {cf.Vorname}".Trim();

                    }
                    else
                    {
                        result = $"unbekannte Adresse ({adressnummer})";
                    }
                    _names.Add(adressnummer, result);
                }
            }
            else
            {
                result = _names[adressnummer];
            }

            return result;
        }
    }
}
