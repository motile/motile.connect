﻿using FluentValidation;
using motile.Connect.Validation;

namespace motile.Connect.Connexia.Validation
{
    class ConnexiaFallStammdatenValidator : AbstractValidator<ConnexiaFall>, IValidatorInfo
    {
        public ConnexiaFallStammdatenValidator(IConnexiaFallCategorizer categorizer)
        {
            RuleFor(x => x.Adresse)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();

            RuleFor(x => x.Land)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();

            RuleFor(x => x.PLZ)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .WithErrorMessage("'PLZ' darf nicht leer sein");    //wegen standardmäßigem SplitPascalCase die Meldung hier definieren

            RuleFor(x => x.Ort)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();

            RuleFor(x => x.Geschlecht)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();

            RuleFor(x => x.Staatsbuergerschaft)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();
        }
        
        public string GetValidatorID() => ValidatorID;
        
        public const string ValidatorID = "ConnexiaFallStammdatenValidator";
    }
}
