﻿using FluentValidation;
using FluentValidation.Validators;
using motile.Connect.Validation;
using System.Collections.Generic;
using System.Linq;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaFallDokuRequiredFieldsValidator : AbstractValidator<ConnexiaFall>, IValidatorInfo
    {
        // diese Felder sind zwingend erforderlich
        public static string[] RequiredFields = new[] { "21" };


        public ConnexiaFallDokuRequiredFieldsValidator(IConnexiaFallCategorizer categorizer)
            : base()
        {

            _categorizer = categorizer;

            RuleFor(x => x.Dokus)
                .SetValidator(new MyPropertyValidator())
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaFallDokuRequiredFieldsValidator";
        private readonly IConnexiaFallCategorizer _categorizer;

        #region - PropertyValidator as subclass
        public class MyPropertyValidator : PropertyValidator
        {

            public MyPropertyValidator()
                : base("Fehlender Eintrag für Feld: {Detail}")
            {

            }

            protected override bool IsValid(PropertyValidatorContext context)
            {
                bool result = true;

                var list = context.PropertyValue as IEnumerable<ConnexiaFall.Doku>;


                var data = list.Select(x => x.Gruppe).Where(x => RequiredFields.Contains(x)).Distinct();

                var missing = RequiredFields.Except(data);

                if (missing.Any())
                {
                    result = false;

                    context.MessageFormatter.AppendArgument("Detail", missing.AggregateToString(","));
                }


                return result;
            }
        }
        #endregion
    }
}
