﻿using FluentValidation;
using FluentValidation.Results;
using motile.Connect.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Validation
{

    public class ConnexiaLeistungAuszubildendeValidator : AbstractValidator<ConnexiaLeistung>, IValidatorInfo
    {

        public ConnexiaLeistungAuszubildendeValidator(IPflegerIsinRoleProvider provider, NameOfAddressProvider nameProvider)
            : base()
        {
            this.RuleFor(x => new { x.Pflegernummer, x.Liste })
                .CustomAsync(async (l, c, ct) =>
                    {
                        if (await IsAuszubildend(provider, l.Pflegernummer))
                        {
                            var med = l.Liste.Where(x => x.Leistung >= 6 && x.Leistung <= 10);

                            if (med.Any())
                            {
                                var info = med.GroupBy(x => new { x.Adressnummer, x.Datum })
                                 .Select(async x => $"{x.Key.Datum:dd.MM.yyyy}, {await this.GetName(nameProvider, x.Key.Adressnummer)} ({x.Key.Adressnummer}): LP {string.Join(", ", x.Select(y => y.Leistung))}")
                                 .ToList();

                                await Task.WhenAll(info);

                                var msg = ValidationInfo.FormatAsErrorMessage($"Pflegernummer {l.Pflegernummer} darf als Auszubildende/r keine medizinischen Leistungen (6-10) dokumentieren.{System.Environment.NewLine}{string.Join(", ", info.Select(x => x.Result))})");

                                var f = new ValidationFailure(nameof(l.Liste), msg);
                                c.AddFailure(f);
                            }
                        }

                    });

        }

        private async Task<string> GetName(NameOfAddressProvider nameProvider, int adressnummer)
        {
            if (nameProvider == null)
                return "???";

            return await nameProvider.GetName(adressnummer);
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaLeistungAuszubildendeValidator";

        public async Task<bool> IsAuszubildend(IPflegerIsinRoleProvider provider, int pflegernummer)
        {
            var result = await provider.IsInRole(pflegernummer, "Auszubildend");

            return result;
        }
    }
}
