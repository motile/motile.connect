﻿using FluentValidation;
using motile.Connect.Validation;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaFallVersicherungValidator : AbstractValidator<ConnexiaFall>, IValidatorInfo
    {
        public ConnexiaFallVersicherungValidator(IConnexiaFallCategorizer categorizer)
        {
            RuleFor(x => x.Versicherungsnummer)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();

            RuleFor(x => x.Versicherungsnummer)
                .Must(x => VNummerHelper.IsValid(x))
                .WithErrorMessage("Die Versicherungsnummer {PropertyValue} ist nicht korrekt")
                .Unless(x => string.IsNullOrEmpty(x.Versicherungsnummer));

            RuleFor(x => x.Versicherung)
                .NotEmpty()
                .Unless(x => string.IsNullOrEmpty(x.Versicherungsnummer))
                .AsErrorMessage();
        }
        
        public string GetValidatorID() => ValidatorID;
        
        public const string ValidatorID = "ConnexiaFallVersicherungValidator";
    }
}
