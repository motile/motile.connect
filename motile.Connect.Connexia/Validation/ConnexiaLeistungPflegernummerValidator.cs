﻿using FluentValidation;
using motile.Connect.Validation;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaLeistungPflegernummerValidator : AbstractValidator<ConnexiaLeistung>, IValidatorInfo
    {
        public ConnexiaLeistungPflegernummerValidator()
        {
            RuleFor(x => x.Pflegernummer)
                .GreaterThan(0)                                
                .WithErrorMessage("Ungültige Pflegernummer!");

            RuleFor(x => x.Pflegernummer)
                .LessThan(30000)
                .WithErrorMessage("Ungültige Pflegernummer!");
        }

        public string GetValidatorID() => ValidatorID;
        
        public const string ValidatorID = "ConnexiaLeistungPflegernummerValidator";
    }
}
