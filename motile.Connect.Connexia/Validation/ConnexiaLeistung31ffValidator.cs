﻿using FluentValidation;
using FluentValidation.Validators;
using motile.Connect.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaLeistung31ffValidator : AbstractValidator<ConnexiaLeistung>, IValidatorInfo
    {

        public ConnexiaLeistung31ffValidator(NameOfAddressProvider nameProvider, IConnexiaFallAdressnummerCategorizer categorizer)
            : base()
        {
            RuleFor(x => x.Liste).SetValidator(new MyPropertyValidator(nameProvider, categorizer)).AsErrorMessage();
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaLeistung31ffValidator";

        #region - PropertyValidator as subclass
        public class MyPropertyValidator : PropertyValidator
        {

            private readonly NameOfAddressProvider _nameProvider;
            private readonly IConnexiaFallAdressnummerCategorizer _categorizer;

            public MyPropertyValidator(NameOfAddressProvider nameProvider, IConnexiaFallAdressnummerCategorizer categorizer)
                : base("{Detail}")
            {
                _nameProvider = nameProvider;
                _categorizer = categorizer;
            }

            public override bool ShouldValidateAsync(ValidationContext context) => true;

            protected override bool IsValid(PropertyValidatorContext context)
            {
                throw new InvalidOperationException();
            }

            protected override async Task<bool> IsValidAsync(PropertyValidatorContext context, CancellationToken cancellation)
            {
                var messages = new StringBuilder();

                var l = context.PropertyValue as IEnumerable<ConnexiaLeistung.Eintrag>;

                var adressnummerMitLeistungen = l
                    .GroupBy(x => x.Adressnummer)
                    .Select(x => new
                    {
                        Adressnummer = x.Key,
                        Leistungen = x.Select(y => y.Leistung).Distinct()
                    });

                foreach (var entry in adressnummerMitLeistungen)
                {
                    if (await _categorizer.GetCategoryAsync(entry.Adressnummer) == ConnexiaFallCategory.NichtKlient)
                    {
                        var invalid = entry.Leistungen.Where(x => !IstBeratungsleistung(x));
                        if (invalid.Any())
                        {
                            var date = l.Where(x => x.Adressnummer == entry.Adressnummer)
                                .Where(x => invalid.Contains(x.Leistung))
                                .Select(x => x.Datum)
                                .FirstOrDefault();

                            var name = await _nameProvider.GetName(entry.Adressnummer);
                            messages.AppendLine($"Beim Beratungsklienten '{name}' wurden ungültige Leistungen dokumentiert! ({invalid.AggregateToString(",")}) [{date.ToString("dd.MM.yyyy")}]");
                        }
                    }
                    else
                    {
                        var invalid = entry.Leistungen.Where(x => IstBeratungsleistung(x));
                        if (invalid.Any())
                        {

                            var date = l.Where(x => x.Adressnummer == entry.Adressnummer)
                                .Where(x => invalid.Contains(x.Leistung))
                                .Select(x => x.Datum)
                                .FirstOrDefault();

                            var name = await _nameProvider.GetName(entry.Adressnummer);
                            messages.AppendLine($"Beim Klient '{name}' wurden ungültige Beratungsleistungen dokumentiert! ({invalid.AggregateToString(",")}) [{date.ToString("dd.MM.yyyy")}]");
                        }
                    }
                }

                context.MessageFormatter.AppendArgument("Detail", messages.ToString().TrimEnd());

                return messages.Length == 0;
            }

            private bool IstBeratungsleistung(int lp)
            {
                return lp >= 31 && lp <= 34;
            }

        }
        #endregion
    }
}
