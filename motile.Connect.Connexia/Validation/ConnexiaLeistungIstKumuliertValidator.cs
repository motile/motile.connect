﻿using FluentValidation;
using FluentValidation.Validators;
using motile.Connect.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaLeistungIstKumuliertValidator : AbstractValidator<ConnexiaLeistung>, IValidatorInfo
    {
        public ConnexiaLeistungIstKumuliertValidator(NameOfAddressProvider nameProvider)
            : base()
        {
            RuleFor(x => x.Liste).SetValidator(new MyPropertyValidator(nameProvider)).AsErrorMessage();
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaLeistungIstKumuliertValidator";

        #region - PropertyValidator as subclass
        public class MyPropertyValidator : PropertyValidator
        {
            private readonly NameOfAddressProvider _nameProvider;

            public MyPropertyValidator(NameOfAddressProvider nameProvider)
                : base("Die Einträge sind nicht kumuliert: {Detail}")
            {
                _nameProvider = nameProvider;
            }

            public override bool ShouldValidateAsync(ValidationContext context) => true;

            protected override bool IsValid(PropertyValidatorContext context)
            {
                throw new InvalidOperationException();
            }

            protected override async Task<bool> IsValidAsync(PropertyValidatorContext context, CancellationToken cancellation)
            {
                var list = context.PropertyValue as IEnumerable<ConnexiaLeistung.Eintrag>;

                var e = list.GroupBy(y => new { y.Datum.Date, y.Leistung, y.Adressnummer }).Where(y => y.Count() > 1).Select(x => x.Key);
                var result = !e.Any();

                if (!result)
                {
                    var namesTasks = e.Select(x => x.Adressnummer)
                      .Distinct().Select(x => new
                      {
                          adressnummer = x,
                          nameTask = _nameProvider != null ? _nameProvider.GetName(x) : Task.FromResult("???")
                      });

                    await Task.WhenAll(namesTasks.Select(x => x.nameTask));

                    var names = namesTasks.ToDictionary(x => x.adressnummer, x => x.nameTask.Result);

                    var msg = e.Select(x => string.Format("{0:dd.MM.yyyy}, {1}, bei {2}", x.Date, x.Leistung, names[x.Adressnummer])).AggregateToString(System.Environment.NewLine);

                    context.MessageFormatter.AppendArgument("Detail", msg);
                }

                return result;
            }
        }
        #endregion
    }
}
