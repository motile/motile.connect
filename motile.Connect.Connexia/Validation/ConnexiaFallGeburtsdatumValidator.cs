﻿using FluentValidation;
using motile.Connect.Validation;
using System;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaFallGeburtsdatumValidator : AbstractValidator<ConnexiaFall>, IValidatorInfo
    {
        public ConnexiaFallGeburtsdatumValidator(IConnexiaFallCategorizer categorizer)
        {
            RuleFor(x => x.Geburtsdatum)
                .NotEmpty()
                .Unless(x => categorizer.GetCategory(x) == ConnexiaFallCategory.NichtKlient)
                .AsErrorMessage();

            RuleFor(x => x.Geburtsdatum)
                .LessThan(DateTime.Today)
                .AsErrorMessage();

            RuleFor(x => x.Geburtsdatum)
               .GreaterThanOrEqualTo(new DateTime(1900, 01, 01))
               .AsErrorMessage();

            RuleFor(x => x.Geburtsdatum)
                .Must(CheckDates)
                .Unless(x => x.Geburtsdatum == null || !VNummerHelper.IsValid(x.Versicherungsnummer))
                .WithWarningMessage(x => $"Das Geburtsdatum {x.Geburtsdatum:dd.MM.yy} unterscheidet sich vom Wert in der Versicherungsnummer {VNummerHelper.Format(x.Versicherungsnummer).Substring(5)}.");
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaFallGeburtsdatumValidator";


        private bool CheckDates(ConnexiaFall fall, DateTime? date)
        {
            if (!date.HasValue) return false;

            var date2 = VNummerHelper.GetBirthDate(fall.Versicherungsnummer);

            if (!date2.HasValue) return true;

            return date2.Value.ToString("ddMMyy") == date.Value.ToString("ddMMyy");
        }        
    }
}
