﻿using FluentValidation;
using FluentValidation.Validators;
using motile.Connect.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaLeistung123Und15 : AbstractValidator<ConnexiaLeistung>, IValidatorInfo
    {
       
        public ConnexiaLeistung123Und15(NameOfAddressProvider nameProvider)
            : base()
        {
            RuleFor(x => x.Liste).SetValidator(new MyPropertyValidator(nameProvider)).AsErrorMessage();
        }
        
        public string GetValidatorID() => ValidatorID;
        
        public const string ValidatorID = "ConnexiaLeistung123Und15";

        #region - PropertyValidator as subclass
        public class MyPropertyValidator : PropertyValidator
        {
            public class C
            {
                public bool L_1 { get; set; }
                public bool L_2_3 { get; set; }
                public bool L_15 { get; set; }
                public bool L_X { get; set; }
            }

            private NameOfAddressProvider _nameProvider;

            public MyPropertyValidator(NameOfAddressProvider nameProvider)
                : base("Eintrag ist unvollständig: {Detail}")
            {
                _nameProvider = nameProvider;
            }

            public override bool ShouldValidateAsync(ValidationContext context) => true;

            protected override bool IsValid(PropertyValidatorContext context)
            {
                throw new InvalidOperationException();
            }

            protected override async Task<bool> IsValidAsync(PropertyValidatorContext context, CancellationToken cancellation)
            {
                bool result = true;

                var list = context.PropertyValue as IEnumerable<ConnexiaLeistung.Eintrag>;

                var r = new Dictionary<Tuple<DateTime, int>, C>();
                foreach (var entry in list.Where(x => x.Leistung <= 17))
                {
                    var key = new Tuple<DateTime, int>(entry.Datum.Date, entry.Adressnummer);
                    if (!r.ContainsKey(key))
                    {
                        r.Add(key, new C());
                    }
                    var c = r[key];

                    if (entry.Leistung == 1)
                        c.L_1 = true;
                    if (entry.Leistung > 1 && entry.Leistung <= 3)
                        c.L_2_3 = true;
                    else if (entry.Leistung == 15)
                        c.L_15 = true;
                    else
                        c.L_X = true;

                }

                var error_xOhne123 = r.Where(x => x.Value.L_X && !(x.Value.L_1 || x.Value.L_2_3));

                var error_23ohneX15 = r.Where(x => x.Value.L_2_3 && !(x.Value.L_X || x.Value.L_15));

                if (error_xOhne123.Any() || error_23ohneX15.Any())
                {
                    result = false;

                    var namesTasks = error_xOhne123.Select(x => x.Key.Item2).Union(error_23ohneX15.Select(x => x.Key.Item2))
                        .Distinct().Select(x => new
                        {
                            adressnummer = x,
                            nameTask = _nameProvider != null ? _nameProvider.GetName(x) : Task.FromResult("???")
                        });

                    await Task.WhenAll(namesTasks.Select(x => x.nameTask));

                    var names = namesTasks.ToDictionary(x => x.adressnummer, x => x.nameTask.Result);

                    var msg = error_xOhne123.Select(x => string.Format("Kein Eintrag 1,2,3 am {0:dd.MM.yyyy}, bei {1}", x.Key.Item1, names[x.Key.Item2]))
                        .Union(error_23ohneX15.Select(x => string.Format("Keine Leistung bei einem Eintrag 2, 3 am {0:dd.MM.yyyy}, bei {1}", x.Key.Item1, names[x.Key.Item2])))
                        .AggregateToString(System.Environment.NewLine);

                    context.MessageFormatter.AppendArgument("Detail", msg);
                }

                return result;
            }
        }
        #endregion

    }
}
