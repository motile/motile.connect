﻿using FluentValidation;
using FluentValidation.Validators;
using motile.Connect.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace motile.Connect.Connexia.Validation
{

    public class ConnexiaLeistungAnzahlValidator : AbstractValidator<ConnexiaLeistung>, IValidatorInfo
    {
        public ConnexiaLeistungAnzahlValidator(NameOfAddressProvider nameProvider)
            : base()
        {
            RuleFor(x => x.Liste).SetValidator(new MyPropertyValidator(nameProvider)).AsWarningMessage();
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaLeistungAnzahlValidator";

        #region - PropertyValidator as subclass
        public class MyPropertyValidator : PropertyValidator
        {

            private readonly NameOfAddressProvider _nameProvider;

            public MyPropertyValidator(NameOfAddressProvider nameProvider)
                : base("Es gibt Leistungen mit der Anzahl größer 5: {Detail}")
            {
                _nameProvider = nameProvider;
            }

            public override bool ShouldValidateAsync(ValidationContext context) => true;

            protected override bool IsValid(PropertyValidatorContext context)
            {
                throw new InvalidOperationException();
            }

            protected override async Task<bool> IsValidAsync(PropertyValidatorContext context, CancellationToken cancellation)
            {
                var list = context.PropertyValue as IEnumerable<ConnexiaLeistung.Eintrag>;

                var e = list.Where(x => x.Datum.Year >= 2015 && x.Leistung < 31 && x.Anzahl > 5);
                var result = !e.Any();

                if (!result)
                {
                    var namesTasks = e.Select(x => x.Adressnummer)
                      .Distinct().Select(x => new
                      {
                          adressnummer = x,
                          nameTask = _nameProvider != null ? _nameProvider.GetName(x) : Task.FromResult("???")
                      });

                    await Task.WhenAll(namesTasks.Select(x => x.nameTask));

                    var names = namesTasks.ToDictionary(x => x.adressnummer, x => x.nameTask.Result);

                    var msg = e.Select(x => string.Format("{0:dd.MM.yyyy}, {1}, bei {2}", x.Datum, x.Leistung, names[x.Adressnummer])).AggregateToString(System.Environment.NewLine);

                    context.MessageFormatter.AppendArgument("Detail", msg);

                }

                return result;
            }

        }
        #endregion
    }
}
