﻿using FluentValidation;
using FluentValidation.Validators;
using motile.Connect.Validation;
using System.Collections.Generic;
using System.Linq;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaFallListeLengthOfNameValidator : AbstractValidator<ConnexiaFallListe>, IValidatorInfo
    {

        public ConnexiaFallListeLengthOfNameValidator()
            : base()
        {
            RuleFor(x => x.Adressnummern).SetValidator(new MyPropertyValidator()).AsErrorMessage();
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaFallListeLengthOfNameValidator";

        #region - PropertyValidator as subclass
        public class MyPropertyValidator : PropertyValidator
        {

            public MyPropertyValidator()
                : base("Der Name ist zu lang (maximal 30 Zeichen): {Detail}")
            {

            }

            public override bool ShouldValidateAsync(ValidationContext context) => true;

            protected override bool IsValid(PropertyValidatorContext context)
            {
                var list = context.PropertyValue as IDictionary<int, string>;

                var e = list.Where(x => x.Value != null && x.Value.Length > 30);

                var result = !e.Any();

                if (!result)
                {
                    var msg = e.Select(x => $"{x.Key}: {x.Value}").AggregateToString(System.Environment.NewLine);

                    context.MessageFormatter.AppendArgument("Detail", msg);
                }

                return result;
            }
        }
        #endregion
    }
}
