﻿using FluentValidation;
using FluentValidation.Validators;
using motile.Connect.Validation;
using System.Collections.Generic;
using System.Linq;

namespace motile.Connect.Connexia.Validation
{
    public class ConnexiaFallDokuSingleValueValidator : AbstractValidator<ConnexiaFall>, IValidatorInfo
    {
        // für diese Felder darf nur ein Wert gemeldet werden
        public static string[] SingleValueFields = new[] { "21" };

        public ConnexiaFallDokuSingleValueValidator()
            : base()
        {
            RuleFor(x => x.Dokus)
                .SetValidator(new MyPropertyValidator())
                .AsErrorMessage();
        }

        public string GetValidatorID() => ValidatorID;

        public const string ValidatorID = "ConnexiaFallDokuSingleValueValidator";

        #region - PropertyValidator as subclass
        public class MyPropertyValidator : PropertyValidator
        {

            public MyPropertyValidator()
                : base("Ein Mehrfach-Eintrag für im Feld '{Detail}' ist nicht zulässig.")
            {

            }

            protected override bool IsValid(PropertyValidatorContext context)
            {
                bool result = true;

                var list = context.PropertyValue as IEnumerable<ConnexiaFall.Doku>;

                if (list.Any() && list.Select(x => x.Datum).Min().Year >= 2015)  // nur halbwegs aktuelle Neuaufnahmen sollen berücksichtigt werden
                {
                    var data = list.Where(x => SingleValueFields.Contains(x.Gruppe)).ToList();

                    var nonSingle = data.GroupBy(x => new { x.Datum, x.Gruppe })
                        .Where(x => x.Count() > 1);

                    if (nonSingle.Any())
                    {
                        result = false;

                        context.MessageFormatter.AppendArgument("Detail", nonSingle.Select(x => x.Key.Gruppe).Distinct().AggregateToString(","));
                    }
                }

                return result;
            }
        }
        #endregion
    }
}
