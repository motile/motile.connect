﻿using motile.Connect.CQ;
using motile.Connect.CQ.OleDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaPflegerChangeInfoGenerator : IChangeInfoGenerator<ConnexiaPfleger>
    {
        public Task<IEnumerable<ChangeInfo>> Process(IEnumerable<DiffsPack> diffsPacks)
        {
            var result = new List<ChangeInfo>();

            foreach (var diffsPack in diffsPacks)
            {
                var diffs = diffsPack.Diffs.ToList();
                var cmds = new List<Command>();

                while (diffs.Any())
                {
                    var diff = diffs[0];
                    var removeAlso = new List<Difference>();

                    ChangeProperty cpDiff;
                    if ((cpDiff = diff as ChangeProperty) != null)
                    {
                        int pflegernummer;

                        ConnexiaPfleger.GetIDFaktoren(diffsPack.DtoKey.Id, out pflegernummer);
                        
                        if (diff.IsPath<ConnexiaPfleger, string>((x) => x.Vorname, (x) => x.Nachname))
                        {
                            string nachname = diffs.OfType<ChangeProperty>().Where(x => x.IsPath<ConnexiaPfleger, string>(y => y.Nachname)).Select(x => (string)x.NewValue).FirstOrDefault();
                            string vorname = diffs.OfType<ChangeProperty>().Where(x => x.IsPath<ConnexiaPfleger, string>(y => y.Vorname)).Select(x => (string)x.NewValue).FirstOrDefault();

                            var pflegername = string.Format("{0} {1}", nachname, vorname).Trim();

                            if ((string)cpDiff.OldValue == ConnexiaPfleger.NameForNonexistent)
                            {
                                var info = SqlInsertInfo.Create("Pfleger", "Pflegernummer", pflegernummer, new[] { "Pflegername" }, new[] { SqlValueHelper.EscapeValue(pflegername) });

                                cmds.Add(new OleDbSQLInsertCommand(info, $"neuer Eintrag: {pflegername}"));
                            }
                            else
                            {
                                var info = SqlUpdateInfo.Create("Pfleger", "Pflegernummer", "Pflegername", pflegernummer, pflegername);

                                cmds.Add(new OleDbSQLUpdateCommand(info, $"ändere Eintrag: {pflegername}"));
                            }

                            // Nachname und Vorname werden im selben Feld gespeichert -> nur einmal notwendig
                            removeAlso.AddRange(diffs.Where(y => y.IsPath<ConnexiaPfleger, string>((x) => x.Vorname, (x) => x.Nachname)));
                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }

                    diffs.RemoveAt(0);

                    foreach (var remove in removeAlso)
                    {
                        var index = diffs.IndexOf(remove);
                        if (index >= 0)
                            diffs.RemoveAt(index);
                    }
                }

                result.Add(new ChangeInfo(diffsPack, cmds.ToArray()));

            }
            return Task.FromResult<IEnumerable<ChangeInfo>>(result);
        }


    }
}
