﻿using Dapper;
using System;
using System.Data.OleDb;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaPflegerQE_Part : ConnexiaBaseQE_Part<ConnexiaPfleger>
    {
        #region - ctr
        public ConnexiaPflegerQE_Part(OleDbConnection connection)
            : base(connection)
        {

        }
        #endregion

        #region -  methods
        protected override async Task<ConnexiaPfleger> GetDTOAsync(string id, OleDbConnection connection)
        {
            ConnexiaPfleger dto = null;
            int pflegernummer = int.Parse(id);
            
            var row = (await connection.QueryAsync("SELECT Pflegername FROM Pfleger WHERE Pflegernummer = @pflegernummer", new { pflegernummer = pflegernummer })).FirstOrDefault();
            
            if (row != null)
            {
                dto = new ConnexiaPfleger() { Pflegernummer = pflegernummer };
                var name = row.Pflegername;
                var n = SplitName(name);
                dto.Nachname = n.Item1;
                dto.Vorname = n.Item2;
            }
            else
            {
                dto = new ConnexiaPfleger() { Pflegernummer = pflegernummer };
                dto.Nachname = ConnexiaPfleger.NameForNonexistent;
                dto.Vorname = ConnexiaPfleger.NameForNonexistent;
            }


            return dto;
        }
        #endregion

        #region - private methods
        private static Regex _namePattern = new Regex(@"^(?<name1>.+?) +(?<name2>.*)$");
        internal static Tuple<string, string> SplitName(string name)
        {
            var n1 = string.Empty;
            var n2 = string.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                var m = _namePattern.Match(name);
                if (m.Success)
                {
                    n1 = m.Groups["name1"].Value;
                    n2 = m.Groups["name2"].Value;
                }
                else
                {
                    n1 = name;
                }
            }

            return new Tuple<string, string>(n1, n2);
        }
        #endregion
    }
}
