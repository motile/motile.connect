﻿using System;
using System.Runtime.Serialization;

namespace motile.Connect.Connexia
{
    [DataContract]
    public class ConnexiaPfleger : IDTO
    {
        public const string NameForNonexistent = "*";

        [DataMember]
        public Guid SubjectID { get; set; }

        [DataMember]
        public int Pflegernummer { get; set; }

        [DataMember]
        public string Nachname { get; set; }

        [DataMember]
        public string Vorname { get; set; }


        public DtoKey DtoKey
        {
            get { return new DtoKey(DTOTypeProvider.ConnexiaPfleger, Pflegernummer.ToString()); }
        }

        public static void GetIDFaktoren(string id, out int pflegernummer)
        {
            pflegernummer = int.Parse(id);
        }

        public static string GetID(int pflegernummer)
        {
            return pflegernummer.ToString();
        }

        public override string ToString()
        {
            return $"Pfleger {Nachname} {Vorname}";
        }
    }
}
