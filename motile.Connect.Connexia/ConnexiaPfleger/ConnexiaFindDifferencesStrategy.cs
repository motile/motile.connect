﻿using System;
using System.Collections.Generic;


namespace motile.Connect.Connexia
{
    public class ConnexiaFindDifferencesStrategy : IFindDifferencesStrategy<ConnexiaPfleger>
    {
        public IEnumerable<DiffsPack> GetDifferences(ConnexiaPfleger master, ConnexiaPfleger changeThis)
        {
            if (!string.Equals(string.Format("{0} {1}", master.Nachname, master.Vorname).Trim(), string.Format("{0} {1}", changeThis.Nachname, changeThis.Vorname).Trim(), StringComparison.Ordinal))
            {
                yield return new DiffsPack(master.DtoKey,
                    ChangeProperty.Create(master, changeThis, x => x.Nachname),
                    ChangeProperty.Create(master, changeThis, x => x.Vorname)
                    );
            }
        }
    }
}
