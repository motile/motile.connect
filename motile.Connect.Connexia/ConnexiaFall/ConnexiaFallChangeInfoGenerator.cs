﻿using motile.Connect.CQ;
using motile.Connect.CQ.Fnc;
using motile.Connect.CQ.OleDb;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallChangeInfoGenerator : IChangeInfoGenerator<ConnexiaFall>
    {
        #region - fields
        private readonly CountryProvider _countryProvider;
        private readonly CityProvider _cityProvider;
        private readonly IQueryEngine _queryEngine;
        #endregion

        #region - ctr
        public ConnexiaFallChangeInfoGenerator(CountryProvider countryProvider, CityProvider cityProvider, IQueryEngine queryEngine)
        {
            _countryProvider = countryProvider;
            _cityProvider = cityProvider;
            _queryEngine = queryEngine;
        }
        #endregion

        #region - methods
        public async Task<IEnumerable<ChangeInfo>> Process(IEnumerable<DiffsPack> diffsPacks)
        {
            //ungüldige Änderungen (derzeit noch) hier abfangen                        
            var invalid = await ResultForInvalidDifferences(diffsPacks);

            if (invalid != null && invalid.Any())
                return invalid;

            var result = new List<ChangeInfo>();
            foreach (var diffsPack in diffsPacks)
            {
                ConnexiaFall.GetIDFaktoren(diffsPack.DtoKey.Id, out int adressnummer);

                var diffs = diffsPack.Diffs.ToList();

                var cmds = new List<Command>();

                while (diffs.Any())
                {
                    var diff = diffs[0];
                    var removeAlso = new List<Difference>();

                    ChangeProperty cpDiff;
                    AddEntryDifference aeDiff;
                    RemoveEntryDifference reDiff;

                    if ((cpDiff = diff as ChangeProperty) != null)
                    {
                        if (diff.IsPath<ConnexiaFall, string>((x) => x.Vorname, (x) => x.Nachname))
                        {
                            var nachnameDiff = diffs.OfType<ChangeProperty>().Where(x => x.IsPath<ConnexiaFall, string>(y => y.Nachname)).FirstOrDefault();
                            var vornameDiff = diffs.OfType<ChangeProperty>().Where(x => x.IsPath<ConnexiaFall, string>(y => y.Vorname)).FirstOrDefault();

                            var nachname = (string)nachnameDiff.NewValue;
                            var vorname = (string)vornameDiff.NewValue;

                            var info = string.Format("Name: {0} -> {1}", string.Format("{0} {1}", nachnameDiff.OldValue, vornameDiff.OldValue), string.Format("{0} {1}", nachname, vorname));

                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Name_1", adressnummer, string.Format("{0} {1}", nachname, vorname));

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));

                            // Nachname und Vorname werden im selben Feld gespeichert -> nur einmal notwendig
                            if (diff != nachnameDiff)
                            {
                                removeAlso.Add(nachnameDiff);
                            }
                            else
                            {
                                removeAlso.Add(vornameDiff);
                            }

                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.PLZ))
                        {

                            var plzDiff = diffs.OfType<ChangeProperty>().Where(x => x.IsPath<ConnexiaFall, string>(y => y.PLZ)).FirstOrDefault();
                            var ortDiff = diffs.OfType<ChangeProperty>().Where(x => x.IsPath<ConnexiaFall, string>(y => y.Ort)).FirstOrDefault();

                            var plz = (string)plzDiff.NewValue;
                            var ort = (string)ortDiff.NewValue;


                            var city = _cityProvider.Cities.Where(x => x.Code == plz)
                                .Where(x => string.IsNullOrEmpty(ort) || string.Equals(x.Name, ort, StringComparison.CurrentCultureIgnoreCase))
                                .FirstOrDefault();

                            if (city != null)
                            {
                                plz = city.ConnexiaCode;
                            }

                            // in der connexia-db wird nur die plz gespeichert
                            if (!string.Equals((string)plzDiff.OldValue, plz))
                            {
                                var info = string.Format("Postleitzahl: {0} -> {1}", plzDiff.OldValue, plz);
                                var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Postleitzahl", adressnummer, plz);

                                cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                            }

                            // Plz und Ort werden im selben Feld gespeichert -> nur einmal notwendig
                            if (diff != plzDiff)
                            {
                                removeAlso.Add(plzDiff);
                            }
                            else
                            {
                                removeAlso.Add(ortDiff);
                            }
                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Adresse))
                        {
                            var newValue = CompareHelper.GetStringLengthValue<ConnexiaFall>(x => x.Adresse, (string)cpDiff.NewValue);

                            var info = string.Format("Straße: {0} -> {1}", cpDiff.OldValue, newValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Adresse", adressnummer, newValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Land))
                        {
                            var land = _countryProvider.Countries.Where(x => string.Equals(x.Value, (string)cpDiff.NewValue, StringComparison.InvariantCultureIgnoreCase)).Select(x => x.Key).FirstOrDefault() ?? (string)cpDiff.NewValue;

                            var info = string.Format("Land: {0} -> {1}", cpDiff.OldValue, land);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Land", adressnummer, land);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }

                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Ort))
                        {
                            // ignorieren
                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Geschlecht))
                        {
                            var newValue = CompareHelper.GetStringLengthValue<ConnexiaFall>(x => x.Verein, (string)cpDiff.NewValue);

                            var info = string.Format("Geschlecht: {0} -> {1}", cpDiff.OldValue, newValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Geschlecht", adressnummer, newValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else if (diff.IsPath<ConnexiaFall, DateTime?>((x) => x.Geburtsdatum))
                        {
                            var info = string.Format("Geburtsdatum: {0:dd.MM.yyyy} -> {1:dd.MM.yyyy}", cpDiff.OldValue, cpDiff.NewValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Geburtsdatum", adressnummer, (DateTime?)cpDiff.NewValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else if (diff.IsPath<ConnexiaFall, int?>((x) => x.Mitgliedsnummer))
                        {
                            var info = string.Format("Mitgliedsnummer: {0} -> {1}", cpDiff.OldValue, cpDiff.NewValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Mitgliedsnummer", adressnummer, ((int?)cpDiff.NewValue).HasValue ? cpDiff.NewValue : "null");

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Verein))
                        {
                            var newValue = CompareHelper.GetStringLengthValue<ConnexiaFall>(x => x.Verein, (string)cpDiff.NewValue);

                            var info = string.Format("Verein: {0} -> {1}", cpDiff.OldValue, newValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Zuord_SB", adressnummer, newValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Staatsbuergerschaft))
                        {
                            var newValue = CompareHelper.GetStringLengthValue<ConnexiaFall>(x => x.Staatsbuergerschaft, (string)cpDiff.NewValue);

                            var info = string.Format("Nationalität: {0} -> {1}", cpDiff.OldValue, newValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Staatsbuergerschaft", adressnummer, newValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Telefon))
                        {
                            var newValue = CompareHelper.GetStringLengthValue<ConnexiaFall>(x => x.Telefon, (string)cpDiff.NewValue);

                            var info = string.Format("Telefon: {0} -> {1}", cpDiff.OldValue, newValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Telefon", adressnummer, newValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Versicherung))
                        {
                            var newValue = CompareHelper.GetStringLengthValue<ConnexiaFall>(x => x.Versicherung, (string)cpDiff.NewValue);

                            var info = string.Format("Versicherung: {0} -> {1}", cpDiff.OldValue, newValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Versicherung", adressnummer, newValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));

                        }
                        else if (diff.IsPath<ConnexiaFall, string>((x) => x.Versicherungsnummer))
                        {
                            var newValue = CompareHelper.GetStringLengthValue<ConnexiaFall>(x => x.Versicherungsnummer, (string)cpDiff.NewValue);

                            var info = string.Format("Versicherungsnr: {0} -> {1}", cpDiff.OldValue, newValue);
                            var updateInfo = SqlUpdateInfo.Create("Adressen", "Adressnummer", "Versicherungsnummer", adressnummer, newValue);

                            cmds.Add(new OleDbSQLUpdateCommand(updateInfo, info));
                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                    else if ((aeDiff = diff as AddEntryDifference) != null)
                    {
                        if (aeDiff.IsPath<ConnexiaFall, List<ConnexiaFall.Doku>>((x) => x.Dokus))
                        {
                            var e = (ConnexiaFall.Doku)aeDiff.NewEntry;
                            var newWert = CompareHelper.GetStringLengthValue(e, x => x.Wert, e.Wert) ?? String.Empty;

                            var info = string.Format("Doku hinzufügen: {0:dd.MM.yyyy} {1}.{2} {3}", e.Datum, e.Gruppe, e.Detail, e.Wert);

                            var insertInfo = SqlInsertInfo.Create("Doku", "Adressnummer", adressnummer, new[] { "Gruppe", "Detail", "Datum", "Wert", "eingespielt_am" },
                                new string[] {
                                SqlValueHelper.EscapeValue(e.Gruppe),
                                SqlValueHelper.EscapeValue(e.Detail),
                                SqlValueHelper.EscapeValue(e.Datum),
                                SqlValueHelper.EscapeValue(newWert),
                                SqlValueHelper.EscapeValue(DateTime.Now)
                                });

                            cmds.Add(new OleDbSQLInsertCommand(insertInfo, info));
                        }
                    }
                    else if ((reDiff = diff as RemoveEntryDifference) != null)
                    {
                        if (reDiff.IsPath<ConnexiaFall, List<ConnexiaFall.Doku>>((x) => x.Dokus))
                        {
                            var e = (ConnexiaFall.Doku)reDiff.RemoveEntry;

                            var info = string.Format("Doku entfernen: {0:dd.MM.yyyy} {1}.{2} {3}", e.Datum, e.Gruppe, e.Detail, e.Wert);

                            cmds.Add(new OleDbBaseCommand(GetRemoveEntryCommand(adressnummer, e.Datum, e.Gruppe, e.Detail), info));
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }

                    diffs.RemoveAt(0);
                    foreach (var remove in removeAlso)
                    {
                        var index = diffs.IndexOf(remove);
                        if (index >= 0)
                            diffs.RemoveAt(index);
                    }
                }

                result.Add(new ChangeInfo(diffsPack, cmds.ToArray()));
            }

            return result;
        }

        private string GetRemoveEntryCommand(int adressnummer, DateTime datum, string gruppe, string detail)
        {
            var cmdText = $@"DELETE [Doku].* FROM [Doku] 
WHERE [Doku].[Adressnummer]={adressnummer} and [Doku].Datum={SqlValueHelper.EscapeValue(datum)} and Gruppe='{gruppe}' and Detail='{detail}'";



            return cmdText;
        }


        private async Task<IEnumerable<ChangeInfo>> ResultForInvalidDifferences(IEnumerable<DiffsPack> diffsPacks)
        {
            IEnumerable<ChangeInfo> result = null;
                        
            foreach (var pack in diffsPacks)
            {
                var changedVNummer = pack.Diffs
                    .OfType<ChangeProperty>()
                    .Where(x => x.IsPath<ConnexiaFall, string>(f => f.Versicherungsnummer))
                    .Where(x => !string.IsNullOrEmpty((string)x.OldValue) && !string.IsNullOrEmpty((string)x.NewValue))
                    .FirstOrDefault();

                if (changedVNummer != null)
                {
                    var dto = (await _queryEngine.QueryAsync(new DtoQuery<ConnexiaFall>(pack.DtoKey))).Results.First();

                    // Das Hauptproblem dürfte entstehen, wenn alte Datensätze überschrieben werden.
                    // -> sehr lasche Prüfung
                    if (dto.Dokus.Where(x => x.Datum < DateTime.Today.AddMonths(-4)).Any()) 
                    {
                        var errorPack = new DiffsPack(pack.DtoKey, new Difference[] { new ChangeProperty() { Path = changedVNummer.Path, OldValue = changedVNummer.OldValue, NewValue = "Änderung nicht erlaubt!" } });
                        var errorComamnd = new FncCommand(() =>
                        {
                            throw new InvalidOperationException("Eine Änderung der Versicherungsnummer ist nicht mehr erlaubt!");
                        }, "Eine Änderung der Versicherungsnummer ist nicht mehr erlaubt!");

                        result = new[] { new ChangeInfo(errorPack, new Command[] { errorComamnd }) };
                    }
                }
            }

            return result;

        }
        #endregion
    }
}
