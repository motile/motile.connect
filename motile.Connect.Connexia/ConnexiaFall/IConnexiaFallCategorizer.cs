﻿using System;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public interface IConnexiaFallCategorizer
    {
        ConnexiaFallCategory GetCategory(ConnexiaFall fall);
    }

    public interface IConnexiaFallAdressnummerCategorizer
    {
        Task<ConnexiaFallCategory> GetCategoryAsync(int adressnummer);
    }
}
