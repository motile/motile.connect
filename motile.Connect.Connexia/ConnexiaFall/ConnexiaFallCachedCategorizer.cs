﻿using System.Collections.Generic;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallCachedCategorizer : IConnexiaFallCategorizer
    {
        #region - fields
        private IConnexiaFallCategorizer _categorizer;
        private SortedDictionary<int, ConnexiaFallCategory> _cache = new SortedDictionary<int, ConnexiaFallCategory>();
        private object _lock = new object();
        #endregion

        #region - ctr
        public ConnexiaFallCachedCategorizer(IConnexiaFallCategorizer categorizer)
        {
            _categorizer = categorizer;
        }
        #endregion

        #region - methods
        public ConnexiaFallCategory GetCategory(ConnexiaFall fall)
        {
            var result = ConnexiaFallCategory.unbestimmt;

            if (fall != null)
            {
                if (!ContainsAdressnummer(fall.Adressnummer))
                {
                    result = GetCategoryAndSetCache(fall);
                }
                else
                {
                    result = _cache[fall.Adressnummer];
                }
            }

            return result;
        }

        public bool ContainsAdressnummer(int adressnummer)
        {
            return _cache.ContainsKey(adressnummer);
        }

        public ConnexiaFallCategory GetCategory(int adressnummer)
        {
            return _cache[adressnummer];
        }

        private ConnexiaFallCategory GetCategoryAndSetCache(ConnexiaFall fall)
        {
            ConnexiaFallCategory result = ConnexiaFallCategory.unbestimmt;

            lock (_lock)
            {
                if (!ContainsAdressnummer(fall.Adressnummer))
                {
                    _cache.Add(fall.Adressnummer, _categorizer.GetCategory(fall));
                }

                result = _cache[fall.Adressnummer];
            }

            return result;

        }

        #endregion
    }
}
