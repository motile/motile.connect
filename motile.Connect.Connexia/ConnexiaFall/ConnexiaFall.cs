﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace motile.Connect.Connexia
{
    [DataContract]
    public class ConnexiaFall : IDTO
    {
        public const int EmptyID = Int16.MinValue;

        [DataContract]
        public class Doku
        {
            [DataMember]
            [StringLength(10)]
            public string Gruppe { get; set; }

            [DataMember]
            [StringLength(10)]
            public string Detail { get; set; }

            [DataMember]
            public DateTime Datum { get; set; }

            [DataMember]
            [StringLength(20)]
            public string Wert { get; set; }
        }

        public ConnexiaFall()
        {
            this.Dokus = new List<Doku>();
            this.Adressnummer = EmptyID;
        }

        [DataMember]
        public int Adressnummer { get; set; }

        [DataMember]        
        public string Nachname { get; set; }

        [DataMember]
        public string Vorname { get; set; }

        [DataMember]
        [StringLength(30, MinimumLength =1)]
        public string Adresse { get; set; }

        [DataMember]
        public string Land { get; set; }

        [DataMember]
        public string PLZ { get; set; }

        [DataMember]
        public string Ort { get; set; }

        [DataMember]
        [StringLength(1, MinimumLength = 1)]
        public string Geschlecht { get; set; }

        [DataMember]
        [StringLength(20, MinimumLength = 1)]
        public string Telefon { get; set; }

        [DataMember]
        public DateTime? Geburtsdatum { get; set; }

        [DataMember]
        public string Staatsbuergerschaft { get; set; }

        [DataMember]
        public string Versicherung { get; set; }

        [DataMember]
        public string Versicherungsnummer { get; set; }

        [DataMember]
        public int? Mitgliedsnummer { get; set; }

        [DataMember]
        public string Verein { get; set; }

        [DataMember]
        public List<Doku> Dokus { get; set; }

        public DtoKey DtoKey
        {
            get
            {
                return new DtoKey(DTOTypeProvider.ConnexiaFall, GetID(Adressnummer));
            }
        }

        public static void GetIDFaktoren(string id, out int adressnummer)
        {
            adressnummer = int.Parse(id);
        }

        public static string GetID(int adressnummer)
        {
            return adressnummer.ToString();
        }

        public override string ToString()
        {
            return $"Adresse: {Nachname} {Vorname}".TrimEnd();
        }
    }
}
