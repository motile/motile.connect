﻿using Dapper;
using System;
using System.Linq;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallQE_Part : ConnexiaBaseQE_Part<ConnexiaFall>
    {
        #region - fields
        private CountryProvider _countryProvider;
        private CityProvider _cityProvider;
        #endregion

        #region - ctr
        public ConnexiaFallQE_Part(OleDbConnection connection, CountryProvider countryProvider, CityProvider cityProvider)
            : base(connection)
        {
            _countryProvider = countryProvider;
            _cityProvider = cityProvider;
        }
        #endregion

        #region -  methods
        protected override async Task<ConnexiaFall> GetDTOAsync(string id, OleDbConnection connection)
        {
            ConnexiaFall dto = null;
            int adressnummer = int.Parse(id);
            
            var fall = (await connection.QueryAsync(@"SELECT Name_1, Adresse, Land, Postleitzahl, Geschlecht, Telefon, Geburtsdatum, Staatsbuergerschaft, Versicherung, Versicherungsnummer, Mitgliedsnummer, Zuord_SB 
FROM Adressen WHERE Adressnummer = @adressnummer", new { adressnummer = adressnummer })).FirstOrDefault();

            if (fall != null)
            {
                dto = new ConnexiaFall() { Adressnummer = adressnummer };

                var name = fall.Name_1?.Trim();
                var n = SplitName(name);
                dto.Nachname = n.Item1;
                dto.Vorname = n.Item2;
                dto.Adresse = fall.Adresse?.Trim();
                var land = fall.Land?.Trim();

                if (!string.IsNullOrEmpty(land))
                {
                    if (_countryProvider.Countries.ContainsKey(land))
                        land = _countryProvider.Countries[land];
                }

                dto.Land = land;

                var plz = fall.Postleitzahl?.Trim();

                if (!string.IsNullOrEmpty(plz))
                {
                    var city = _cityProvider.Cities.Where(x => x.ConnexiaCode == plz).FirstOrDefault();
                    if (city != null)
                    {
                        dto.Ort = city.Name;
                        dto.PLZ = city.Code;
                    }
                    else
                    {
                        dto.PLZ = plz;
                    }
                }

                dto.Geschlecht = fall.Geschlecht?.Trim();

                if (!string.IsNullOrEmpty(dto.Geschlecht))
                {
                    dto.Geschlecht = dto.Geschlecht.ToLower();
                }

                dto.Telefon = fall.Telefon?.Trim();
                dto.Geburtsdatum = fall.Geburtsdatum;                                
                dto.Staatsbuergerschaft = fall.Staatsbuergerschaft?.Trim();
                dto.Versicherung = fall.Versicherung?.Trim();
                dto.Versicherungsnummer = fall.Versicherungsnummer?.Trim();
                dto.Mitgliedsnummer = fall.Mitgliedsnummer;
                dto.Verein = fall.Zuord_SB?.Trim();
            }

            if (dto != null)
            {
                await FillDok(connection, dto);
            }


            return dto;
        }
        #endregion

        #region - private methods
        private static Regex _namePattern = new Regex(@"^(?<name1>.+?) +(?<name2>.*)$");
        public static Tuple<string, string> SplitName(string name)
        {
            var n1 = string.Empty;
            var n2 = string.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                var m = _namePattern.Match(name);
                if (m.Success)
                {
                    n1 = m.Groups["name1"].Value;
                    n2 = m.Groups["name2"].Value;
                }
                else
                {
                    n1 = name;
                    n2 = string.Empty;
                }
            }

            return new Tuple<string, string>(n1, n2);
        }


        private async Task FillDok(OleDbConnection connection, ConnexiaFall fall)
        {
            var dokus = await connection.QueryAsync("SELECT Gruppe, Detail, Datum, Wert FROM Doku WHERE Adressnummer = @adressnummer AND NOT Datum IS NULL ORDER BY Datum, Gruppe, Detail", new { adressnummer = fall.Adressnummer });

            foreach (var doku in dokus)
            {
                var entry = new ConnexiaFall.Doku()
                {
                    Gruppe = doku.Gruppe,
                    Detail = doku.Detail,
                    Datum = doku.Datum,
                    Wert = doku.Wert ?? String.Empty
                };

                // bei alten Daten ist der Wert oft auf 0, was nix zu bedeuten hat -> weg damit
                if (entry.Wert == "0")
                {
                    entry.Wert = String.Empty;
                }
                fall.Dokus.Add(entry);
            }
        }
        #endregion
    }
}
