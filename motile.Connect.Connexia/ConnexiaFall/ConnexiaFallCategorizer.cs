﻿using System.Text.RegularExpressions;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallCategorizer : IConnexiaFallCategorizer
    {
        public ConnexiaFallCategory GetCategory(ConnexiaFall fall)
        {
            var result = ConnexiaFallCategory.unbestimmt;

            if (fall != null)
            {
                var nichtKlientPattern = new Regex("beratung", RegexOptions.IgnoreCase);

                if (nichtKlientPattern.IsMatch(string.Format("{0} {1}", fall.Nachname, fall.Vorname)))
                {
                    result = ConnexiaFallCategory.NichtKlient;
                }
                else
                {
                    result = ConnexiaFallCategory.Klient;
                }
            }

            return result;
        }
    }
}
