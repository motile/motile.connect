﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallFindDifferencesStrategy : IFindDifferencesStrategy<ConnexiaFall>
    {
        public IEnumerable<DiffsPack> GetDifferences(ConnexiaFall master, ConnexiaFall changeThis)
        {
            var result = new List<DiffsPack>();

            // Änderungen in ein Diffspack packen, ausnahmen folgen anschließend
            {
                var differences = new List<Difference>();

                if (!string.Equals(string.Format("{0} {1}", master.Nachname, master.Vorname).Trim(), string.Format("{0} {1}", changeThis.Nachname, changeThis.Vorname).Trim(), StringComparison.Ordinal))
                {
                    differences.AddRange(new[] {
                    ChangeProperty.Create(master, changeThis, x => x.Nachname, changeThis.Nachname, master.Nachname),
                    ChangeProperty.Create(master, changeThis, x => x.Vorname, changeThis.Vorname, master.Vorname)
                });
                }

                if (!string.Equals(string.Format("{0} {1}", master.PLZ, master.Ort).Trim(), string.Format("{0} {1}", changeThis.PLZ, changeThis.Ort).Trim(), StringComparison.Ordinal))
                {
                    differences.AddRange(new[] {
                    ChangeProperty.Create(master, changeThis, x => x.PLZ, changeThis.PLZ, master.PLZ),
                    ChangeProperty.Create(master, changeThis, x => x.Ort, changeThis.Ort, master.Ort)
                });
                }

                foreach (var c in CompareHelper.CompareString(master, changeThis, x => x.Adresse, x => x.Land, x => x.Geschlecht, x => x.Telefon, x => x.Versicherung, x => x.Versicherungsnummer))
                {
                    differences.Add(c);
                }

                foreach (var c in CompareHelper.CompareNDateTime(master, changeThis, x => x.Geburtsdatum))
                {
                    differences.Add(c);
                }

                var dokM = master.Dokus.ToList();
                var dokC = changeThis.Dokus.ToList();
                ConnexiaFall.Doku entry;

                var adds = new List<AddEntryDifference>();
                var removes = new List<RemoveEntryDifference>();

                while ((entry = dokM.FirstOrDefault()) != null)
                {
                    dokM.Remove(entry);

                    var existing = dokC.Where(x => x.Gruppe == entry.Gruppe && x.Datum == entry.Datum && x.Detail == entry.Detail).FirstOrDefault();
                    if (existing != null)
                    {
                        if (CompareHelper.CompareString(existing, entry, x => x.Wert).Any())
                        {
                            adds.Add(new AddEntryDifference() { Path = atstring.of(changeThis, (x) => x.Dokus), NewEntry = entry });
                            removes.Add(new RemoveEntryDifference() { Path = atstring.of(changeThis, (x) => x.Dokus), RemoveEntry = entry });
                        }

                        dokC.Remove(existing);
                    }
                    else
                    {
                        adds.Add(new AddEntryDifference() { Path = atstring.of(changeThis, (x) => x.Dokus), NewEntry = entry });
                    }
                }

                while ((entry = dokC.FirstOrDefault()) != null)
                {
                    dokC.Remove(entry);
                    removes.Add(new RemoveEntryDifference() { Path = atstring.of(changeThis, (x) => x.Dokus), RemoveEntry = entry });
                }

                if (adds.Any() || removes.Any())
                {
                    differences.AddRange(removes.Cast<Difference>().Union(adds).Cast<Difference>());
                }

                if (differences.Any())
                {
                    result.Add(new DiffsPack(master.DtoKey, differences.ToArray()));
                }

            }

            // Ausnahmen: diese Änderungen jeweils einzeln
            {
                foreach (var c in CompareHelper.CompareString(master, changeThis, x => x.Verein, x => x.Staatsbuergerschaft))
                {
                    result.Add(new DiffsPack(master.DtoKey, c));
                }

                foreach (var c in CompareHelper.CompareNInt(master, changeThis, x => x.Mitgliedsnummer))
                {
                    result.Add(new DiffsPack(master.DtoKey, c));
                }
            }
            return result;
        }
    }
}
