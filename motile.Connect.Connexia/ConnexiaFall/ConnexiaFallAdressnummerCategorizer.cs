﻿using System.Linq;
using motile.Connect.CQ;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaFallAdressnummerCategorizer : IConnexiaFallAdressnummerCategorizer
    {
        #region - fields
        private readonly IQueryEngine _queryEngine;
        private readonly ConnexiaFallCachedCategorizer _categorizer;
        #endregion

        #region - ctr
        public ConnexiaFallAdressnummerCategorizer(IQueryEngine queryEngine, ConnexiaFallCachedCategorizer categorizer)
        {
            _queryEngine = queryEngine;
            _categorizer = categorizer;
        }
        #endregion

        #region - methods
        public async Task<ConnexiaFallCategory> GetCategoryAsync(int adressnummer)
        {
            ConnexiaFallCategory result = ConnexiaFallCategory.unbestimmt;

            if (_categorizer.ContainsAdressnummer(adressnummer))
            {
                result = _categorizer.GetCategory(adressnummer);
            }
            else
            {
                var fall = (await  _queryEngine.QueryAsync(DTOTypeProvider.CreateQuery<ConnexiaFall>(ConnexiaFall.GetID(adressnummer)))).Results.FirstOrDefault();

                if (fall != null)
                {
                    result = _categorizer.GetCategory(fall);
                }
            }

            return result;
        }
        #endregion
    }
}
