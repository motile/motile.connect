﻿using motile.Connect.CQ;
using System.Data.OleDb;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public abstract class ConnexiaBaseQE_Part<T> : IQueryEnginePart<T, DtoQuery<T>>
        where T : IDTO
    {
        #region - fields
        protected OleDbConnection _connection;
        #endregion

        #region - ctr
        public ConnexiaBaseQE_Part(OleDbConnection connection)
        {
            _connection = connection;
        }
        #endregion

        #region - public methods
        public async Task<QueryResult<T>> QueryAsync(DtoQuery<T> query)
        {
            T result = default(T);

            result = await GetDTOAsync(query.Key.Id, _connection);

            return new QueryResult<T>(result);
        }
        #endregion

        #region - methods
        protected abstract Task<T> GetDTOAsync(string id, OleDbConnection connection);
        #endregion
    }
}
