﻿using motile.Connect.CQ;
using motile.Connect.CQ.OleDb;
using System;
using System.Data.OleDb;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaCommandProcessor : ICommandExecutor
    {
        #region - private fields        
        private readonly OleDbConnection _connection;
        #endregion

        #region - ctr
        public ConnexiaCommandProcessor(OleDbConnection connection)
        {
            _connection = connection;
        }
        #endregion

        #region - methods
        public async Task<bool> Process(DtoKey dtoKey, Command command)
        {
            Contract.Assert(CanProcess(dtoKey, command));

            bool result = true;
            
            using (OleDbCommand oleDbCmd = ((OleDbBaseCommand)command).GetCommand())
            {
                oleDbCmd.Connection = _connection;

                var i = await oleDbCmd.ExecuteNonQueryAsync();

                await Log(dtoKey, command, oleDbCmd);
            }

            return result;

        }

        public async Task Log(DtoKey dtoKey, Command command, OleDbCommand oleDbCommand)
        {
            var sql = oleDbCommand.CommandText;

            if (oleDbCommand.Parameters.Count > 0)
            {
                foreach (OleDbParameter p in oleDbCommand.Parameters)
                {
                    sql = sql.Replace(p.ParameterName.ToString(), $"'{p.Value.ToString()}'");
                }
            }

            sql = sql.Replace("'", "''");

            var zeitpunkt = SqlValueHelper.EscapeValue(DateTime.Now);

            var cmd = new OleDbCommand($@"INSERT INTO TransdokLog ([Timestamp],[Entity],[Domain],[Info],[Sql]) 
                                            VALUES ({zeitpunkt},'{dtoKey.TypeKey}','{dtoKey.Id}','{command.Info}','{sql}')", oleDbCommand.Connection);

            var task = cmd.ExecuteNonQueryAsync();

            try
            {
                await task;
            }
            catch
            {
                // einfach alle Fehler ingorieren
            }
        }

        public bool CanProcess(DtoKey key, Command command)
        {
            return command is OleDbBaseCommand;
        }
        #endregion
    }
}
