﻿
namespace motile.Connect.Connexia
{
    public class LocalConnexiaSettings
    {
        public const string ID = "ConnexiaSettings";
        
        public LocalConnexiaSettings()
        {
            this.Database = @"c:\verein\vgkvdat.mdb";
        }

        public string Database { get; set; }
    }
}
