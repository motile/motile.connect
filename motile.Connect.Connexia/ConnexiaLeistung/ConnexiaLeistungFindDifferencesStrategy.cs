﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace motile.Connect.Connexia
{
    public class ConnexiaLeistungFindDifferencesStrategy : IFindDifferencesStrategy<ConnexiaLeistung>
    {
        public IEnumerable<DiffsPack> GetDifferences(ConnexiaLeistung master, ConnexiaLeistung changeThis)
        {
            var masterList = master.Liste.ToList();
            var changeList = changeThis.Liste.ToList();
            var addList = new List<AddEntryDifference>();


            foreach (var entry in masterList)
            {
                var c = changeList.Where(x => x.Adressnummer == entry.Adressnummer && x.Datum == entry.Datum && x.Leistung == entry.Leistung && x.Anzahl == entry.Anzahl).FirstOrDefault();
                if (c != null)
                {
                    changeList.Remove(c);
                }
                else
                {
                    addList.Add(AddEntryDifference.Create(changeThis, x => x.Liste, entry));
                }
            }

            foreach (var remove in changeList)
            {
                yield return new DiffsPack(master.DtoKey, RemoveEntryDifference.Create(changeThis, x => x.Liste, remove));
            }

            foreach (var add in addList)
            {
                yield return new DiffsPack(master.DtoKey, add);
            }
        }

    }
}
