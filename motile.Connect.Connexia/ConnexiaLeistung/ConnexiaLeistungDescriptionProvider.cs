﻿using System;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaLeistungDescriptionProvider : IDTODescriptionProvider<ConnexiaLeistung>
    {
        private readonly NameOfPflegerProvider _nameProvider;

        public ConnexiaLeistungDescriptionProvider(NameOfPflegerProvider nameProvider)
        {
            _nameProvider = nameProvider;
        }
        public async Task<string> WithIDAsync(string id)
        {
            DateTime von;
            DateTime bis;
            int pflegernummer;

            ConnexiaLeistung.GetIDFaktoren(id, out pflegernummer, out von, out bis);
            
            string name = await _nameProvider.GetName(pflegernummer);
            
            return $"Leistungspunkte von '{name}' ({von:dd.MM.yyyy}-{bis:dd.MM.yyyy})";
        }
    }
}
