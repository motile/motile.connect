﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace motile.Connect.Connexia
{
    [DataContract]
    public class ConnexiaLeistung : IDTO
    {
        [DataContract]
        public class Eintrag
        {
            [DataMember]
            public int Adressnummer { get; set; }
            [DataMember]
            public DateTime Datum { get; set; }

            [DataMember]
            public int Leistung { get; set; }

            [DataMember]
            public double Anzahl { get; set; }
        }


        public ConnexiaLeistung()
        {
            this.Liste = new List<Eintrag>();
        }

        [DataMember]
        public int Pflegernummer { get; set; }
        [DataMember]
        public DateTime Von { get; set; }
        [DataMember]
        public DateTime Bis { get; set; }

        [DataMember]
        public List<Eintrag> Liste { get; set; }


        #region - ID
        public DtoKey DtoKey
        {
            get { return new DtoKey(DTOTypeProvider.ConnexiaLeistung, GetID(this.Pflegernummer, this.Von, this.Bis)); }
        }

        public static string GetID(int pflegernummer, DateTime von, DateTime bis)
        {
            return string.Format("{0}_{1:yyyyMMdd}_{2:yyyyMMdd}", pflegernummer, von, bis);
        }

        private static Regex _idPattern = new Regex(@"^(?<id>-?\d+)_(?<von>\d+)_(?<bis>\d+)$");
        public static void GetIDFaktoren(string id, out int pflegernummer, out DateTime von, out DateTime bis)
        {
            var m = _idPattern.Match(id);

            pflegernummer = int.Parse(m.Groups["id"].Value);
            von = DateTime.ParseExact(m.Groups["von"].Value, "yyyyMMdd", CultureInfo.InvariantCulture);
            bis = DateTime.ParseExact(m.Groups["bis"].Value, "yyyyMMdd", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59);
        }
        #endregion
    }
}
