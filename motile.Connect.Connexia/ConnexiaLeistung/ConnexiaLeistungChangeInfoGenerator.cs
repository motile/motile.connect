﻿using motile.Connect.CQ;
using motile.Connect.CQ.OleDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaLeistungChangeInfoGenerator : IChangeInfoGenerator<ConnexiaLeistung>
    {

        #region - fields
        private readonly ConnexiaLeistungQE_Part _connexiaLeistungRepository;
        private readonly NameOfAddressProvider _nameProvider;        
        private readonly AsyncLock _fallLock = new AsyncLock();
        #endregion

        #region - ctr
        public ConnexiaLeistungChangeInfoGenerator(ConnexiaLeistungQE_Part connexiaLeistungRepository, NameOfAddressProvider nameProvider)
        {
            _connexiaLeistungRepository = connexiaLeistungRepository;
            _nameProvider = nameProvider;
        }
        #endregion

        #region - methods
        public async Task<IEnumerable<ChangeInfo>> Process(IEnumerable<DiffsPack> diffsPacks)
        {
            var result = new List<ChangeInfo>();

            foreach (var diffsPack in diffsPacks)
            {
                int pflegernummer; DateTime von; DateTime bis;

                ConnexiaLeistung.GetIDFaktoren(diffsPack.DtoKey.Id, out pflegernummer, out von, out bis);
                var diffs = diffsPack.Diffs.ToList();

                var cmds = new List<Command>();

                while (diffs.Any())
                {
                    var diff = diffs[0];
                    diffs.RemoveAt(0);

                    if (diff is AddEntryDifference)
                    {
                        var a = (AddEntryDifference)diff;
                        var n = a.NewEntry as ConnexiaLeistung.Eintrag;

                        var info = string.Format("neue Leistung am {0:dd.MM.yyyy}: {1}x{2}, bei ({3}, {4})", n.Datum, n.Anzahl, n.Leistung, n.Adressnummer, await _nameProvider.GetName(n.Adressnummer));

                        var getCmd = GetAddEntryCommand(pflegernummer, n.Adressnummer, n.Datum, n.Leistung, n.Anzahl);
                        cmds.Add(new OleDbBaseCommand(getCmd, info));
                    }
                    else if (diff is RemoveEntryDifference)
                    {
                        var r = (RemoveEntryDifference)diff;
                        var n = r.RemoveEntry as ConnexiaLeistung.Eintrag;

                        var ls = await this._connexiaLeistungRepository.GetLs(n.Adressnummer, pflegernummer, n.Datum, n.Leistung);

                        if (!ls.Where(x => x.Übertragen.HasValue).Any())
                        {
                            foreach (var l in ls)
                            {
                                var info = string.Format("Leistung löschen am {0:dd.MM.yyyy} {1}x{2}, bei ({3}, {4})", l.Datum, l.Anzahl, l.Leistung, l.Adressnummer, await _nameProvider.GetName(n.Adressnummer));
                                var sqlDeleteInfo = SqlDeleteInfo.Create("Leistungen", "ID", l.ID);

                                cmds.Add(new OleDbSQLDeleteCommand(sqlDeleteInfo, info));
                            }
                        }
                        else
                        {
                            foreach (var l in ls.Where(x => !x.Übertragen.HasValue))
                            {
                                var info = string.Format("Leistung löschen am {0:dd.MM.yyyy} {1}x{2}, bei ({3}, {4})", l.Datum, l.Anzahl, l.Leistung, l.Adressnummer, await _nameProvider.GetName(n.Adressnummer));

                                var deleteInfo = SqlDeleteInfo.Create("Leistungen", "ID", l.ID);

                                cmds.Add(new OleDbSQLDeleteCommand(deleteInfo, info));
                            }

                            foreach (var l in ls.Where(x => x.Übertragen.HasValue))
                            {
                                var info = string.Format("Leistung löschen (Wert ist bereits übertragen!) am {0:dd.MM.yyyy} {1}x{2}, bei ({3}, {4})", l.Datum, l.Anzahl, l.Leistung, l.Adressnummer, await _nameProvider.GetName(n.Adressnummer));

                                var deleteInfo = SqlDeleteInfo.Create("Leistungen", "ID", l.ID);

                                cmds.Add(new OleDbSQLDeleteCommand(deleteInfo, info));
                            }
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }

                result.Add(new ChangeInfo(diffsPack, cmds.ToArray()));
            }

            return result;
        }
        
        private string GetAddEntryCommand(int pfleger, int adressnummer, DateTime datum, int leistung, double anzahl)
        {
            var cmdText = $@"insert into [Leistungen] ([Adressnummer],[Pfleger], [Datum], [Leistung], [Anzahl], [eingespielt_am], [transdok]) 
                values ({adressnummer}, {pfleger}, {SqlValueHelper.EscapeValue(datum)}, {leistung}, {SqlValueHelper.EscapeValue(anzahl)}, {SqlValueHelper.EscapeValue(DateTime.Now.Clip())}, -1)";

            return cmdText;
        }
        #endregion
    }
}
