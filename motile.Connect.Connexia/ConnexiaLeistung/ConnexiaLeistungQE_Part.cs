﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaLeistungQE_Part : ConnexiaBaseQE_Part<ConnexiaLeistung>
    {
        private readonly ConnexiaSettings _settings;

        public class L
        {
            public int ID { get; set; }

            public int Pflegernummer { get; set; }

            public int Adressnummer { get; set; }

            public DateTime Datum { get; set; }

            public int Leistung { get; set; }

            public double Anzahl { get; set; }

            public DateTime? Übertragen { get; set; }

            public DateTime Eingespielt { get; set; }
        }

        #region - ctr



        public ConnexiaLeistungQE_Part(OleDbConnection connection, ConnexiaSettings settings)
            : base(connection)
        {
            _settings = settings;
        }
        #endregion

        #region -  methods
        public async Task<List<L>> GetLs(int adressnummer, int pfleger, DateTime datum, int leistung)
        {
            var result = new List<L>();

            var query = @"SELECT ID, Anzahl, eingespielt_am, übertragen_am  
FROM [Leistungen] 
WHERE Adressnummer= @adressnummer AND Pfleger = @pfleger AND Datum = @datum AND Leistung = @leistung";

            if (_settings.OnlyTransdok)
            {
                query = query + " and transdok = -1";
            }

            var rows = await _connection.QueryAsync(query, new { adressnummer = adressnummer, pfleger = pfleger, datum = datum, leistung = leistung });


            foreach (var row in rows)
            {
                result.Add(new L()
                {
                    Adressnummer = adressnummer,
                    Pflegernummer = pfleger,
                    Leistung = leistung,
                    Datum = datum,
                    ID = row.ID,
                    Anzahl = row.Anzahl,
                    Eingespielt = row.eingespielt_am,
                    Übertragen = row.übertragen_am
                });
            }



            return result;
        }
        #endregion

        #region -  private methods
        protected override async Task<ConnexiaLeistung> GetDTOAsync(string id, OleDbConnection connection)
        {
            ConnexiaLeistung dto = null;


            DateTime from;
            DateTime to;
            int pflegernummer;

            ConnexiaLeistung.GetIDFaktoren(id, out pflegernummer, out from, out to);

            var query = @"SELECT Datum, Adressnummer, Leistung, Anzahl 
FROM Leistungen 
WHERE Pfleger=@pflegernummer AND (Datum >= @from) AND (Datum <= @to)" + (_settings.OnlyTransdok ? " AND transdok = -1" : "") + @"
ORDER BY Datum, Adressnummer, Leistung, Anzahl";

            // derzeit nur Leistungen kleiner oder gleich 17 berücksichtigen (Beratung ausklammern)
            var rows = await connection.QueryAsync(query, new { pflegernummer = pflegernummer, from = from, to = to });


            dto = new ConnexiaLeistung() { Pflegernummer = pflegernummer, Von = from, Bis = to };
            foreach (var row in rows)
            {
                dto.Liste.Add(new ConnexiaLeistung.Eintrag()
                {
                    Datum = row.Datum,
                    Adressnummer = row.Adressnummer,
                    Leistung = row.Leistung,
                    Anzahl = row.Anzahl
                });
            }

            return dto;
        }
        #endregion
    }
}
