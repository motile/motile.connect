﻿using Autofac;
using motile.Connect.CQ;
using System.Data.OleDb;

namespace motile.Connect.Connexia
{
    public class ConnexiaContainerProvider : ConnexiaContainerProviderDiff
    {
        private ConnexiaSettings _settings;

        #region - ctr
        public ConnexiaContainerProvider(ConnexiaSettings settings, IDialogManager dlgMgr)
            : base(dlgMgr)
        {
            _settings = settings;
        }
        #endregion

        protected override void ConfigBuilder(ContainerBuilder builder)
        {
            builder.RegisterInstance(_settings);                      
            
            builder.RegisterType<ConnexiaCommandProcessor>().As<ICommandExecutor>();

            builder.RegisterType<ConnexiaDBStateChangeInfoGenerator>().As<IChangeInfoGenerator<ConnexiaDBState>>();
            builder.RegisterType<ConnexiaDBStateRepository>().As<IQueryEnginePart<ConnexiaDBState, DtoQuery<ConnexiaDBState>>>();

            builder.RegisterType<ConnexiaFallListeChangeInfoGenerator>().As<IChangeInfoGenerator<ConnexiaFallListe>>();
            builder.RegisterType<ConnexiaFallListeQU_Part>().As<IQueryEnginePart<ConnexiaFallListe, DtoQuery<ConnexiaFallListe>>>();
            builder.RegisterType<ConnexiaFallListeQU_Part>().AsSelf();

            builder.RegisterType<ConnexiaFallChangeInfoGenerator>().As<IChangeInfoGenerator<ConnexiaFall>>();
            builder.RegisterType<ConnexiaFallQE_Part>().As<IQueryEnginePart<ConnexiaFall, DtoQuery<ConnexiaFall>>>();
            builder.RegisterType<ConnexiaFallQE_Part>().AsSelf();

            builder.RegisterType<ConnexiaLeistungChangeInfoGenerator>().As<IChangeInfoGenerator<ConnexiaLeistung>>();
            builder.RegisterType<ConnexiaLeistungQE_Part>().As<IQueryEnginePart<ConnexiaLeistung, DtoQuery<ConnexiaLeistung>>>();
            builder.RegisterType<ConnexiaLeistungQE_Part>().AsSelf();

            builder.RegisterType<ConnexiaPflegerChangeInfoGenerator>().As<IChangeInfoGenerator<ConnexiaPfleger>>();
            builder.RegisterType<ConnexiaPflegerQE_Part>().As<IQueryEnginePart<ConnexiaPfleger, DtoQuery<ConnexiaPfleger>>>();
            builder.RegisterType<ConnexiaPflegerQE_Part>().AsSelf();
            
            builder.RegisterType<CommandExecutor>().AsSelf();
            builder.RegisterGeneric(typeof(Connector<>)).AsSelf();

            builder.RegisterType<DBInfoProvider>().As<IDBInfoProvider>().InstancePerMatchingLifetimeScope(CompositionHelper.LifetimeTag);


            builder.Register(c =>
            {
                var conn = new OleDbConnection(c.Resolve<ConnexiaSettings>().ConnectionString);
                conn.Open();
                return conn;
            }).OnRelease(instance =>
            {
                instance.Close();
                instance.Dispose();
            }).InstancePerMatchingLifetimeScope(CompositionHelper.LifetimeTag).AsSelf();


            builder.RegisterType<CityProvider>().SingleInstance();
            builder.RegisterType<CountryProvider>().SingleInstance();

            base.ConfigBuilder(builder);
        }
    }
}
