﻿using motile.Connect.CQ;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class NameOfPflegerProvider
    {
        private readonly IDictionary<int, string> _names = new SortedDictionary<int, string>();
        private readonly AsyncLock _lock = new AsyncLock();
        private readonly IQueryEngine _queryEngine;

        public NameOfPflegerProvider(IQueryEngine queryEngine)
        {
            _queryEngine = queryEngine;
        }

        public async Task<string> GetName(int id)
        {
            string result;
            if (!_names.ContainsKey(id))
            {
                using (await _lock.LockAsync())
                {
                    var p = (await _queryEngine.QueryAsync(DTOTypeProvider.CreateQuery<ConnexiaPfleger>(ConnexiaPfleger.GetID(id)))).Results.FirstOrDefault();

                    if (p != null)
                    {
                        result = $"{p.Nachname} {p.Vorname}".Trim();

                    }
                    else
                    {
                        result = $"unbekannte Pfleger ({id})";
                    }
                    _names.Add(id, result);
                }
            }
            else
            {
                result = _names[id];
            }

            return result;
        }
    }
}
