﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaDBStateRepository : ConnexiaBaseQE_Part<ConnexiaDBState>
    {
        #region - ctr
        public ConnexiaDBStateRepository(OleDbConnection connection)
            : base(connection)
        {

        }
        #endregion

        #region - private methods
        protected override async Task<ConnexiaDBState> GetDTOAsync(string id, OleDbConnection connection)
        {
            var result = new ConnexiaDBState();

            result.HasNoDokuNullValues = !(await connection.QueryAsync(@"SELECT Doku.Wert FROM Doku WHERE Doku.Wert Is Null")).Any();
            
            result.HasTableLeistungAColumnTransdok = HasTableLeistungAColumnTransdok(connection);

            result.HasTransdokLogTable = HasTransdokLogTable(connection);

            return result;
        }


        private bool HasTableLeistungAColumnTransdok(OleDbConnection connection)
        {
            bool result;

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 transdok FROM [Leistungen]";
                try
                {
                    cmd.ExecuteReader();
                    result = true;
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }

        private bool HasTransdokLogTable(OleDbConnection connection)
        {
            bool result;

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 ID FROM [TransdokLog]";
                try
                {
                    cmd.ExecuteReader();
                    result = true;
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
        #endregion
    }
}