﻿using System.Runtime.Serialization;

namespace motile.Connect.Connexia
{
    [DataContract]
    public class ConnexiaDBState : IDTO
    {
        public ConnexiaDBState()
        {
            this.HasNoDokuNullValues = true;
            this.HasTableLeistungAColumnTransdok = true;
            this.HasTransdokLogTable = true;
        }

        [DataMember]
        public bool HasNoDokuNullValues { get; set; }

        [DataMember]
        public bool HasTableLeistungAColumnTransdok { get; set; }

        [DataMember]
        public bool HasTransdokLogTable { get; set; }
        
        public DtoKey DtoKey { get { return new DtoKey(DTOTypeProvider.ConnexiaDBState, "1"); } }


        public override string ToString()
        {
            return "Datenbank";
        }
    }
}
