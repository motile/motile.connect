﻿using System.Collections.Generic;

namespace motile.Connect.Connexia
{
    public class ConnexiaDBStateFindDifferencesStrategy : IFindDifferencesStrategy<ConnexiaDBState>
    {
        public IEnumerable<DiffsPack> GetDifferences(ConnexiaDBState master, ConnexiaDBState changeThis)
        {
            if (master.HasNoDokuNullValues != changeThis.HasNoDokuNullValues)
            {
                yield return new DiffsPack(master.DtoKey, new ChangeProperty { Path = atstring.of(changeThis, (x) => x.HasNoDokuNullValues), OldValue = changeThis.HasNoDokuNullValues, NewValue = master.HasNoDokuNullValues });
            }

            if (master.HasTableLeistungAColumnTransdok != changeThis.HasTableLeistungAColumnTransdok)
            {
                yield return new DiffsPack(master.DtoKey, new ChangeProperty { Path = atstring.of(changeThis, (x) => x.HasTableLeistungAColumnTransdok), OldValue = changeThis.HasTableLeistungAColumnTransdok, NewValue = master.HasTableLeistungAColumnTransdok });
            }

            if (master.HasTransdokLogTable != changeThis.HasTransdokLogTable)
            {
                yield return new DiffsPack(master.DtoKey, new ChangeProperty { Path = atstring.of(changeThis, (x) => x.HasTransdokLogTable), OldValue = changeThis.HasTransdokLogTable, NewValue = master.HasTransdokLogTable });
            }
        }
    }
}
