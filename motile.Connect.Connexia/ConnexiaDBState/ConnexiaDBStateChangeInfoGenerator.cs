﻿using motile.Connect.CQ;
using motile.Connect.CQ.OleDb;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class ConnexiaDBStateChangeInfoGenerator : IChangeInfoGenerator<ConnexiaDBState>
    {
        public Task<IEnumerable<ChangeInfo>> Process(IEnumerable<DiffsPack> diffsPacks)
        {
            var result = new List<ChangeInfo>();

            foreach (var diffsPack in diffsPacks)
            {
                var diffs = diffsPack.Diffs.ToList();
                var cmds = new List<Command>();

                while (diffs.Any())
                {
                    var diff = diffs[0];
                    diffs.RemoveAt(0);

                    if (diff.IsPath<ConnexiaDBState, bool>((x) => x.HasNoDokuNullValues))
                    {
                        var cmdText = "UPDATE Doku SET Doku.Wert='' WHERE Doku.Wert Is Null";

                        cmds.Add(new OleDbBaseCommand(cmdText, "Überprüfe Doku null-Werte"));
                    }
                    else if (diff.IsPath<ConnexiaDBState, bool>((x) => x.HasTableLeistungAColumnTransdok))
                    {
                        if (!(bool)((ChangeProperty)diff).OldValue)
                        {

                            var cmdText = "ALTER TABLE [Leistungen] ADD COLUMN [transdok] bit";

                            cmds.Add(new OleDbBaseCommand(cmdText, "Spalte Transdok anlegen"));
                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                    else if (diff.IsPath<ConnexiaDBState, bool>((x) => x.HasTransdokLogTable))
                    {
                        if (!(bool)((ChangeProperty)diff).OldValue)
                        {
                            var cmdText = @"CREATE TABLE TransdokLog([ID] COUNTER PRIMARY KEY, [Timestamp] datetime, [Entity] CHAR(255), [Domain] CHAR(255), [Info] CHAR(255), [Sql] NOTE)";

                            cmds.Add(new OleDbBaseCommand(cmdText, "Tabelle TransdokLog anlegen"));

                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                }

                result.Add(new ChangeInfo(diffsPack, cmds.ToArray()));
            }

            return Task.FromResult<IEnumerable<ChangeInfo>>(result);
        }
    }
}
