﻿using Autofac;
using System.Threading.Tasks;
using System;
using System.Linq;
using motile.Connect.CQ;

namespace motile.Connect.Connexia
{

    public class DTODefaultDescriptionProvider<T> : IDTODescriptionProvider<T>
        where T : IDTO
    {
        private readonly IQueryEngine _queryEngine;

        public DTODefaultDescriptionProvider(IQueryEngine queryEngine)
        {
            _queryEngine = queryEngine;
        }
        public async Task<string> WithIDAsync(string id)
        {
            var dto = (await _queryEngine.QueryAsync<T>(DTOTypeProvider.CreateQuery<T>(id))).Results.FirstOrDefault();

            if (dto != null)
                return dto.ToString();

            return null;
        }
    }

    public class DTODescriptionProvider : IDTODescriptionProvider
    {
        private readonly IComponentContext _container;


        public DTODescriptionProvider(IComponentContext container)
        {
            _container = container;

        }

        public Task<string> WithIDAsync(DtoKey key)
        {
            var typeProvider = _container.Resolve<IDTOTypeProvider>();
            var type = typeProvider.GetTypeFromKey(key.TypeKey);

            var m = this.GetType().GetMethods(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).Where(x => x.Name == "WidthIDAsync_intern").First().MakeGenericMethod(type);

            var t = (Task<string>)m.Invoke(this, new[] { key.Id });

            return t;
        }

        protected async Task<string> WidthIDAsync_intern<T>(string id)
            where T : IDTO
        {

            string result = null;

            IDTODescriptionProvider<T> dp;

            if (_container.TryResolve<IDTODescriptionProvider<T>>(out dp))
            {
                result = await dp.WithIDAsync(id);
            }
            else
            {

                var typeProvider = _container.Resolve<IDTOTypeProvider>();
                var qe = _container.Resolve<IQueryEngine>();

                var entity = (await qe.QueryAsync(typeProvider.CreateQuery<T>(id))).Results.FirstOrDefault();

                if (entity != null)
                {
                    result = entity.ToString();
                }
            }

            return result;
        }
    }
}
