﻿using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public interface IPflegerIsinRoleProvider
    {
        Task<bool> IsInRole(int pflegernummer, string role);
    }
}
