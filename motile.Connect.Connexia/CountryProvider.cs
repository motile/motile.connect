﻿using System.Collections.Generic;
using System.Data.OleDb;

namespace motile.Connect.Connexia
{
    public class CountryProvider
    {
        #region - fields
        private static Dictionary<string, string> _countries;  //einfach gestricktes singleton
        #endregion


        #region - ctr
        public CountryProvider(ConnexiaSettings settings)
        {
            LoadCountriesFromConnexia(settings.ConnectionString);
        }
        #endregion

        #region - properties
        public IReadOnlyDictionary<string, string> Countries { get { return _countries; } }
        #endregion

        #region - methods
        private void LoadCountriesFromConnexia(string connectionString)
        {
            if (_countries == null)
            {
                _countries = new Dictionary<string, string>();
                using (var connection = new OleDbConnection(connectionString))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "SELECT Land, LänderText FROM tb_länder";

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var a = reader.GetFieldValue<string>(0);
                            var b = reader.GetFieldValue<string>(1);
                            if (!_countries.ContainsKey(a))
                            {
                                _countries.Add(a, b);
                            }
                        }

                        reader.Close();
                    }
                    connection.Close();
                }
            }
        }
        #endregion
    }
}
