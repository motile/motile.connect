﻿using Dapper;
using System.Data.OleDb;

namespace motile.Connect.Connexia
{
    public class DBInfoProvider : IDBInfoProvider
    {
        #region - ctr
        public DBInfoProvider(OleDbConnection connection)
        {
            Init(connection);
        }
        #endregion

        #region - properties
        public string Name { get; private set; }
        #endregion

        #region - methods
        private void Init(OleDbConnection connection)
        {
            this.Name = connection.ExecuteScalar<string>("select top 1 Bezeichnung from Verein");
        }
        #endregion
    }
}
