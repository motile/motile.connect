﻿using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace motile.Connect.Connexia
{
    public class PflegerIsinRoleProvider : IPflegerIsinRoleProvider
    {
        private readonly Dictionary<int, bool> _cache = new Dictionary<int, bool>();
        private readonly AsyncLock _lock = new AsyncLock();
        private static bool _disabled = false;
        private readonly string _idSrv;

        public PflegerIsinRoleProvider()
        {
            this._idSrv = ConfigurationManager.AppSettings["IdentityServer"] ?? "https://identity.transdok.at";

            _disabled = string.IsNullOrEmpty(_idSrv);
        }

        public async Task<bool> IsInRole(int pflegernummer, string role)
        {
            if (_cache.ContainsKey(pflegernummer))
                return _cache[pflegernummer];

            using (await _lock.LockAsync())
            {
                if (_cache.ContainsKey(pflegernummer))
                    return _cache[pflegernummer];

                var result = await IsInRoleFromIdentityServer(pflegernummer, role);

                _cache.Add(pflegernummer, result);

                return result;
            }
        }

        private async Task<bool> IsInRoleFromIdentityServer(int pflegernummer, string role)
        {
            bool result = false;

            if (_disabled)
                return false;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync($"{_idSrv}/api/info/external/{pflegernummer}/inrole/auszubildend");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var s = await response.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(s))
                    {
                        result = Newtonsoft.Json.JsonConvert.DeserializeObject<bool>(s);
                    }
                }
                else
                {
                    _disabled = true;
                }
            }

            return result;
        }
    }
}
