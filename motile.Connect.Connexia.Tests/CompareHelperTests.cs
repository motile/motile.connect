﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class CompareHelperTests
    {
        public class Test
        {
            [StringLength(3)]
            public string A { get; set; }

            [StringLength(1, MinimumLength = 1)]
            public string B { get; set; }
        }


        [Fact]
        public void CompareString_PropertiesAreEqual_ReturnsEmptyList()
        {
            var t1 = new Test() { A = "123" };
            var t2 = new Test() { A = "123" };

            var c = CompareHelper.CompareString(t1, t2, x => x.A);

            Assert.Empty(c);
        }

        [Fact]
        public void CompareString_PropertiesAreNotEqual_ReturnsChangeEntry()
        {
            var t1 = new Test() { A = "1234" };
            var t2 = new Test() { A = "12" };

            var change = CompareHelper.CompareString(t1, t2, x => x.A).Single();

            Assert.Equal("12", change.OldValue);
            Assert.Equal("123", change.NewValue);
            Assert.Equal("A", change.Path);

        }

        [Fact]
        public void CompareString_PropertiesAreNotEqualButDiffernsBeyondMaxLenght_ReturnsEmptyList()
        {
            var t1 = new Test() { A = "123" };
            var t2 = new Test() { A = "1234" };

            var c = CompareHelper.CompareString(t1, t2, x => x.A);

            Assert.Empty(c);
        }

        [Fact]
        public void GetStringLengthValue_StringMinimumlengthIsSet_EmptyString_ReturnsNull()
        {
            var t1 = new Test() { B = "" };

            var c = CompareHelper.GetStringLengthValue(t1, x => t1.B);

            Assert.Null(c);
        }
    }
}
