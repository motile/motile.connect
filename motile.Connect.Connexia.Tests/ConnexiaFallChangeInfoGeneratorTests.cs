﻿using motile.Connect.CQ.OleDb;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaFallChangeInfoGeneratorTests
    {
        [Fact]
        public async Task InsertDoku_LengthOfWertToLong_ReturnsExpectedValue()
        {
            int len = 20;

            var cfmaster = new ConnexiaFall() { Adressnummer = -1 };
            var cfchangeThis = new ConnexiaFall() { Adressnummer = -1 };
            cfmaster.Dokus.Add(new ConnexiaFall.Doku() { Wert = new string('.', len + 10), Gruppe = "01", Detail = "01", Datum = DateTime.Today });

            var s = new ConnexiaFallFindDifferencesStrategy();
            var diffsPacks = s.GetDifferences(cfmaster, cfchangeThis);

            var gen = new ConnexiaFallChangeInfoGenerator(null, null, null);
            var changeInfos = await gen.Process(diffsPacks);
            var cmd = (OleDbSQLInsertCommand)changeInfos.Single().Commands.Single();

            // der Wert in Insertskript wurde entsprechend gestutzt?
            Assert.Contains(string.Format("'{0}'", new string('.', len)), cmd.GetCommand().CommandText);
        }

        [Fact]
        public async Task UpdateMitgliedsnummer_SetNintToNull_ReturnsExpectedValue()
        {
            var cfmaster = new ConnexiaFall() { Mitgliedsnummer = null };
            var cfchangeThis = new ConnexiaFall() { Mitgliedsnummer = 1 };

            var s = new ConnexiaFallFindDifferencesStrategy();
            var diffsPacks = s.GetDifferences(cfmaster, cfchangeThis);

            var gen = new ConnexiaFallChangeInfoGenerator(null, null, null);
            var changeInfos = await gen.Process(diffsPacks);
            var cmd = (OleDbSQLUpdateCommand)changeInfos.Single().Commands.Single();

            Assert.Contains("[mitgliedsnummer]=null", cmd.GetCommand().CommandText.ToLower().Replace(" ", ""));
        }

        [Fact]
        public async Task CreateDoku_SetWertNull_ReturnsStringEmpty()
        {
            var cfmaster = new ConnexiaFall() { Adressnummer = -1 };
            var cfchangeThis = new ConnexiaFall() { Adressnummer = -1 };
            cfmaster.Dokus.Add(new ConnexiaFall.Doku() { Wert = null, Gruppe = "01", Detail = "01", Datum = DateTime.Today });

            var s = new ConnexiaFallFindDifferencesStrategy();
            var diffsPacks = s.GetDifferences(cfmaster, cfchangeThis);

            var gen = new ConnexiaFallChangeInfoGenerator(null, null, null);
            var changeInfos = await gen.Process(diffsPacks);
            var cmd = (OleDbSQLInsertCommand)changeInfos.Single().Commands.Single();

            Assert.DoesNotContain("null", cmd.GetCommand().CommandText);
        }
    }
}
