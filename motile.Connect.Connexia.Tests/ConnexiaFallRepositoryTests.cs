﻿using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaFallRepositoryTests
    {
        [Fact]
        public void SplitName_SomeNameInOneString_ReturnsName1AndName2Seperated()
        {
            var name = "Mustermann-Frau Franz Maria";

            var result = ConnexiaFallQE_Part.SplitName(name);

            Assert.Equal("Mustermann-Frau", result.Item1);
            Assert.Equal("Franz Maria", result.Item2);
        }
    }
}
