﻿using System;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaLeistungTests
    {
        [Fact]
        public void GetIDFaktoren_SomeID_AssignsOutParameters()
        {
            ConnexiaLeistung.GetIDFaktoren("12_20010201_20011110", out int pflegernummer, out DateTime von, out DateTime bis);

            Assert.Equal(12, pflegernummer);
            Assert.Equal("01.02.2001 00:00", von.ToString("dd.MM.yyyy HH:mm"));
            Assert.Equal("10.11.2001 23:59", bis.ToString("dd.MM.yyyy HH:mm"));
        }

        [Fact]
        public void GetIDFaktoren_SomeNegativeID_AssignsOutParameters()
        {
            ConnexiaLeistung.GetIDFaktoren("-12_20010201_20011110", out int pflegernummer, out DateTime von, out DateTime bis);

            Assert.Equal(-12, pflegernummer);
            Assert.Equal("01.02.2001 00:00", von.ToString("dd.MM.yyyy HH:mm"));
            Assert.Equal("10.11.2001 23:59", bis.ToString("dd.MM.yyyy HH:mm"));
        }

        [Fact]
        public void DtoKey_Id()
        {
            var von = new DateTime(2014, 09, 1);
            var bis = von.AddMonths(1).AddDays(-1);

            var dto = new ConnexiaLeistung() { Pflegernummer = 11, Von = von, Bis = bis };

            var id = dto.DtoKey.Id;

            Assert.Equal("11_20140901_20140930", id);
        }
    }
}
