﻿using Autofac;
using motile.Connect.Autofac;
using motile.Connect.Connexia.Fake;
using motile.Connect.Connexia.Validation;
using motile.Connect.Validation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia
{

    public partial class ConnexiaFakeDataTests : IDisposable
    {
        #region - buildup
        /// <summary>
        /// Grundlagen für alle Tests
        /// </summary>
        private ConnexiaFakeDataProvider _fakeDataProvider;
        private IContainer _container;
        private ILifetimeScope _scope;

        public ConnexiaFakeDataTests()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("de");

            _fakeDataProvider = new ConnexiaFakeDataProvider();
        }



        private ConnexiaValidator GetValidator(FakeDataOptions options = null)
        {
            if (_container == null)
            {
                var cp = new ConnexiaFakeContainerProvider(options);
                _container = cp.GetContainer();
                _scope = _container.BeginLifetimeScope(CompositionHelper.LifetimeTag);
            }

            return new ConnexiaValidator(_scope);
        }
        #endregion

        #region - private Testhelper Methods
        /// <summary>
        /// Hilfsmethoden, um die einzelnen Tests einfach zu halten
        /// </summary>
        private ConnexiaFakeDataContainer GetTestDaten()
        {
            return new ConnexiaFakeDataContainer() { Fall = _fakeDataProvider.GetStandardFall() };
        }

        private ConnexiaFakeDataContainer GetTestDatenFürNichtKlient()
        {
            return new ConnexiaFakeDataContainer() { Fall = _fakeDataProvider.GetStandardFallNichtKlient() };
        }

        /// <summary>
        /// Methode zur Durchführung eines Tests
        /// </summary>
        /// <param name="data">Die zu testenden Daten</param>
        /// <param name="severity">(optional) Der erwartete Schweregrad (Error oder Warning)</param>
        /// <param name="message">(optional) Die erwartete Fehlermeldung</param>
        /// <param name="validatorType">(optional) Der Typ des Validators, der den Fehler melden soll.</param>
        /// <returns></returns>
        private async Task Test(ConnexiaFakeDataContainer data, string severity = null, string message = null, Type validatorType = null, bool emptyResult = false)
        {
            string validatorID = null;
            if (validatorType != null)
            {
                validatorID = (string)validatorType.GetFields().Where(x => x.IsLiteral && x.Name == "ValidatorID").FirstOrDefault().GetValue(null);
            }

            var options = new FakeDataOptions() { Fälle = new[] { data.Fall } };

            var validator = GetValidator(options);
            var validationResults = new List<ValidationInfo>();

            validationResults.AddRange(await validator.ValidateAsync(data.Fall));

            if (data.Leistungen != null)
            {
                validationResults.AddRange(await validator.ValidateAsync(data.Leistungen));
            }

            if (!string.IsNullOrEmpty(validatorID))
            {
                validationResults.RemoveAll(x => x.ValidatorID != validatorID);
            }

            var result = validationResults.SingleOrDefault();

            if (emptyResult)
            {
                Assert.Null(result);
                return;
            }

            Assert.NotNull(result);

            if (!string.IsNullOrEmpty(severity))
            {                
                Assert.Equal(result.Severity, severity);
            }

            if (!string.IsNullOrEmpty(message))
            {
                Assert.Contains(message, result.Message);
            }
        }

        private async Task TestForNoErrorOrWarnung(ConnexiaFakeDataContainer data)
        {
            var options = new FakeDataOptions() { Fälle = new[] { data.Fall } };

            var validator = GetValidator(options);
            var validationResults = new List<ValidationInfo>();

            validationResults.AddRange(await validator.ValidateAsync(data.Fall));

            if (data.Leistungen != null)
            {
                validationResults.AddRange(await validator.ValidateAsync(data.Leistungen));
            }

            Assert.True(validationResults.Count() == 0);
        }

        public void Dispose()
        {
            if (_scope != null)
            {
                _scope.Dispose();
            }
            if (_container != null)
            {
                _container.Dispose();
            }
        }
        #endregion
    }   
    
}