﻿using motile.Connect.Connexia.Fake;
using motile.Connect.Validation;
using System;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia
{
    public partial class ConnexiaFakeDataTests
    {

        #region - tests: Klienten
        [Fact]
        public async Task Fall_OhneGeburtsdatum_EinFehler()
        {
            var daten = GetTestDaten()              //Aufbau des Falls
                .Mit(x => x.Geburtsdatum, null);    //Änderung des Wertes "Geburtsdatum" auf leer

            await Test(daten, message: "'Geburtsdatum' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_OhneAdresse_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Adresse, null);

            await Test(daten, message: "'Adresse' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_OhneLand_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Land, null);

            await Test(daten, message: "'Land' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_OhnePLZ_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.PLZ, null);

            await Test(daten, message: "'PLZ' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_OhneOrt_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Ort, null);

            await Test(daten, message: "'Ort' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_OhneGeschlecht_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Geschlecht, null);

            await Test(daten, message: "'Geschlecht' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_OhneStaatsbuergerschaft_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Staatsbuergerschaft, null);

            await Test(daten, message: "'Staatsbuergerschaft' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_GeburtsdatumNachHeute_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Geburtsdatum, new DateTime(year: 2040, month: 01, day: 01))
                .Mit(x => x.Versicherungsnummer, "2033-01.01.40");


            await Test(daten, message: $@"Der Wert von 'Geburtsdatum' muss kleiner sein als '{DateTime.Today:dd.MM.yyyy}");
        }

        [Fact]
        public async Task Fall_GeburtsdatumVor1900_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Geburtsdatum, new DateTime(year: 1801, month: 01, day: 01));

            await Test(daten, message: $@"Der Wert von 'Geburtsdatum' muss grösser oder gleich '01.01.1900 00:00:00' sein.");
        }

        [Fact]
        public async Task Fall_GeburtsdatumUngleichDatumDerVersicherungsnummer_EineWarnung()
        {
            var datum = new DateTime(year: 1999, month: 01, day: 01);

            var daten = GetTestDaten()
                .Mit(x => x.Geburtsdatum, datum)
                .Mit(x => x.Versicherungsnummer, "1235-01.01.01");


            await Test(daten, severity: ValidationInfo.Warning, message: $@"Das Geburtsdatum {datum:dd.MM.yy} unterscheidet sich vom Wert in der Versicherungsnummer");
        }

        [Fact]
        public async Task Fall_OhneVersicherung_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Versicherung, null);

            await Test(daten, message: @"'Versicherung' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_OhneVersicherungsnummer_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Versicherungsnummer, null);

            await Test(daten, message: @"'Versicherungsnummer' darf nicht leer sein");
        }

        [Fact]
        public async Task Fall_VersicherungsnummerUngültig_EinFehler()
        {
            var daten = GetTestDaten()
                .Mit(x => x.Versicherungsnummer, "1234-01.01.01");

            await Test(daten, severity: ValidationInfo.Error, message: @"Die Versicherungsnummer 1234-01.01.01 ist nicht korrekt");
        }
        #endregion

        #region - tests: Klienten Dok-Daten
        [Fact]
        public async Task FallDoku_Gruppe21Fehlt_EinFehler()
        {
            var daten = GetTestDaten()
                .MitDoku(gruppe: "21", detail: "");

            await Test(daten, message: @"Fehlender Eintrag für Feld: 21");
        }
        #endregion

        #region - tests: Leistungen
        [Fact]
        public async Task Leistung_MitarbeiterOhneNummer_EinFehler()
        {
            var daten = GetTestDaten()
                .MitLeistungen(pflegernummer: null, leistungen: new int[] { 2, 15 });

            await Test(daten, message: @"Ungültige Pflegernummer");
        }

        [Fact]
        public async Task Leistung_OhneEinsZweiOderDrei_EinFehler()
        {
            var daten = GetTestDaten()
              .MitLeistungen(leistungen: new int[] { 6, 15 });

            await Test(daten, message: @"Eintrag ist unvollständig: Kein Eintrag 1,2,3");
        }

        [Fact]
        public async Task Leistung2_OhneLeistung_einFehler()
        {
            var daten = GetTestDaten()
             .MitLeistungen(leistungen: new int[] { 2 });

            await Test(daten, message: @"Eintrag ist unvollständig: Keine Leistung bei einem Eintrag 2, 3");
        }

        [Fact]
        public async Task Leistung3_OhneLeistung_einFehler()
        {
            var daten = GetTestDaten()
             .MitLeistungen(leistungen: new int[] { 3 });

            await Test(daten, message: @"Eintrag ist unvollständig: Keine Leistung bei einem Eintrag 2, 3");
        }

        [Fact]
        public async Task Leistung1_OhneLeistung_keinFehler()
        {
            var daten = GetTestDaten()
             .MitLeistungen(leistungen: new int[] { 1 });

            await Test(daten, emptyResult: true);
        }

        [Fact]
        public async Task Leistung15_Ohne123_keinFehler()
        {
            var daten = GetTestDaten()
             .MitLeistungen(leistungen: new int[] { 15 });

            await Test(daten, emptyResult: true);
        }

        [Fact]
        public async Task Leistung_31bis34BeiKlient_EinFehler()
        {
            var daten = GetTestDaten()
             .MitLeistungen(leistungen: new int[] { 2, 15, 31, 32, 33, 34 });

            await Test(daten, message: $@"Beim Klient 'Testklient Test' wurden ungültige Beratungsleistungen dokumentiert! (31,32,33,34) [{DateTime.Today.ToString("dd.MM.yyyy")}]");
        }

        [Fact]
        public async Task Leistung_ZweiUndFünfzehnBeiNichtKlient_EinFehler()
        {
            var daten = GetTestDatenFürNichtKlient()
             .MitLeistungen(leistungen: new int[] { 2, 15, 33, 34 });

            await Test(daten, message: $@"Beim Beratungsklienten 'Testklient Beratung' wurden ungültige Leistungen dokumentiert! (2,15) [{DateTime.Today.ToString("dd.MM.yyyy")}]");
        }        

        [Fact]
        public async Task Leistung_NichtKumuliert_EinFehler()
        {
            var daten = GetTestDaten()
                .MitLeistungen(leistungen: new int[] { 2, 6, 15 })
                .MitLeistungen(leistungen: new int[] { 6 });

            await Test(daten, message: @"Die Einträge sind nicht kumuliert");
        }

        [Fact]
        public async Task Leistung_AnzahlÜberFünf_EineWarnung()
        {
            var daten = GetTestDaten()
                .MitLeistungen(leistungen: new int[] { 2, 15, 15, 15, 15, 15, 15 });

            await Test(daten, message: @"Es gibt Leistungen mit der Anzahl größer 5");
        }

        [Fact]
        public async Task Leistung_AnzahlÜberFünfBeiNichtKlient_KeinFehlerUndKeineWarnung()
        {
            var daten = GetTestDatenFürNichtKlient()
                .MitLeistungen(leistungen: new int[] { 31, 31, 31, 31, 31, 31, 32, 32, 32, 32, 32, 32, 33, 33, 33, 33, 33, 33, 34, 34, 34, 34, 34, 34 });

            //Leistungen 31 bis 34 dürfen über 5 sein. Dieser Test darf keine Warnung bringen
            await TestForNoErrorOrWarnung(daten);
        }

        [Fact]
        public async Task Leistung_AuszubildenderMitMedizinischenLeistungen_EinFehler()
        {
            var daten = GetTestDaten()
                .MitLeistungen(leistungen: new int[] { 2, 6, 15 }, pflegernummer: FakeConstants.AuszubildenenPflegerNr);

            await Test(daten, message: $"Pflegernummer {FakeConstants.AuszubildenenPflegerNr} darf als Auszubildende/r keine medizinischen Leistungen (6-10) dokumentieren");
        }

        #endregion


    }
}