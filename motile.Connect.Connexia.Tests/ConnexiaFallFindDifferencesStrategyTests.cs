﻿using System;
using System.Linq;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaFallFindDifferencesStrategyTests
    {
        [Fact]
        public void GetDifferences_EqualDTO_ReturnsEmptyList()
        {
            var s = new ConnexiaFallFindDifferencesStrategy();

            var cf = new ConnexiaFall() { Adressnummer = -1, Adresse = "Teststraße 1", Geschlecht = "m", Geburtsdatum = new DateTime(1950, 10, 15), Land = "AT", PLZ = "6900", Ort = "Bregenz", Mitgliedsnummer = 10, Nachname = "Test", Vorname = "TT" };

            var result = s.GetDifferences(cf, cf);

            Assert.Empty(result);
        }


        [Fact]
        public void GetDifferences_ssUndß_ReturnsDifference()
        {
            var s = new ConnexiaFallFindDifferencesStrategy();

            var cf1 = new ConnexiaFall() { Adressnummer = -1, Adresse = "Teststraße 1" };
            var cf2 = new ConnexiaFall() { Adressnummer = -1, Adresse = "Teststrasse 1" };

            var result = s.GetDifferences(cf1, cf2);

            Assert.Equal("Adresse", result.Single().Diffs.Single().Path);
        }


        [Fact]
        public void GetDifferences_TelefonSehrLang_ReturnsExpectedResult()
        {
            var s = new ConnexiaFallFindDifferencesStrategy();

            int len = 20;
            var cf1 = new ConnexiaFall() { Adressnummer = -1, Telefon = new string('0', len) };
            var cf2 = new ConnexiaFall() { Adressnummer = -1, Telefon = new string('0', len + 1) };

            var result = s.GetDifferences(cf1, cf2);

            Assert.Empty(result);
        }

        [Fact]
        public void GetDifferences_TelefonSehrLangMitLeerzeichenAnMaxLengthStelle_ReturnsNoDifference()
        {
            var s = new ConnexiaFallFindDifferencesStrategy();

            int len = 19;
            var maxLengthZeichen = " "; //Leerzeichen an MaxPos (= Pos 20)
            var cf1 = new ConnexiaFall() { Adressnummer = -1, Telefon = new string('0', len) };
            var cf2 = new ConnexiaFall() { Adressnummer = -1, Telefon = new string('0', len) + maxLengthZeichen + "weiterer Text" };

            var result = s.GetDifferences(cf1, cf2);

            Assert.Empty(result);
        }

        [Fact]
        public void GetDifferences_VornamenNachnamenTrennungUnterschied_ReturnsNoDifference()
        {
            var s = new ConnexiaFallFindDifferencesStrategy();

            var cf1 = new ConnexiaFall() { Nachname = "NN X", Vorname = "VN" };
            var cf2 = new ConnexiaFall() { Nachname = "NN", Vorname = "X VN" };

            var result = s.GetDifferences(cf1, cf2);

            Assert.Empty(result);
        }

        [Fact]
        public void GetDifferences_NachnamenMitLeerzeichenOhneVornamen_ReturnsNoDifference()
        {
            var s = new ConnexiaFallFindDifferencesStrategy();

            var cf1 = new ConnexiaFall() { Nachname = ">> Beratungen" };
            var cf2 = new ConnexiaFall() { Nachname = ">>", Vorname = "Beratungen" };

            var result = s.GetDifferences(cf1, cf2);

            Assert.Empty(result);
        }

        [Fact]
        public void GetDifferences_VereinMitgliedsnummerStaatsbuergerschaft_ReturnsAsSeperateDiff()
        {
            //die Eigenschaften Verein, Mitgliedsnummer und Staatsbürgerschaften dürfen nicht mit anderen Änderungen zusammengefasst werden

            var s = new ConnexiaFallFindDifferencesStrategy();

            var cf1 = new ConnexiaFall() { Nachname = "NN X", Vorname = "VN" };
            var cf2 = new ConnexiaFall() { Nachname = "NN", Vorname = "X VN", Adresse = "Teststraße", Verein = "1", Mitgliedsnummer = 1, Staatsbuergerschaft = "Österreich" };

            var result = s.GetDifferences(cf1, cf2);

            var pathWithCNumberOfhanges = result.SelectMany(x => x.Diffs.Select(y => new { y.Path, Count = x.Diffs.Count() })).ToDictionary(x => x.Path, x => x.Count);

            Assert.Equal(1, pathWithCNumberOfhanges["Verein"]);
            Assert.Equal(1, pathWithCNumberOfhanges["Staatsbuergerschaft"]);
            Assert.Equal(1, pathWithCNumberOfhanges["Mitgliedsnummer"]);


        }
    }
}
