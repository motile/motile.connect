﻿using System;
using System.Linq;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaLeistungFindDifferencesStrategyTests
    {
        [Fact]
        public void GetDifferences_EqualDTO_ReturnsEmptyList()
        {
            var s = new ConnexiaLeistungFindDifferencesStrategy();

            var cf = new ConnexiaLeistung() { Pflegernummer = -1, Von = DateTime.Today, Bis = DateTime.Today };
            cf.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = -1, Anzahl = 1, Datum = DateTime.Today, Leistung = 2 });
            cf.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = -1, Anzahl = 2, Datum = DateTime.Today, Leistung = 2 });


            var result = s.GetDifferences(cf, cf);

            Assert.Empty(result);
        }

        [Fact]
        public void GetDifferences_EmptyChangeThis_ReturnsAddEntry()
        {
            var s = new ConnexiaLeistungFindDifferencesStrategy();

            var cf = new ConnexiaLeistung() { Pflegernummer = -1, Von = DateTime.Today, Bis = DateTime.Today };
            cf.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = -1, Anzahl = 1, Datum = DateTime.Today, Leistung = 2 });


            var cf2 = new ConnexiaLeistung() { Pflegernummer = -1, Von = DateTime.Today, Bis = DateTime.Today };

            var result = s.GetDifferences(cf, cf2);

            var add = result.Single().Diffs.Single() as AddEntryDifference;

            Assert.Equal(add.NewEntry, cf.Liste.Single());
        }

        [Fact]
        public void GetDifferences_EmptyMaster_ReturnsRemoveEntry()
        {
            var s = new ConnexiaLeistungFindDifferencesStrategy();

            var cf = new ConnexiaLeistung() { Pflegernummer = -1, Von = DateTime.Today, Bis = DateTime.Today };
            cf.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = -1, Anzahl = 1, Datum = DateTime.Today, Leistung = 2 });


            var cf2 = new ConnexiaLeistung() { Pflegernummer = -1, Von = DateTime.Today, Bis = DateTime.Today };

            var result = s.GetDifferences(cf2, cf);

            var remove = result.Single().Diffs.Single() as RemoveEntryDifference;

            Assert.Equal(remove.RemoveEntry, cf.Liste.Single());
        }

        [Fact]
        public void GetDifferences_AnzahlChanges_ReturnsChangeEntry()
        {
            var s = new ConnexiaLeistungFindDifferencesStrategy();

            var cf1 = new ConnexiaLeistung() { Pflegernummer = -1, Von = DateTime.Today, Bis = DateTime.Today };
            cf1.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = -1, Anzahl = 1.0, Datum = DateTime.Today, Leistung = 2 });


            var cf2 = new ConnexiaLeistung() { Pflegernummer = -1, Von = DateTime.Today, Bis = DateTime.Today };
            cf2.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = -1, Anzahl = 2.0, Datum = DateTime.Today, Leistung = 2 });

            var result = s.GetDifferences(cf2, cf1);

            var remove = result.SelectMany(x => x.Diffs).OfType<RemoveEntryDifference>().Single();

            var add = result.SelectMany(x => x.Diffs).OfType<AddEntryDifference>().Single();

            Assert.Equal(1.0, ((ConnexiaLeistung.Eintrag)remove.RemoveEntry).Anzahl);
            Assert.Equal(2.0, ((ConnexiaLeistung.Eintrag)add.NewEntry).Anzahl);
        }
    }
}
