﻿using Moq;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaFallCachedCategorizerTests
    {
        [Fact]
        public void ContainsAdressnummer_FallsFirstOccurance_ReturnsFalse()
        {
            var categorizer = new ConnexiaFallCachedCategorizer(null);

            Assert.False(categorizer.ContainsAdressnummer(1));
        }

        [Fact]
        public void ContainsAdressnummer_FallHasOccured_ReturnsTrue()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test" };

            var catMock = new Mock<IConnexiaFallCategorizer>();
            catMock.Setup(x => x.GetCategory(It.IsAny<ConnexiaFall>())).Returns(ConnexiaFallCategory.Klient);

            var categorizer = new ConnexiaFallCachedCategorizer(catMock.Object);

            var result = categorizer.GetCategory(fall);

            Assert.True(categorizer.ContainsAdressnummer(1));
            catMock.Verify(x => x.GetCategory(It.IsAny<ConnexiaFall>()), Times.Once);
        }

        [Fact]
        public void GetCategory_IsCalledMultipleTimes_CategorizerIsAskJustOnce()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test" };

            var catMock = new Mock<IConnexiaFallCategorizer>();
            catMock.Setup(x => x.GetCategory(It.IsAny<ConnexiaFall>())).Returns(ConnexiaFallCategory.Klient);

            var categorizer = new ConnexiaFallCachedCategorizer(catMock.Object);

            var result1 = categorizer.GetCategory(fall);
            var result2 = categorizer.GetCategory(fall);

            Assert.Equal(result1, result2);
            catMock.Verify(x => x.GetCategory(It.IsAny<ConnexiaFall>()), Times.Once);
        }
    }
}
