﻿using Moq;
using motile.Connect.CQ;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaFallAdressnummerCategorizerTests
    {
        [Fact]
        public void GetCategory_AdressnumerIsInCache_ReturnsResultFromCachesynchronously()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test" };

            var catMock = new Mock<IConnexiaFallCategorizer>();
            catMock.Setup(x => x.GetCategory(It.IsAny<ConnexiaFall>())).Returns(ConnexiaFallCategory.Klient);

            var categorizer = new ConnexiaFallCachedCategorizer(catMock.Object);

            var result1 = categorizer.GetCategory(fall);

            var categorizer2 = new ConnexiaFallAdressnummerCategorizer(null, categorizer);
            var result2 = categorizer2.GetCategoryAsync(fall.Adressnummer).Result;

            Assert.Equal(result1, result2);
            catMock.Verify(x => x.GetCategory(It.IsAny<ConnexiaFall>()), Times.Once);
        }


        [Fact]
        public async Task GetCategory_AdressnumerIsNotInCache_ReturnsResultFromFetchedEntity()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test" };

            var catMock = new Mock<IConnexiaFallCategorizer>();
            catMock.Setup(x => x.GetCategory(It.IsAny<ConnexiaFall>())).Returns(ConnexiaFallCategory.Klient);

            var categorizer = new ConnexiaFallCachedCategorizer(catMock.Object);


            var repMock = new Mock<IQueryEngine>();
            repMock.Setup(x => x.QueryAsync<ConnexiaFall>(It.IsAny<DtoQuery<ConnexiaFall>>())).Returns(Task.FromResult(new QueryResult<ConnexiaFall>(fall)));

            var categorizer2 = new ConnexiaFallAdressnummerCategorizer(repMock.Object, categorizer);

            var result1 = await categorizer2.GetCategoryAsync(1);
            var result2 = categorizer.GetCategory(fall);

            repMock.Verify(x => x.QueryAsync(It.Is<DtoQuery<ConnexiaFall>>(y => y.Key.Id == "1")), Times.Once);
            Assert.Equal(result1, result2);
        }
    }
}
