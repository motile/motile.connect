﻿using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaFallCategorizerTests
    {
        [Fact]
        public void GetCategory_ConnexiaFallWithNameBeratung_ReturnsNichtKlient()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Beratung" };

            var categorizer = new ConnexiaFallCategorizer();

            var result = categorizer.GetCategory(fall);

            Assert.Equal(ConnexiaFallCategory.NichtKlient, result);

        }

        [Fact]
        public void GetCategory_ConnexiaFallWithName_ReturnsKlient()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test" };

            var categorizer = new ConnexiaFallCategorizer();

            var result = categorizer.GetCategory(fall);

            Assert.Equal(ConnexiaFallCategory.Klient, result);

        }
    }
}
