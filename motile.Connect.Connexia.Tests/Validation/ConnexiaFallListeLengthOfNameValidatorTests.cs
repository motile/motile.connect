﻿using motile.Connect.Connexia.Validation;
using Xunit;

namespace motile.Connect.Connexia.Tests.Validation
{
    public class ConnexiaFallListeLengthOfNameValidatorTests
    {
        [Fact]
        public void Adressnummern_MitSehrLangemNamen_ReturnsError()
        {

            var v = new ConnexiaFallListeLengthOfNameValidator();

            var fl = new ConnexiaFallListe();
            fl.Adressnummern.Add(1, "DasIstEinSehrLangerName MitLangemVornamen");

            var cr = v.ValidateAsync(fl);

            Assert.False(cr.Result.IsValid);
            
        }
    }
}
