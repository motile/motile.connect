﻿using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia.Validation.Tests
{
    public class ConnexiaLeistungIstKumuliertValidatorTests
    {
        NameOfAddressProvider _nameProvider = null;

        [Fact]
        public void List_DoppelterEintrag_ReturnsError()
        {

            var v = new ConnexiaLeistungIstKumuliertValidator(_nameProvider);

            var cl = new ConnexiaLeistung();
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 2, Anzahl = 1, Adressnummer = 1 });
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 2, Anzahl = 2, Adressnummer = 1 });

            var cr = v.ValidateAsync(cl);

            Assert.False(cr.Result.IsValid);
        }
    }
}
