﻿using System;
using Xunit;

namespace motile.Connect.Connexia.Validation.Tests
{
    public class ConnexiaFallGeburtsdatumValidatorTests
    {
        [Fact]
        public void Geburtsdatum_IsEmpty_ReturnsOneError()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallGeburtsdatumValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test" };

            var r = v.Validate(fall);

            Assert.Equal(1, r.Errors.Count);
        }

        [Fact]
        public void Geburtsdatum_IsEmptyButNameIsBeratung_ReturnsValidResult()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallGeburtsdatumValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 2, Nachname = "Beratung", Vorname = "Test" };

            var r = v.Validate(fall);

            Assert.True(r.IsValid);
        }

        [Fact]
        public void Geburtsdatum_IsInFuture_ReturnsError()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallGeburtsdatumValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Geburtsdatum = DateTime.Today.AddDays(1) };

            var r = v.Validate(fall);

            Assert.False(r.IsValid);
        }

        [Theory]
        [InlineData(2011, 11, 11, "1234-11.11.11")]
        [InlineData(1911, 11, 11, "1234-11.11.11")]     //egal ob 19xx oder 20xx
        [InlineData(2011, 11, 11, "1234-11.11.19")]     //Versicherungsnummer invalid
        [InlineData(2011, 11, 11, "")]
        public void Geburtsdatum_ComparedWithVersicherungsnummer_ValidResults(int year, int month, int day, string vnummer)
        {
            var birthdate = new DateTime(year, month, day);
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Versicherungsnummer = vnummer, Geburtsdatum = birthdate };

            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallGeburtsdatumValidator(c);
            var r = v.Validate(fall);

            Assert.True(r.IsValid);
        }

        [Fact]
        public void Geburtsdatum_DiffersFromVersicherungsnummer_ReturnsError()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Geburtsdatum = new DateTime(2012, 11, 12), Versicherungsnummer = "1234-11.11.11" };

            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallGeburtsdatumValidator(c);
            var r = v.Validate(fall);

            Assert.False(r.IsValid);
        }

        [Fact]
        public void Geburtsdatum_IsLowerThan1900_ReturnsError()
        {
            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Geburtsdatum = new DateTime(1899, 11, 12) };

            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallGeburtsdatumValidator(c);
            var r = v.Validate(fall);

            Assert.False(r.IsValid);
        }
    }
}
