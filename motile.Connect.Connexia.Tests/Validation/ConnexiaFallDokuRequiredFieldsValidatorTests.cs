﻿using motile.Connect.Validation;
using System;
using System.Linq;
using Xunit;

namespace motile.Connect.Connexia.Validation.Tests
{
    public class ConnexiaFallDokuRequiredFieldsValidatorTests
    {
        [Fact]
        public void Doku_DatasetIsComplete_ReturnsValid()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallDokuRequiredFieldsValidator(c);

            var fall = new ConnexiaFall();

            fall.Dokus.AddRange(ConnexiaFallDokuRequiredFieldsValidator.RequiredFields.Select(x => new ConnexiaFall.Doku() { Gruppe = x, Datum = DateTime.Today, Wert = "01" }));

            var r = v.Validate(fall);

            Assert.True(r.IsValid);
        }

        [Fact]
        public void Doku_DatasetIsNotComplete_ReturnsError()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallDokuRequiredFieldsValidator(c);

            var fall = new ConnexiaFall();

            fall.Dokus.AddRange(ConnexiaFallDokuRequiredFieldsValidator.RequiredFields.Select(x => new ConnexiaFall.Doku() { Gruppe = x, Datum = DateTime.Today, Wert = "01" }));

            fall.Dokus.RemoveAt(0);

            var r = v.Validate(fall);

            Assert.False(r.IsValid);
        }        

        [Fact]
        public void Doku_DatasetContains01Before2015_ReturnsValid()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallDokuRequiredFieldsValidator(c);

            var fall = new ConnexiaFall();

            fall.Dokus.AddRange(ConnexiaFallDokuRequiredFieldsValidator.RequiredFields.Select(x => new ConnexiaFall.Doku() { Gruppe = x, Datum = DateTime.Today, Wert = "01" }));

            fall.Dokus.Add(new ConnexiaFall.Doku() { Gruppe = "01", Datum = new DateTime(2014, 12, 31), Wert = "01" });

            var r = v.Validate(fall);

            Assert.True(r.IsValid);
        }
    }
}
