﻿using motile.Connect.Validation;
using System;
using System.Linq;
using Xunit;

namespace motile.Connect.Connexia.Validation.Tests
{
    public class ConnexiaFallVersicherungValidatorTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void VNummmer_IsNullOrEmpty_ReturnsError(string vnummer)
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallVersicherungValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Versicherungsnummer = vnummer };

            var r = v.Validate(fall);

            Assert.False(r.IsValid);
            Assert.Equal(ValidationInfo.GetSeverity(r.Errors.Single()), ValidationInfo.Error);
        }

        [Fact]
        public void VNummer_IsEmptyButNameIsBeratung_ReturnsValidResult()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallVersicherungValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 2, Nachname = "Beratung", Vorname = "Test", Geburtsdatum = DateTime.Today };

            var r = v.Validate(fall);

            Assert.True(r.IsValid);
        }

        [Theory]
        [InlineData("12341111111")]
        [InlineData("12341111")]
        [InlineData("1234.11.11.11")]
        [InlineData("1233-11.11.11")]
        public void VNummmer_IsNotWellformed_ReturnsError(string vnummer)
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallVersicherungValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Versicherungsnummer = vnummer, Versicherung = "01" };

            var r = v.Validate(fall);

            Assert.False(r.IsValid);
            Assert.Equal(ValidationInfo.GetSeverity(r.Errors.Single()), ValidationInfo.Error);
        }


        [Theory]
        [InlineData("1234111111")]
        [InlineData("1234-11.11.11")]
        public void VNummmer_IsValid(string vnummer)
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallVersicherungValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Versicherungsnummer = vnummer, Versicherung = "01" };

            var r = v.Validate(fall);

            Assert.True(r.IsValid);
        }

        [Fact]
        public void Versicherung_IsEmpty_ReturnsError()
        {
            var c = new ConnexiaFallCategorizer();
            var v = new ConnexiaFallVersicherungValidator(c);

            var fall = new ConnexiaFall() { Adressnummer = 1, Nachname = "Test", Vorname = "Test", Versicherungsnummer = "1234-11.11.11" };

            var r = v.Validate(fall);

            Assert.False(r.IsValid);
        }
    }
}
