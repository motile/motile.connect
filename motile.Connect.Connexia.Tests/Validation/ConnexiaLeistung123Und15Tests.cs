﻿using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia.Validation.Tests
{
    public class ConnexiaLeistung123Und15Tests
    {
        NameOfAddressProvider _nameProvider = null;

        [Fact]
        public void Kein15er()
        {
            var v = new ConnexiaLeistung123Und15(_nameProvider);

            var cl = new ConnexiaLeistung();
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 2, Anzahl = 1, Adressnummer = 1 });

            var cr = v.ValidateAsync(cl);

            Assert.False(cr.Result.IsValid);
        }

        [Fact]
        public void Kein123er()
        {
            var v = new ConnexiaLeistung123Und15(_nameProvider);

            var cl = new ConnexiaLeistung();
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 4, Anzahl = 1, Adressnummer = 1 });

            var cr = v.ValidateAsync(cl);

            Assert.False(cr.Result.IsValid);
        }

        [Fact]
        public void Richtig()
        {
            var v = new ConnexiaLeistung123Und15(_nameProvider);

            var cl = new ConnexiaLeistung();
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 2, Anzahl = 1, Adressnummer = 1 });
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 15, Anzahl = 1, Adressnummer = 1 });

            var cr = v.ValidateAsync(cl);

            Assert.True(cr.Result.IsValid);
        }
    }
}
