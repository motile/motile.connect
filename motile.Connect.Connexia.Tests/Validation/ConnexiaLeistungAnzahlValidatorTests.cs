﻿using Moq;
using motile.Connect.Connexia.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia.Tests.Validation
{
    public class ConnexiaLeistungAnzahlValidatorTests
    {
        NameOfAddressProvider _nameProvider = null;

        [Fact]
        public void Anzahl_ÜberFünf_ReturnsWarning()
        {
            var v = new ConnexiaLeistungAnzahlValidator(_nameProvider);

            var cl = new ConnexiaLeistung() { Pflegernummer = 1 };
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 2, Anzahl = 6, Adressnummer = 1 });

            var cr = v.ValidateAsync(cl);

            Assert.False(cr.Result.IsValid);
        }
    }
}
