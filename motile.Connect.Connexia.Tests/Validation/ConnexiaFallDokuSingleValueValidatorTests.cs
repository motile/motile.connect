﻿using System;
using Xunit;

namespace motile.Connect.Connexia.Validation.Tests
{
    public class ConnexiaFallDokuSingleValueValidatorTests
    {
        [Fact]
        public void Doku_DatasetConvainsMultiple_ReturnsInValid()
        {
            var v = new ConnexiaFallDokuSingleValueValidator();

            var fall = new ConnexiaFall();

            fall.Dokus.Add(new ConnexiaFall.Doku() { Datum = DateTime.Today, Gruppe = "21", Detail = "01" });
            fall.Dokus.Add(new ConnexiaFall.Doku() { Datum = DateTime.Today, Gruppe = "21", Detail = "02" });

            var r = v.Validate(fall);

            Assert.False(r.IsValid);
        }

      
    }
}
