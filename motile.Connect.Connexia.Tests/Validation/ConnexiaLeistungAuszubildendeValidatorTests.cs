﻿using Moq;
using motile.Connect.Connexia.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia.Tests.Validation
{
    public class ConnexiaLeistungAuszubildendeValidatorTests
    {
        [Fact]
        public void Auszubildend_6bis10_ReturnsNotIsValid()
        {
            var m = new Mock<IPflegerIsinRoleProvider>(MockBehavior.Strict);
            m.Setup(x => x.IsInRole(It.Is<int>(i => i == 1), It.Is<string>(s => s == "Auszubildend"))).Returns(Task.FromResult(true));

            var v = new ConnexiaLeistungAuszubildendeValidator(m.Object, null);

            var cl = new ConnexiaLeistung() { Pflegernummer = 1 };
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 2, Anzahl = 1, Adressnummer = 1 });
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 6, Anzahl = 1, Adressnummer = 1 });
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 15, Anzahl = 1, Adressnummer = 1 });

            var cr = v.ValidateAsync(cl);

            Assert.False(cr.Result.IsValid);
        }

        [Fact]
        public void Auszubildend_Keine6bis10_ReturnsIsValid()
        {
            var m = new Mock<IPflegerIsinRoleProvider>(MockBehavior.Strict);
            m.Setup(x => x.IsInRole(It.Is<int>(i => i == 1), It.Is<string>(s => s == "Auszubildend"))).Returns(Task.FromResult(true));

            var v = new ConnexiaLeistungAuszubildendeValidator(m.Object, null);

            var cl = new ConnexiaLeistung() { Pflegernummer = 1 };
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 2, Anzahl = 1, Adressnummer = 1 });
            cl.Liste.Add(new ConnexiaLeistung.Eintrag() { Datum = DateTime.Today, Leistung = 15, Anzahl = 1, Adressnummer = 1 });

            var cr = v.ValidateAsync(cl);

            Assert.True(cr.Result.IsValid);
        }
    }
}
