﻿using System;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaFallListeTests
    {
        [Fact]
        public void GetIDFaktoren_SomeID_AssignsOutParameters()
        {

            ConnexiaFallListe.GetIDFaktoren("20010201_20011110", out DateTime von, out DateTime bis, out bool nurAktive);

            Assert.Equal("01.02.2001", von.ToString("dd.MM.yyyy"));
            Assert.Equal("10.11.2001", bis.ToString("dd.MM.yyyy"));
            Assert.False(nurAktive);
        }

        [Fact]
        public void GetIDFaktoren_nurAktive_AssignsOutParameters()
        {

            ConnexiaFallListe.GetIDFaktoren("20010201_20011110+nurAktive", out DateTime von, out DateTime bis, out bool nurAktive);

            Assert.Equal("01.02.2001", von.ToString("dd.MM.yyyy"));
            Assert.Equal("10.11.2001", bis.ToString("dd.MM.yyyy"));
            Assert.True(nurAktive);
        }

        [Fact]
        public void DtoKey_Id()
        {
            var von = new DateTime(2014, 09, 1);
            var bis = von.AddMonths(1).AddDays(-1);

            var dto = new ConnexiaFallListe() { Von = von, Bis = bis };
            for (var i = 1; i < 5; i++)
            {
                dto.Adressnummern.Add(i, i.ToString());
            }

            var id = dto.DtoKey.Id;

            Assert.Equal("20140901_20140930", id);
        }

        [Fact]
        public void DtoKey_id_nurAktive()
        {
            var von = new DateTime(2014, 09, 1);
            var bis = von.AddMonths(1).AddDays(-1);

            var dto = new ConnexiaFallListe() { Von = von, Bis = bis, NurAktive = true };
            for (var i = 1; i < 5; i++)
            {
                dto.Adressnummern.Add(i, i.ToString());
            }

            var id = dto.DtoKey.Id;

            Assert.Equal("20140901_20140930+nurAktive", id);
        }
    }
}
