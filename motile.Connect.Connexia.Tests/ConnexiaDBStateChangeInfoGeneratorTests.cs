﻿using motile.Connect.CQ.OleDb;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace motile.Connect.Connexia.Tests
{
    public class ConnexiaDBStateChangeInfoGeneratorTests
    {
        [Fact]
        public async Task HasNoDokuNullValues_IsFalse_ReturnsSetWert()
        {
            var master = new ConnexiaDBState() { HasNoDokuNullValues = true };
            var changeThis = new ConnexiaDBState() { HasNoDokuNullValues = false };

            var s = new ConnexiaDBStateFindDifferencesStrategy();
            var diffs = s.GetDifferences(master, changeThis);

            var gen = new ConnexiaDBStateChangeInfoGenerator();
            var changeInfos = await gen.Process(diffs);
            var cmd = (OleDbBaseCommand)changeInfos.Single().Commands.Single();

            Assert.Contains("wert=''", cmd.GetCommand().CommandText.ToLower().Replace(" ", ""));
        }
    }
}
