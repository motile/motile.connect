@echo off


if '%1'=='/?' goto help
if '%1'=='-help' goto help
if '%1'=='--help' goto help
if '%1'=='-h' goto help


set task=default
set version=1.0.0.0

if not '%1' == '' set task=%1
if not '%2' == '' set version=%2


IF EXIST %~dp0\packages\psake.4.4.2 goto run
echo restoring nuget packages
%~dp0\.nuget\nuget.exe restore

:run
echo Executing psake script Build.ps1 with task "%task%" and version "%version%"

powershell.exe -NoProfile -ExecutionPolicy unrestricted -Command "& { Import-Module '%~dp0\packages\psake.4.4.2\tools\psake.psm1'; Invoke-psake '%~dp0\psake.ps1' -task %task% -parameters @{version='%version%'} ; if ($lastexitcode) {write-host "ERROR: $lastexitcode" -fore RED; exit $lastexitcode} }"
exit /B %errorlevel%


:help
echo psake.cmd [Task [Version]]
echo -------------------
echo psake.cmd Build
echo psake.cmd Test
echo -------------------
echo psake.cmd DeployNuGet 0.0.99  
echo -------------------
echo Hinweis:
echo     vor dem ersten mal DeployNuGet muss der apikey wie folgt in die %%AppData%%\NuGet\NuGet.config 
echo     geschrieben werden:
echo     .nuget\nuget.exe setapikey ___apikey___ -source https://www.myget.org/F/motile/api/v2/package
echo -------------------

