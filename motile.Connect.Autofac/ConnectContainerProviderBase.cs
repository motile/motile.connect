﻿using Autofac;
using motile.Connect.CQ;
using motile.Connect.Validation;
using System;

namespace motile.Connect.Autofac
{
    public class ConnectContainerProviderBase
    {
        #region - fields
        private IDialogManager _dlgMgr;
        private Action<ContainerBuilder> _configBuilder;
        #endregion

        #region - ctr
        public ConnectContainerProviderBase(IDialogManager dlgMgr)
        {
            _dlgMgr = dlgMgr;
        }
        #endregion

        public void SetConfigAction(Action<ContainerBuilder> configBuilder)
        {
            _configBuilder = configBuilder;
        }

        public IContainer GetContainer()
        {
            var builder = new ContainerBuilder();

            if (_configBuilder != null) _configBuilder.Invoke(builder);

            ConfigBuilder(builder);

            return builder.Build();
        }

        protected virtual void ConfigBuilder(ContainerBuilder builder)
        {
            builder.RegisterType<CommandExecutor>().AsSelf();
            builder.RegisterGeneric(typeof(Connector<>)).AsSelf();

            if (_dlgMgr != null)
                builder.RegisterInstance(_dlgMgr);

            var settingsProvider = new LocalSettingsProvider<LocalConnectSettings>(LocalConnectSettings.ID);

            var valSettings = new ValidationSettings(settingsProvider.GetSettings().IgnoreValidationInfos);
            builder.RegisterInstance<ValidationSettings>(valSettings);

            builder.RegisterType<MemoryCacheProvider>().InstancePerMatchingLifetimeScope(CompositionHelper.LifetimeTag).As<ICachingProvider>();
            builder.Register(c => new QueryEngine(c.Resolve<IComponentContext>())).As<IQueryEngine>();
        }
    }
}
