﻿using Autofac;
using motile.Connect.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Autofac
{
    public class ConnexiaValidator : IDisposable
    {
        #region - fields
        private ILifetimeScope _scope;
        #endregion

        #region - ctr
        public ConnexiaValidator(ILifetimeScope scope)
        {
            _scope = scope;
        }
        #endregion

        #region - methods
        public Task<IEnumerable<ValidationInfo>> ValidateAsync(IDTO dto)
        {
            return this.ValidateAsync(new[] { dto });
        }

        public Task<IEnumerable<ValidationInfo>> ValidateAsync(IEnumerable dtos)
        {
            Task<IEnumerable<ValidationInfo>> result = null;

            if (dtos != null && dtos.Cast<object>().Any())
            {
                Contract.Assert(dtos.Cast<object>().Select(x => x.GetType()).Distinct().Count() == 1); // nur Elemente eines Type pro Durchgang!

                var mInfo = this.GetType().GetMethods().Where(x => x.IsGenericMethod && x.Name == "ValidateAsync")
                    .Single()
                    .MakeGenericMethod(dtos.Cast<object>().First().GetType());


                result = mInfo.Invoke(this, new object[] { dtos }) as Task<IEnumerable<ValidationInfo>>;
            }
            return result ?? Task.FromResult<IEnumerable<ValidationInfo>>(new ValidationInfo[] { });
        }

        public async Task<IEnumerable<ValidationInfo>> ValidateAsync<T>(IEnumerable dtos)
            where T : IDTO
        {
            var tasks = GetValidationTasks<T>(dtos);

            await Task.WhenAll(tasks.Select(x => x.Item2));

            return tasks.SelectMany(x =>
                x.Item2.IsFaulted ? new[] { new ValidationInfo() { DTOID = x.Item1.DtoKey.Id, Message = x.Item2.Exception.Message, Severity = ValidationInfo.Error } } :
                x.Item2.IsCanceled ? new[] { new ValidationInfo() { DTOID = x.Item1.DtoKey.Id, Message = "Cancelled", Severity = ValidationInfo.Error } } :
                x.Item2.Result);
        }

        private List<Tuple<T, Task<IEnumerable<ValidationInfo>>>> GetValidationTasks<T>(IEnumerable dtos)
            where T : IDTO
        {
            var tasks = new List<Tuple<T, Task<IEnumerable<ValidationInfo>>>>();

            ValidatorAggregator<T> validator;
            if (_scope.TryResolve<ValidatorAggregator<T>>(out validator))
            {
                foreach (var dto in dtos.OfType<T>())
                {
                    tasks.Add(new Tuple<T, Task<IEnumerable<ValidationInfo>>>(dto, validator.ValidateAsync(dto)));
                }
            }

            return tasks;
        }
        #endregion  

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_scope != null)
                    {
                        _scope.Dispose();
                        _scope = null;
                    }
                }

                disposedValue = true;
            }
        }


        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
