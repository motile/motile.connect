﻿using Autofac;
using motile.Connect.CQ;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Autofac
{
    public class QueryEngine : IQueryEngine
    {
        private const int _exp = 2 * 60 * 1000;
        
        private readonly IComponentContext _context;

        public QueryEngine(IComponentContext context)
        {
            _context = context;            
        }

        public async Task<QueryResult<T>> QueryAsync<T>(Query<T> query)
        {

            var cacheKey = query.AllPropsToUniqueString();

            object cachedResult;

            var cachingProvider = _context.Resolve<ICachingProvider>();

            if (cachingProvider.GetItem<object>(cacheKey, out cachedResult ))
            {
                return (QueryResult<T>)cachedResult;
            }
            
            
            var queryType = query.GetType();
            if (queryType.IsGenericType && queryType.GetGenericTypeDefinition() == typeof(DtoQuery<>))
            {

                var m = this.GetType().GetMethods(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic)
                    .Where(x => x.IsGenericMethod)
                    .Where(x => x.Name == "QueryDtoAsync").First();

                var result = await ( m.MakeGenericMethod(typeof(T)).Invoke(this, new[] { query }) as Task<QueryResult<T>>);

                cachingProvider.AddItem(cacheKey, result, _exp);

                return result;
            }
            

            throw new NotImplementedException();
        }


        private Task<QueryResult<T>> QueryDtoAsync<T>(DtoQuery<T> query)
            where T : IDTO
        {
            var part = _context.Resolve<IQueryEnginePart<T, DtoQuery<T>>>();
            return part.QueryAsync(query);
        }
    }
}
