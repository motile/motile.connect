﻿using Autofac;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Autofac
{
    public class ConnexiaConnector
    {
        #region - fields
        private ILifetimeScope _scope;
        #endregion

        #region - ctr
        public ConnexiaConnector(ILifetimeScope scope)
        {
            _scope = scope;
        }
        #endregion

        #region - public methods
        public Task<IEnumerable<ChangeInfo>> ResolveChangeInfos(IDTO dto)
        {
            return this.ResolveChangeInfos(new[] { dto });
        }

        public Task<IEnumerable<ChangeInfo>> ResolveChangeInfos(IEnumerable dtos)
        {
            Task<IEnumerable<ChangeInfo>> result = null;

            if (dtos != null && dtos.Cast<object>().Any())
            {
                Contract.Assert(dtos.Cast<object>().Select(x => x.GetType()).Distinct().Count() == 1); // nur Elemente eines Type pro Durchgang!

                var mInfo = this.GetType().GetMethods().Where(x => x.IsGenericMethod && x.Name == "ResolveChangeInfos")
                    .Single()
                    .MakeGenericMethod(dtos.Cast<object>().First().GetType());


                result = mInfo.Invoke(this, new object[] { dtos }) as Task<IEnumerable<ChangeInfo>>;
            }
            return result ?? Task.FromResult<IEnumerable<ChangeInfo>>(new ChangeInfo[] { });
        }

        public async Task<IEnumerable<ChangeInfo>> ResolveChangeInfos<T>(IEnumerable dtos)
            where T : IDTO
        {
            var result = new List<ChangeInfo>();
            
            var connector = _scope.Resolve<Connector<T>>();

            foreach (var dto in dtos.OfType<T>())
            {
                result.AddRange(await connector.ResolveChangeInfosAsync(dto));
            }

            return result;
        }
        #endregion
    }
}
