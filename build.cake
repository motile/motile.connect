#tool "nuget:?package=xunit.runner.console"


var target = Argument("target", "Default");
var version = Argument("fileversion", "0.0.1");
var buildDir = MakeAbsolute(Directory("./build/bin")).FullPath;
var nugetDir = MakeAbsolute(Directory("./build")).FullPath;
var configuration = "Release";


Task("Default")
  .IsDependentOn("CreateAssemblyInfo")
  .IsDependentOn("Build")
  .IsDependentOn("Test")
  .IsDependentOn("Pack");


Task("CreateAssemblyInfo")
.Does(() => {
  var file = "./CommonAssemblyInfo.cs";
  
  CreateAssemblyInfo(file, new AssemblyInfoSettings {
    Product = "motile.Connect",
    Company = "motile users Software GmbH",
    Version = version,
    FileVersion = version    
  });
});

Task("Build")  
  .Does(() =>{    
    DotNetBuild("./motile.connect.sln", settings =>
    settings.SetConfiguration(configuration)        
        .WithTarget("Build")
        .WithProperty("OutputPath",buildDir));

  });

Task("Test")  
  .Does(() =>{
    var pattern = string.Format("{0}/**/*.Tests.dll", buildDir);
		Information(pattern);
		
		foreach(var file in GetFiles(pattern)) 
		{
			Information("{0}", file);
			XUnit2(file.ToString());
		}   
  });

Task("Clean")
  .Does(()=>{
    var directoriesToClean = GetDirectories("./**/bin/Debug").Union(GetDirectories("./**/bin/Release"));
    CleanDirectories(directoriesToClean);
  });

Task("Pack")  
  .Does(() =>{
    Information("version: {0}", version);
     var nuGetPackSettings   = new NuGetPackSettings {
                                     Id                      = "motile.connect.wpf",
                                     Description             = "motile.connect.wpf",
                                     Version                 = version,
                                     Title                   = "motile.connect.wpf",
                                     Authors                 = new[] {"dgorbach"},
                                     Owners                  = new[] {"motile users Software GmbH"},
                                     Copyright               = "motile users Software GmbH 2017",  

                                     RequireLicenseAcceptance= false,
                                     Symbols                 = false,  
                                     NoPackageAnalysis       = true,                                   
				                             Dependencies            = new [] {
                                                                new NuSpecDependency {Id="Autofac", Version="4.8"},
                                                                new NuSpecDependency {Id="Caliburn.Micro", Version="3.2"},                                                                
                                                                new NuSpecDependency {Id="Dapper", Version="1.50"},
                                                                new NuSpecDependency {Id="FluentValidation", Version="8.0"},
                                                                new NuSpecDependency {Id="Newtonsoft.Json", Version="10.0"},
																	                            },
                                     Files                   = new [] {
                                                                  new NuSpecContent {Source = "motile.connect.dll", Target = "lib/net45"},
                                                                  new NuSpecContent {Source = "motile.connect.autofac.dll", Target = "lib/net45"},
                                                                  new NuSpecContent {Source = "motile.connect.connexia.dll", Target = "lib/net45"},
                                                                  new NuSpecContent {Source = "motile.connect.connexia.wpf.dll", Target = "lib/net45"},
                                                              },
                                     BasePath                = buildDir,
                                     OutputDirectory         = nugetDir
                                 };

     NuGetPack(nuGetPackSettings);
  });


RunTarget(target);