﻿using Autofac;
using motile.Connect.Autofac;
using motile.Connect.Connexia;
using motile.Connect.CQ;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace motile.Connect.Demo
{

    public class Demonstrator : IDisposable
    {

        #region - private fields
        private static int _c;
        private static string _lastTime;
        private Action<string> _logger;
        private string _path;
        private readonly IContainer _container;
        #endregion

        #region - ctr
        public Demonstrator(string path, string connectionString, IDialogManager dlgManager, Action<string> logger)
        {
            this._logger = logger ?? new Action<string>((x) => System.Diagnostics.Debug.Print(x)); ;

            this._path = path;

            var cs = new ConnexiaSettings() { ConnectionString = connectionString };
            var cp = new ConnexiaContainerProvider(cs, dlgManager);

            _container = cp.GetContainer();
        }
        #endregion

        #region - public methods
        public async Task Init()
        {
            var fileDB = Path.Combine(_path, "vgkvdat.mdb");

            if (!File.Exists(fileDB))
            {
                _logger(string.Format("Erzeuge Demodaten: {0}", fileDB));

                File.Copy("vgkvdat.mdb", fileDB);

                var gen = new DemoDataGenerator();
                await gen.Generate(_path, 2, 2);
            }

            // der Status der DB soll gleich überprüft werden.
            var DBInfo = new ConnexiaDBState()
            {
                HasTableLeistungAColumnTransdok = true,
                HasNoDokuNullValues = true,
                HasTransdokLogTable = true
            };

            using (var scope = _container.BeginLifetimeScope(CompositionHelper.LifetimeTag))
            {
                var DBInfoTask = ProcessDTO(DBInfo, scope);

                await DBInfoTask;
            }
        }

        public async Task Check(string pathInbox)
        {
            //jetzt die Files abarbeiten
            if (!Directory.Exists(pathInbox))
            {
                Directory.CreateDirectory(pathInbox);
            }

            var task = ProcessFiles(pathInbox);

            await task;


            if (task.IsFaulted)
            {
                _logger(string.Format("Fehler: {0}", task.Exception));
            }
        }
        #endregion


        #region - private methods
        private async Task<IDTO> GetDTO(FileInfo file)
        {
            IDTO dto = null;

            using (TextReader f = File.OpenText(file.FullName))
            {
                var json = await f.ReadToEndAsync();
                Type type = null;
                var typeName = DTOHelper.GetTypeNameFromFileName(file.Name);
                if (!string.IsNullOrEmpty(typeName))
                {
                    if (string.Equals(typeof(ConnexiaFall).Name, typeName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        type = typeof(ConnexiaFall);
                    }
                    else if (string.Equals(typeof(ConnexiaFallListe).Name, typeName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        type = typeof(ConnexiaFallListe);
                    }
                    else if (string.Equals(typeof(ConnexiaPfleger).Name, typeName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        type = typeof(ConnexiaPfleger);
                    }
                    else if (string.Equals(typeof(ConnexiaLeistung).Name, typeName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        type = typeof(ConnexiaLeistung);
                    }
                }

                if (type != null)
                {
                    dto = JsonConvert.DeserializeObject(json, type) as IDTO;
                }
            }

            return dto;
        }

        private async Task<bool> ProcessDTO(IDTO dto, ILifetimeScope scope)
        {
            bool result = false;

            var v = new ConnexiaValidator(scope);
            var dp = new DTODescriptionProvider(scope);

            var rr = (await v.ValidateAsync(new[] { dto }));
            foreach (var r in rr)
            {
                _logger(string.Format("{0}:{1}, {2}", r.DTOID, r.Severity, r.Message));
            }

            if (!rr.Any())
            {
                var connector = new ConnexiaConnector(scope);

                var ci = await connector.ResolveChangeInfos(dto);

                foreach (var ci1 in ci.OrderBy(x => x.DiffsPack.DtoKey.TypeKey).GroupBy(x => x.DiffsPack.DtoKey.TypeKey))
                {
                    foreach (var ch2 in ci1.GroupBy(x => x.DiffsPack.DtoKey.Id))
                    {
                        _logger(await dp.WithIDAsync(ch2.First().DiffsPack.DtoKey));

                        foreach (var cmd in ch2.SelectMany(x => x.Commands))
                        {

                            _logger(cmd.Info);
                        }
                    }
                }

                var e = scope.Resolve<CommandExecutor>();
                result = await e.Execute(ci);
            }

            return result;
        }

        private async Task<bool> ProcessFiles(string path)
        {
            var result = true;
            var files = new DirectoryInfo(path).GetFiles().OrderBy(x => x.Name.Contains("ConnexiaFallListe") ? 0 : 1).ThenBy(f => f.LastWriteTime).ToList();

            using (var scope = _container.BeginLifetimeScope(CompositionHelper.LifetimeTag))
            {

                foreach (var file in files)
                {
                    bool ok = false;

                    _logger(string.Format(@"file changed: {0}", file.Name));

                    IDTO dto = await GetDTO(file);

                    if (dto != null)
                    {
                        try
                        {
                            ok = await ProcessDTO(dto, scope);
                        }
                        catch (Exception e)
                        {
                            _logger(e.Message);
                        }
                    }

                    if (!ok)
                    {
                        result = false;
                        break;
                    }

                    _logger("ok");

                    var pathMove = Path.Combine(_path, "log");
                    if (!Directory.Exists(pathMove))
                    {
                        _logger(string.Format("create {0}", pathMove));
                        Directory.CreateDirectory(pathMove);
                    }

                    var time = string.Format("{0:yyyyMMddHHss}", DateTime.Now);
                    if (time != _lastTime)
                    {
                        _lastTime = time;
                        _c = 0;
                    }

                    var moveto = string.Format(@"{0}\{1}{2}_{3}_{4}.txt", pathMove, time, _c++, dto.GetType().Name, dto.DtoKey.Id);

                    File.Move(file.FullName, moveto);
                }
            }
            return result;
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _container.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
