﻿using motile.Connect.Connexia;
using motile.Connect.Connexia.Validation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace motile.Connect.Demo
{
    public class DemoDataGenerator
    {
        #region - fields
        private Random _rand = new Random();
        private string[] _addresses;
        private string[] _names;
        private string[] _surnames;
        #endregion

        #region - methods
        public async Task Generate(string path, int adressen, int pfleger)
        {
            var listePfleger = await GenerateConnexiaPflegerList(pfleger);
            await SaveToPath(path, listePfleger);

            var listeFälle = await GenerateConnexiaFallList(adressen);

            var cfl = new ConnexiaFallListe() { Von = new DateTime(DateTime.Today.Year, 1, 1), Bis = new DateTime(DateTime.Today.Year, 12, 31) };
            foreach (var fall in listeFälle)
            {
                cfl.Adressnummern.Add(fall.Adressnummer, string.Format("{0} {1}", fall.Nachname, fall.Vorname));
            }

            await SaveToPath(path, new[] { cfl });

            await SaveToPath(path, listeFälle);

            var listeLeistungen = GenerateConnexiaLeistung(listePfleger, listeFälle, 5);
            await SaveToPath(path, listeLeistungen);
        }
        #endregion

        #region - private methdos
        private async Task Init()
        {
            var t1 = Task.Run(() => _addresses = File.ReadAllLines("gemplzstr_8.csv", Encoding.Default));
            var t2 = Task.Run(() => _names = File.ReadAllLines("vornamen.txt"));
            var t3 = Task.Run(() => _surnames = File.ReadAllLines("nachnamen.txt"));

            await Task.WhenAll(t1, t2, t3);
        }

        private List<ConnexiaLeistung> GenerateConnexiaLeistung(List<ConnexiaPfleger> pfleger, List<ConnexiaFall> adressen, int count)
        {
            var result = new List<ConnexiaLeistung>();
            var year = DateTime.Today.Year;

            // zum Betreuungsbeginn muss ein Eintrag vorhanden sein!
            var beginn = new DateTime(year, 1, 1);

            foreach (var entry in adressen)
            {
                var p = pfleger[_rand.Next(pfleger.Count())].Pflegernummer;
                var l = CreateNewLeistung(p, entry.Adressnummer, year, 1, 10 + _rand.Next(20));
                if (!l.Liste.Where(x => x.Datum == beginn).Any())
                {
                    l.Liste.Insert(0, new ConnexiaLeistung.Eintrag() { Datum = beginn, Adressnummer = entry.Adressnummer, Anzahl = 1, Leistung = 2 });
                    l.Liste.Insert(0, new ConnexiaLeistung.Eintrag() { Datum = beginn, Adressnummer = entry.Adressnummer, Anzahl = 1, Leistung = 15 });
                }
                result.Add(l);
            }

            for (var i = 0; i < count; i++)
            {
                var p = pfleger[_rand.Next(pfleger.Count())].Pflegernummer;
                var a = adressen[_rand.Next(adressen.Count())].Adressnummer;

                var l = CreateNewLeistung(p, a, year, 2 + _rand.Next(11), 10 + _rand.Next(20));
                if (!result.Where(x => x.DtoKey.Id == l.DtoKey.Id).Any())
                {
                    result.Add(l);
                }
            }


            return result.OrderBy(x => x.Von).ThenBy(x => x.Pflegernummer).ToList();
        }

        private async Task<List<ConnexiaFall>> GenerateConnexiaFallList(int count)
        {
            var result = new List<ConnexiaFall>();
            await Init();

            for (var i = 0; i < count; i++)
            {
                result.Add(CreateNewAdress(-1 - i));
            }

            return result;
        }

        private async Task<List<ConnexiaPfleger>> GenerateConnexiaPflegerList(int count)
        {
            var result = new List<ConnexiaPfleger>();
            await Init();

            for (var i = 0; i < count; i++)
            {
                result.Add(CreateNewPfleger(1 + i));
            }

            return result;
        }

        private ConnexiaLeistung CreateNewLeistung(int pfleger, int adressnummer, int year, int month, int count)
        {
            var result = new ConnexiaLeistung()
            {
                Pflegernummer = pfleger,
                Von = new DateTime(year, month, 1),
                Bis = new DateTime(year, month, 1).AddMonths(1).AddDays(-1)
            };

            DateTime date = result.Von;

            for (var i = 0; i < count; i++)
            {
                date = date.AddDays(1 + _rand.Next(10));

                if (date > result.Bis.Date)
                    date = result.Bis.Date;

                var leistung = 4 + _rand.Next(11);
                var anzahl = 1 + _rand.Next(4);

                if (!result.Liste.Where(x => x.Adressnummer == adressnummer && x.Datum == date && x.Leistung == leistung).Any())
                {
                    result.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = adressnummer, Datum = date, Leistung = 2, Anzahl = 1 });
                    result.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = adressnummer, Datum = date, Leistung = leistung, Anzahl = anzahl });
                    result.Liste.Add(new ConnexiaLeistung.Eintrag() { Adressnummer = adressnummer, Datum = date, Leistung = 15, Anzahl = 1 });
                }

                if (date.Date == result.Bis.Date)
                    break;
            }

            result.Liste = result.Liste.OrderBy(x => x.Datum).ThenBy(x => x.Adressnummer).ThenBy(x => x.Leistung).ToList();

            return result;
        }

        private ConnexiaFall CreateNewAdress(int addressnummer)
        {
            var result = new ConnexiaFall
            {
                Adressnummer = addressnummer,
                Nachname = _surnames[_rand.Next(_surnames.Length - 1)],
                Vorname = _names[_rand.Next(_names.Length - 1)]
            };

            // die Anschrift
            {
                var line = 3 + _rand.Next(_addresses.Length - 5);  // die ersten drei und die letzte Zeile sind ungültig
                var address = _addresses[line].Split(';');

                result.Land = "Österreich";
                result.PLZ = address[6];
                result.Ort = address[3];
                result.Adresse = string.Format("{0} {1}", address[5], 1 + _rand.Next(30));

            }

            result.Geschlecht = _rand.Next(2) == 1 ? "m" : "w";
            result.Staatsbuergerschaft = "AT";
            result.Telefon = _rand.Next(999999999).ToString();
            result.Geburtsdatum = new DateTime(1920, 01, 01).AddDays(_rand.Next(20000));

            result.Versicherung = "19";
            
            result.Versicherungsnummer = CreateRandomVersicherungsnummer(result.Geburtsdatum.Value);

            result.Mitgliedsnummer = _rand.Next(10000);
            result.Verein = "123";


            foreach (var gruppe in ConnexiaFallDokuRequiredFieldsValidator.RequiredFields)
            {
                result.Dokus.Add(new ConnexiaFall.Doku()
                {
                    Gruppe = gruppe,
                    Detail = _rand.Next(5).ToString("D2"),
                    Datum = new DateTime(DateTime.Today.Year, 1, 1)
                });
            }

            return result;
        }

        private string CreateRandomVersicherungsnummer(DateTime date)
        {
            int nr;
            int cd;

            while (true)
            {
                nr = _rand.Next(899) + 100;

                cd = VNummerHelper.GetCheckDigit(nr.ToString("000"), date.ToString("ddMMyy"));

                if (cd >= 0 && cd <= 9)
                    break;
            }

            return VNummerHelper.Format(string.Format("{0}{1}-{2:dd.MM.yy}", nr, cd, date));
        }

        private ConnexiaPfleger CreateNewPfleger(int pflegerNummer)
        {
            var result = new ConnexiaPfleger
            {
                Pflegernummer = pflegerNummer,
                SubjectID = Guid.Parse(string.Format("00000000-0000-0000-0000-{0:D12}", pflegerNummer)),
                Nachname = _surnames[_rand.Next(_surnames.Length - 1)],
                Vorname = _names[_rand.Next(_names.Length - 1)]
            };

            return result;
        }

        private async Task SaveToPath<T>(string path, IEnumerable<T> dtos)
            where T : IDTO
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var tasks = new List<Task>();
            foreach (var dto in dtos)
            {
                var s = JsonConvert.SerializeObject(dto, new JsonSerializerSettings() { Formatting = Formatting.Indented });
                await WriteTextAsync(Path.Combine(path, dto.GetFileName()), s);
            }
        }


        private async Task WriteTextAsync(string filePath, string text)
        {
            byte[] encodedText = Encoding.UTF8.GetBytes(text);

            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Create, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };
        }
        #endregion
    }
}
