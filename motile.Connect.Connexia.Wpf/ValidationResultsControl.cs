﻿using motile.Connect;
using motile.Connect.Validation;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;


namespace motile.Connect.Connexia.Wpf
{
    public class ValidationResultsControl : Control
    {
        static ValidationResultsControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ValidationResultsControl), new FrameworkPropertyMetadata(typeof(ValidationResultsControl)));
        }

        public IEnumerable<Tuple<IDTO, ValidationInfo>> Results
        {
            get { return (IEnumerable<Tuple<IDTO, ValidationInfo>>)GetValue(ResultsProperty); }
            set { SetValue(ResultsProperty, value); }
        }
        
        public static readonly DependencyProperty ResultsProperty =
            DependencyProperty.Register("Results", typeof(IEnumerable<Tuple<IDTO, ValidationInfo>>), typeof(ValidationResultsControl), new PropertyMetadata(null));



        public void Ignore(Tuple<IDTO, ValidationInfo> info)
        {
            var settingsProvider = new LocalSettingsProvider<LocalConnectSettings>(LocalConnectSettings.ID);
            var settings = settingsProvider.GetSettings();

            settings.AddValidationInfoToIgnore(info.Item2);

            settingsProvider.SetSettings(settings);

            if (Results is ICollection<Tuple<IDTO, ValidationInfo>>)
            {
                ((ICollection<Tuple<IDTO, ValidationInfo>>)Results).Remove(info);
            }
        }
    }
}
